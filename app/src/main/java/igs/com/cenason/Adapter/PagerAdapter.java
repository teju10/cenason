package igs.com.cenason.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import igs.com.cenason.fragment.Nearby_cenas_list_fragment;
import igs.com.cenason.fragment.Nearby_cenas_map_fragment;

/**
 * Created by Shoaib on 22-Aug-16.
 */
public class PagerAdapter  extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Nearby_cenas_map_fragment tab1 = new Nearby_cenas_map_fragment();
                return tab1;
            case 1:
                Nearby_cenas_list_fragment tab2 = new Nearby_cenas_list_fragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

