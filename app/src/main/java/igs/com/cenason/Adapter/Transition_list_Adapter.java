package igs.com.cenason.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;

/**
 * Created by Infograins on 11/11/2016.
 */
public class Transition_list_Adapter extends BaseAdapter {

    Context appContext;
    LinkedList<JSONObject> list;
    int[] res = new int[]{R.drawable.background1, R.drawable.background2};

    public Transition_list_Adapter(Context appContext, LinkedList<JSONObject> list) {
        this.appContext = appContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.activity_transition_history_item, null);
        }
        ((LinearLayout) convertView.findViewById(R.id.rootLayout)).setBackgroundResource(res[position % 2]);
        try {
            ((CustomTextView) convertView.findViewById(R.id.transition_id)).setText(list.get(position).getString("transaction_id"));
            ((CustomTextView) convertView.findViewById(R.id.transition_amount)).setText("$ " + list.get(position).getString("price"));
            ((CustomTextView) convertView.findViewById(R.id.transition_Type)).setText(list.get(position).getString("transaction_type"));
            ((CustomTextView) convertView.findViewById(R.id.transition_date_time))
                    .setText(parseDateToyyyyMMdd(list.get(position).getString("thedate")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public String parseDateToyyyyMMdd(String time) {
        String outputPattern = "dd/MM/yyyy";
        String inputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        String dayOfTheWeek = null;
        try {
            date = inputFormat.parse(time);
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            dayOfTheWeek = sdf.format(date);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dayOfTheWeek + " " + str;
    }

    public String Time(String time) {
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
        String str = null;
        try {
            Date date = parseFormat.parse(time);
            str = displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
