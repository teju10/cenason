package igs.com.cenason.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import igs.com.cenason.Activity_BookingHistoryList;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.CustomWidget.SweetAlertDialog;
import igs.com.cenason.My_cenas_Activity;
import igs.com.cenason.R;

/**
 * Created by Infograins on 9/1/2016.
 */
public class MycenaslistAdapter extends BaseAdapter {
    Context appContext;
    LinkedList<JSONObject> list;
    int[] res = new int[]{R.drawable.background1, R.drawable.background2};

    public MycenaslistAdapter(Context appContext, LinkedList<JSONObject> list) {
        this.appContext = appContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.activity_my_cenas_item, null);
        }
        ((LinearLayout) convertView.findViewById(R.id.rootLayout)).setBackgroundResource(res[position % 2]);
        try {
            ((CustomTextView) convertView.findViewById(R.id.my_cenas_name)).setText(list.get(position).getString("name"));
            ((CustomTextView) convertView.findViewById(R.id.my_cenas_des_title)).setText(list.get(position).getString("description"));
            System.out.println("Discription Title ===> " + list.get(position).getString("description"));

            Glide.with(appContext).load(list.get(position).getJSONArray("cenas_image").getString(0))
                    .into(((ImageView) convertView.findViewById(R.id.mycenas_image_view)));

            (convertView.findViewById(R.id.delete_image)).setOnClickListener(new MyClick(position));
            (convertView.findViewById(R.id.edit_image)).setOnClickListener(new MyClick(position));
            (convertView.findViewById(R.id.info_image)).setOnClickListener(new MyClick(position));
            (convertView.findViewById(R.id.eventbuk_image)).setOnClickListener(new CenasHistory(position));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    class MyClick implements View.OnClickListener {
        int position;

        public MyClick(int mposition) {
            position = mposition;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.delete_image:
                    new SweetAlertDialog(appContext)
                            .setTitleText("Delete Cenas")
                            .setContentText("Do you want to delete cenas")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            }).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            ((My_cenas_Activity) appContext).deletemycenas(position);
                            sweetAlertDialog.dismissWithAnimation();
                        }
                    })
                            .show();
                    break;
                case R.id.edit_image:
                    ((My_cenas_Activity) appContext).editCenas(position);
                    break;
                case R.id.info_image:
                    ((My_cenas_Activity) appContext).infoCenas(position);
                    break;
            }
        }
    }

    public class CenasHistory implements View.OnClickListener {
        int Position;

        public CenasHistory(int Pos) {
            Position = Pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Intent i = new Intent(appContext, Activity_BookingHistoryList.class);
                Bundle b = new Bundle();
                b.putString("C_ID", list.get(Position).getString("id"));
                i.putExtras(b);
                appContext.startActivity(i);
            } catch (JSONException r) {
                r.printStackTrace();
            }
        }
    }

    public void MyListNotify(LinkedList<JSONObject> liscenas) {
        list = new LinkedList<>();
        list = liscenas;
        notifyDataSetChanged();
    }
}
