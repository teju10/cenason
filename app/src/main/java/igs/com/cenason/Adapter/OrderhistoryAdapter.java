package igs.com.cenason.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import igs.com.cenason.Activity_cancle_order;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;
import igs.com.cenason.Utilities.ConstantsUrlKey;

/**
 * Created by Infograins on 9/21/2016.
 */
public class OrderhistoryAdapter extends BaseAdapter {

    Context appContext;
    LinkedList<JSONObject> list;
    int[] res = new int[]{R.drawable.background1, R.drawable.background2};

    public OrderhistoryAdapter(Context appContext, LinkedList<JSONObject> list) {
        this.appContext = appContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.activity_order_history_item, null);
        }
        ((LinearLayout) convertView.findViewById(R.id.rootLayout)).setBackgroundResource(res[position % 2]);
        try {
            ((CustomTextView) convertView.findViewById(R.id.orderhist_cenas_name)).setText(list.get(position).getString("name"));
            ((CustomTextView) convertView.findViewById(R.id.orderhist_cenas_price)).setText("$ " + list.get(position).getString("price"));
            ((CustomTextView) convertView.findViewById(R.id.orderhist_cenas_number)).setText(list.get(position).getString("contact_number"));
            ((CustomTextView) convertView.findViewById(R.id.orderhist_cenas_quantity)).setText(list.get(position).getString("quantity"));
            ((CustomTextView) convertView.findViewById(R.id.orderhist_cenas_date_time))
                    .setText(parseDateToyyyyMMdd(list.get(position).getString("order_date")) + " , " + Time(list.get(position).getString("order_time")));
            System.out.println("Discription Title ===> " + list.get(position).getString("description"));
            ((CustomTextView) convertView.findViewById(R.id.orderhist_orderstatus)).setText(list.get(position).getString("order_status"));
            Glide.with(appContext).load(list.get(position).getString("cenas_image")).into(((ImageView) convertView.findViewById(R.id.mycenas_image_view)));
            convertView.setOnClickListener(new MyClick(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public String parseDateToyyyyMMdd(String time) {
        String outputPattern = "dd/MM/yyyy";
        String inputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        String dayOfTheWeek = null;
        try {
            date = inputFormat.parse(time);
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            dayOfTheWeek = sdf.format(date);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dayOfTheWeek + " " + str;
    }

    public String Time(String time) {
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
        String str = null;
        try {
            Date date = parseFormat.parse(time);
            str = displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    class MyClick implements View.OnClickListener {
        int position;

        public MyClick(int Pos) {
            position = Pos;
        }

        @Override
        public void onClick(View v) {
            try {
                System.out.println("list position ====> " + position);
                if (list.get(position).getString("order_status").equals("complete")) {
                    Intent intent = new Intent(appContext, Activity_cancle_order.class);
                    intent.putExtra(ConstantsUrlKey.CENAS_DATA, list.get(position).toString());
                    appContext.startActivity(intent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
