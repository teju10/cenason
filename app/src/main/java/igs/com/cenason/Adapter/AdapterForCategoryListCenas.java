package igs.com.cenason.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.gc.materialdesign.views.ButtonRectangle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igs.com.cenason.Activity_View_Cenas;
import igs.com.cenason.Activity_list_by_category;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.CustomWidget.HorizontalListView;
import igs.com.cenason.R;

/**
 * Created by Infograins on 9/5/2016.
 */
public class AdapterForCategoryListCenas extends BaseAdapter {
    ArrayList<JSONObject> listcenas_cat;
    LayoutInflater li;
    private Context appContext;
    int[] res = new int[]{R.color.blue, R.color.material_deep_teal_50, R.color.gp_color};

    public AdapterForCategoryListCenas(Context appContext, ArrayList<JSONObject> list) {
        this.appContext = appContext;
        this.listcenas_cat = list;
        li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listcenas_cat.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vadapter = convertView;
        SingleGridViewHolder viewHolder;
        if (convertView == null) {
            vadapter = li.inflate(R.layout.item_single_at_home, null);
            viewHolder = new SingleGridViewHolder(vadapter);
            vadapter.setTag(viewHolder);
        } else {
            viewHolder = (SingleGridViewHolder) vadapter.getTag();
        }
        viewHolder.cat_name.setBackgroundResource(res[position % 3]);
        try {
            JSONObject j = listcenas_cat.get(position);
            viewHolder.h_list.setAdapter(new AdapterForHorizontal(appContext, j.getJSONArray("cenas_list"), position));
            viewHolder.cat_name.setText(j.getString("cat_name"));
            (vadapter.findViewById(R.id.cat_name)).setOnClickListener(new MyClick(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return vadapter;
    }

    class MyClick implements View.OnClickListener {
        int position;

        public MyClick(int Pos) {
            position = Pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Intent intent = new Intent(appContext, Activity_list_by_category.class);
                intent.putExtra("catgoryname", listcenas_cat.get(position).getString("cat_name"));
                appContext.startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}

class SingleGridViewHolder {
    public CustomTextView cat_name;
    public HorizontalListView h_list;
    public ButtonRectangle btn_view_all;

    public SingleGridViewHolder(View base) {
        cat_name = (CustomTextView) base.findViewById(R.id.cat_name);
        h_list = (HorizontalListView) base.findViewById(R.id.h_list);
        btn_view_all = (ButtonRectangle) base.findViewById(R.id.btn_view_all);
    }
}

class AdapterForHorizontal extends BaseAdapter {
    JSONArray listcenas_cat;
    LayoutInflater li;
    int position;
    private Context appContext;

    public AdapterForHorizontal(Context appContext, JSONArray list, int pos) {
        this.appContext = appContext;
        this.listcenas_cat = list;
        this.position = pos;
        li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listcenas_cat.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vadapter = convertView;
        SingleGridViewHolder1 viewHolder;
        if (convertView == null) {
            vadapter = li.inflate(R.layout.item_single_for_horizontal, null);
            viewHolder = new SingleGridViewHolder1(vadapter);
            vadapter.setTag(viewHolder);
        } else {
            viewHolder = (SingleGridViewHolder1) vadapter.getTag();
        }
        try {
            JSONObject j1 = listcenas_cat.getJSONObject(position);
            viewHolder.cenas_name.setText(j1.getString("name"));
            viewHolder.cenas_price.setText("$ " + j1.getString("price"));
            if (j1.getString("cenas_image").equals("")) {
                Glide.with(appContext).load(j1.getString("cenas_thumbnail")).into(viewHolder.cenas_image);
            } else {
                Glide.with(appContext).load(j1.getString("cenas_image")).into(viewHolder.cenas_image);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        vadapter.setOnClickListener(new ListItemClick(position));
        return vadapter;
    }

    class ListItemClick implements View.OnClickListener {
        int position;

        public ListItemClick(int Pos) {
            position = Pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Intent intent = new Intent(appContext, Activity_View_Cenas.class);
                intent.putExtra("catgory_id", listcenas_cat.getJSONObject(position).getString("id"));
                appContext.startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    class SingleGridViewHolder1 {
        public ImageView cenas_image;
        public CustomTextView cenas_name, cenas_price;

        public SingleGridViewHolder1(View base) {
            cenas_image = (ImageView) base.findViewById(R.id.cenas_image);
            cenas_name = (CustomTextView) base.findViewById(R.id.cenas_name);
            cenas_price = (CustomTextView) base.findViewById(R.id.cenas_price);
        }
    }

}
