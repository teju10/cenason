package igs.com.cenason.Adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import igs.com.cenason.R;

/**
 * Created by Nilesh Kansal on 28-May-16.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.PersonViewHolder> {

    List<CategoryItems> people;
    OnItemClickListener mItemClickListener;

    public CategoryAdapter(List<CategoryItems> persons) {
        this.people = persons;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dialog_category_list_item, viewGroup, false);
        return new PersonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.personName.setText(people.get(i).name);
        personViewHolder.cbSelected.setChecked(people.get(i).isSelected());
    }

    public void setItemSelected(int position, boolean isSelected) {
        if (position != -1) {
            people.get(position).setSelected(isSelected);
            notifyDataSetChanged();
        }
    }

    public void removeItemSelected(String LabelName, boolean isSelected) {
        for (int i = 0; i < people.size(); i++) {
            if (LabelName.equals(people.get(i).getName())) {
                people.get(i).setSelected(isSelected);
                notifyDataSetChanged();
                break;
            }
        }
    }

    List<CategoryItems> getPeople() {
        return people;
    }

    public void setPeople(List<CategoryItems> people) {
        this.people = people;
    }

    @Override
    public int getItemCount() {
        return people.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView personName;
        CheckBox cbSelected;

        PersonViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            personName = (TextView) itemView.findViewById(R.id.cat_list_name);
            cbSelected = (CheckBox) itemView.findViewById(R.id.cat_list_Selected);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getLayoutPosition());
            }
        }
    }

}