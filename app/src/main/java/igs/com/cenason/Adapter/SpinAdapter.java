package igs.com.cenason.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;


/**
 * Created by Shoaib on 18-May-16.
 */
public class SpinAdapter extends BaseAdapter {
    String[] language;
    int[] flag;
    private Context appContext;

    public SpinAdapter(Context context, String[] lang, int[] res) {
        this.appContext = context;
        this.language = lang;
        this.flag = res;
    }

    @Override
    public int getCount() {
        return flag.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = li.inflate(R.layout.single_list_item_spin, null);
        }
            ((CustomTextView) convertView.findViewById(R.id.name_lang)).setText(language[position]);
        ((ImageView)convertView.findViewById(R.id.flag)).setImageResource(flag[position]);
        return convertView;
    }
}
