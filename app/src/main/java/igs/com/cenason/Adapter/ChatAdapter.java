package igs.com.cenason.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import igs.com.cenason.Activity_View_Cenas;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 10/24/2016.
 */
public class ChatAdapter extends BaseAdapter {
    Context appContext;
    ArrayList<JSONObject> jsonObjects;
    LayoutInflater inflater;
    String datetime = "";
    String time[];

    public ChatAdapter(Context context, ArrayList<JSONObject> objects) {
        System.out.println("inside adapter");
        this.appContext = context;
        jsonObjects = new ArrayList<>();
        jsonObjects.addAll(objects);
        inflater = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        System.out.println("inside adapter" + jsonObjects.size());
    }

    @Override
    public int getCount() {
        return jsonObjects.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.activity_conversation_right, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        try {

            JSONObject j = jsonObjects.get(position);
        /*    System.out.println("from_user id" + j.getString("from_user"));
            System.out.println("user_id" + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));*/
            if (j.getString("from_user").equals(Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID))) {
                viewHolder.layout_msg_left.setVisibility(View.GONE);
                if (j.getString("type").equals("share")) {
                    viewHolder.layout_msg_right.setVisibility(View.VISIBLE);
                    viewHolder.img_right.setVisibility(View.VISIBLE);
                    Glide.with(appContext).load(j.getString("cenas_image_url")).into(viewHolder.img_right);
                    viewHolder.text_msg_right.setText(j.getString("cenas_name"));
                    datetime = j.getString("datetime");
                    time = datetime.split(" ");
                    viewHolder.text_msg_datetime.setText(Time(time[1]));
                } else {
                    viewHolder.layout_msg_right.setVisibility(View.VISIBLE);
                    viewHolder.text_msg_right.setText(j.getString("message"));
                    viewHolder.img_right.setVisibility(View.GONE);

                }
                datetime = j.getString("datetime");
                time = datetime.split(" ");
                viewHolder.text_msg_datetime.setText(Time(time[1]));
                if (j.getString("is_read").equals("1")) {
                    viewHolder.read_unread_icon.setImageResource(R.drawable.ic_read_icon);
                } else {
                    viewHolder.read_unread_icon.setImageResource(R.drawable.ic_unread_icon);
                    System.out.println("After change");
                }
            } else {
                viewHolder.layout_msg_right.setVisibility(View.GONE);
                viewHolder.layout_msg_left.setVisibility(View.VISIBLE);
                if (j.getString("type").equals("share")) {
                    viewHolder.img_left.setVisibility(View.VISIBLE);
                    Glide.with(appContext).load(j.getString("cenas_image_url")).into(viewHolder.img_left);
                    viewHolder.text_msg_left.setText(j.getString("cenas_name"));
                    datetime = j.getString("datetime");
                    time = datetime.split(" ");
                    viewHolder.chat_left_date_time.setText(Time(time[1]));
                } else {
                    datetime = j.getString("datetime");
                    time = datetime.split(" ");
                    viewHolder.text_msg_left.setText(j.getString("message"));
                    viewHolder.chat_left_date_time.setText(Time(time[1]));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        convertView.setOnClickListener(new MyClickForCenasDetail(position));
        return convertView;
    }

    class MyClickForCenasDetail implements View.OnClickListener {
        int pos;

        public MyClickForCenasDetail(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                if (!jsonObjects.get(pos).getString("cenas_id").equals("")) {
                    //appContext.startActivity(new Intent(appContext,));
                    Intent intent = new Intent(appContext, Activity_View_Cenas.class);
                    intent.putExtra("catgory_id", jsonObjects.get(pos).getString("cenas_id"));
                    appContext.startActivity(intent);
                }

            } catch (JSONException e) {

            }
        }
    }

    class ViewHolder {
        RelativeLayout layout_msg_right;
        RelativeLayout layout_msg_left;
        CustomTextView text_msg_right;
        CustomTextView text_msg_left;
        CustomTextView text_msg_datetime;
        CustomTextView chat_left_date_time;
        ImageView read_unread_icon, img_left, img_right;

        public ViewHolder(View convertView) {
            layout_msg_right = (RelativeLayout) convertView.findViewById(R.id.layout_msg_right);
            layout_msg_left = (RelativeLayout) convertView.findViewById(R.id.layout_msg_left);
            text_msg_right = (CustomTextView) convertView.findViewById(R.id.text_msg_right);
            text_msg_left = (CustomTextView) convertView.findViewById(R.id.text_msg_left);
            text_msg_datetime = (CustomTextView) convertView.findViewById(R.id.chat_date_time);
            chat_left_date_time = (CustomTextView) convertView.findViewById(R.id.chat_left_date_time);
            read_unread_icon = (ImageView) convertView.findViewById(R.id.read_unread_icon);
            img_left = (ImageView) convertView.findViewById(R.id.img_left);
            img_right = (ImageView) convertView.findViewById(R.id.img_right);
        }
    }

    public String parseDateToyyyyMMdd(String time) {
        String outputPattern = "dd/MM/yyyy";
        String inputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        String dayOfTheWeek = null;
        try {
            date = inputFormat.parse(time);
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            dayOfTheWeek = sdf.format(date);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dayOfTheWeek + " " + str;
    }

    public String Time(String time) {
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
        String str = null;
        try {
            Date date = parseFormat.parse(time);
            str = displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}