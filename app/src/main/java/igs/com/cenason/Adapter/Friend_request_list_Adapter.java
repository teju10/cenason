package igs.com.cenason.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONObject;
import java.util.LinkedList;
import igs.com.cenason.Activity_invitation_forchat;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;
import igs.com.cenason.Utilities.ConstantsUrlKey;

/**
 * Created by Infograins on 10/21/2016.
 */

public class Friend_request_list_Adapter extends BaseAdapter {

    Context appContext;
    LinkedList<JSONObject> list;

    public Friend_request_list_Adapter(Context appContext, LinkedList<JSONObject> list) {
        this.appContext = appContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.friend_request_list_item, null);
        }
        try {
            ((CustomTextView) convertView.findViewById(R.id.tvusername)).setText(list.get(position).getString("username"));
            ((CustomTextView) convertView.findViewById(R.id.tvusermsg)).setText(list.get(position).getString("message"));
            final View finalconvertview = convertView;
            Glide.with(appContext).
                    load(list.get(position).getString("profile_image")).
                    listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            ((ProgressBar)finalconvertview.findViewById(R.id.pb_loader)).setVisibility(View.GONE);
                            return false;
                        }
                    }).
                    into(((ImageView) convertView.findViewById(R.id.user_image)));
            convertView.setOnClickListener(new MyClick(position));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    class MyClick implements View.OnClickListener {
        int position;

        public MyClick(int Pos) {
            position = Pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Intent intent = new Intent(appContext, Activity_invitation_forchat.class);
                intent.putExtra(ConstantsUrlKey.CENAS_DATA, list.get(position).toString());
                appContext.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
