package igs.com.cenason.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONObject;

import java.util.LinkedList;

import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;


/**
 * Created by Tejas on 22/2/17.
 */

public class AdapterBookingHistory extends BaseAdapter {

    Context mContext;
    LinkedList<JSONObject> hlist;
    int reso;
    int[] res = new int[]{R.drawable.background1, R.drawable.background2};

    public AdapterBookingHistory(Context context, int res, LinkedList<JSONObject> list) {
        this.mContext = context;
        this.hlist = list;
        this.reso = res;
    }

    @Override
    public int getCount() {
        return hlist.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(reso, null);
            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        JSONObject j = hlist.get(position);
        try {
            String formattedDate = j.getString("datetime");
            String datearr[] = formattedDate.split(" ");
            String date = datearr[0];
            viewHolder.buk_username.setText(j.getString("fullname"));
           /* viewHolder.cenas_name.setText(j.getString("description"));*/
            viewHolder.buk_cenas_date.setText(date);
            viewHolder.buk_cenas_phonenu.setText(j.getString("contact_number"));

            viewHolder.book_cenas_quantity.setText(j.getString("quantity")+" Quantity ");
            Glide.with(mContext).load(j.getString("profile_image"))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into((viewHolder.buk_uimg));


        } catch (Exception e) {
            e.printStackTrace();
        }

        ((LinearLayout) convertView.findViewById(R.id.relatv_layout)).setBackgroundResource(res[position % 2]);
        return convertView;
    }

    public class ViewHolder {
        CustomTextView buk_username, cenas_name, book_cenas_quantity, buk_cenas_date, buk_cenas_phonenu;
        ImageView buk_uimg;

        public ViewHolder(View b) {
            buk_username = (CustomTextView) b.findViewById(R.id.buk_username);
            cenas_name = (CustomTextView) b.findViewById(R.id.cenas_name);
            book_cenas_quantity = (CustomTextView) b.findViewById(R.id.book_cenas_quantity);
            buk_cenas_date = (CustomTextView) b.findViewById(R.id.buk_cenas_date);
            buk_cenas_phonenu = (CustomTextView) b.findViewById(R.id.buk_cenas_phonenu);
            buk_uimg = (ImageView) b.findViewById(R.id.buk_uimg);
        }

    }

}
