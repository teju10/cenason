package igs.com.cenason.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import igs.com.cenason.Activity_View_Cenas;
import igs.com.cenason.Activity_list_by_category;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;

/**
 * Created by Infograins on 8/31/2016.
 */
public class NearbylistAdapter extends BaseAdapter {
    Context appContext;
    LinkedList<JSONObject> list;
    int [] res=new int[]{R.drawable.background1,R.drawable.background2};

    public NearbylistAdapter(Context appContext, LinkedList<JSONObject> list) {
        this.appContext = appContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.tab_nearby_cenas_list_view_itm, null);
        }
        ((LinearLayout)convertView.findViewById(R.id.rootLayout)).setBackgroundResource(res[position%2]);
        try {
            ((CustomTextView) convertView.findViewById(R.id.cenas_name)).setText(list.get(position).getString("name"));
            ((CustomTextView) convertView.findViewById(R.id.cenas_des_title)).setText(list.get(position)
                    .getString("description").replace("\n",""));
            System.out.println("Discription Title ===> "+list.get(position).getString("description"));
            Glide.with(appContext).load(list.get(position).getString("cenas_image")).into(((ImageView)convertView.findViewById(R.id.image_view)));
            convertView.findViewById(R.id.view_btn).setOnClickListener(new MyClick(position));
        } catch (Exception e) {
        }
        return convertView;
    }

    class MyClick implements View.OnClickListener {
        int position;

        public MyClick(int Pos) {
            position = Pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Intent intent = new Intent(appContext, Activity_View_Cenas.class);
                intent.putExtra("catgory_id", list.get(position).getString("id"));
                appContext.startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
