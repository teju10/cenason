package igs.com.cenason.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import igs.com.cenason.Activity_View_Cenas;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;

/**
 * Created by Infograins on 11/8/2016.
 */
public class SearchAdapter extends BaseAdapter {
    Context appContext;
    LinkedList<JSONObject> list;
    int [] res=new int[]{R.drawable.background1,R.drawable.background2};

    public SearchAdapter(Context appContext, LinkedList<JSONObject> list) {
        this.appContext = appContext;
        this.list = list;
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.search_list_item, null);
        }
        ((LinearLayout)convertView.findViewById(R.id.rootLayout)).setBackgroundResource(res[position%2]);
        try {
            ((CustomTextView) convertView.findViewById(R.id.cenas_name)).setText(list.get(position).getString("name"));
            ((CustomTextView) convertView.findViewById(R.id.cenas_des)).setText(list.get(position).getString("description"));
            ((CustomTextView) convertView.findViewById(R.id.cenas_date_time)).setText(parseDateToyyyyMMdd(list.get(position).getString("created_date").split(",")[0]));

           final View finalconvertview = convertView;
            Glide.with(appContext).load(list.get(position).getString("cenas_image")).
                    listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            ((ProgressBar)finalconvertview.findViewById(R.id.pb_search)).setVisibility(View.GONE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            ((ProgressBar)finalconvertview.findViewById(R.id.pb_search)).setVisibility(View.GONE);
                            return false;
                        }
                    }).
                    into(((ImageView) convertView.findViewById(R.id.cenas_image_view)));
            convertView.setOnClickListener(new MyClick(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public String parseDateToyyyyMMdd(String time) {
        String outputPattern = "dd/MM/yyyy";
        String inputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        String dayOfTheWeek = null;
        try {
            date = inputFormat.parse(time);
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            dayOfTheWeek = sdf.format(date);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dayOfTheWeek + " " + str;
    }

    class MyClick implements View.OnClickListener {
        int position;

        public MyClick(int Pos) {
            position = Pos;
        }

        @Override
        public void onClick(View v) {
            try {
                System.out.println("list position ====> "+position);
                Intent intent = new Intent(appContext, Activity_View_Cenas.class);
                intent.putExtra("catgory_id", list.get(position).getString("id"));
                appContext.startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
