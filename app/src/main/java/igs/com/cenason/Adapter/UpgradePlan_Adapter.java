package igs.com.cenason.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.gc.materialdesign.views.ButtonRectangle;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igs.com.cenason.Activity_upgrade_plan_list;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.CustomWidget.SweetAlertDialog;
import igs.com.cenason.R;

/**
 * Created by Infograins on 10/14/2016.
 */

public class UpgradePlan_Adapter extends BaseAdapter {
    public ArrayList<JSONObject> packages_list;
    Context appContext;
    LayoutInflater li;
    int res;

    public UpgradePlan_Adapter(Context appContext, ArrayList<JSONObject> packages_list, int res) {
        this.packages_list = packages_list;
        this.appContext = appContext;
        this.res = res;
        li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return packages_list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SingleViewHolder viewHolder;
        if (convertView == null) {
            convertView = li.inflate(res, null);
            viewHolder = new SingleViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (SingleViewHolder) convertView.getTag();
        }
        try {
            JSONObject j = packages_list.get(position);

                viewHolder.name_pack.setText(j.getString("title"));
                viewHolder.price_pack.setText("$" + j.getString("amount"));
                viewHolder.des_pack.setText(j.getString("description"));
                viewHolder.dura_pack.setText(j.getString("duration")+" Month");
                viewHolder.btn_buy.setOnClickListener(new MyClick(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return convertView;
    }

    class MyClick implements View.OnClickListener {
        int pos;

        public MyClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            new SweetAlertDialog(appContext)
                    .setTitleText(appContext.getResources().getString(R.string._upgrade_plan))
                    .setContentText(appContext.getResources().getString(R.string.do_you))
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                        }
                    }).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    ((Activity_upgrade_plan_list) appContext).onBuyPressed(pos);
                    sweetAlertDialog.dismissWithAnimation();
                }
            })
                    .show();
        }
    }

    class SingleViewHolder {
        CustomTextView name_pack, price_pack, des_pack,dura_pack;
        ButtonRectangle btn_buy;

        public SingleViewHolder(View base) {
            name_pack = (CustomTextView) base.findViewById(R.id.name_pack);
            price_pack = (CustomTextView) base.findViewById(R.id.price_pack);
            des_pack = (CustomTextView) base.findViewById(R.id.des_pack);
            dura_pack = (CustomTextView) base.findViewById(R.id.duration_pack);
            btn_buy = (ButtonRectangle) base.findViewById(R.id.btn_buy);
        }
    }
}
