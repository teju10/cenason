package igs.com.cenason.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import igs.com.cenason.fragment.Chat_contact_list_fragment;
import igs.com.cenason.fragment.Chat_list_fragment;

/**
 * Created by Infograins on 10/14/2016.
 */
public class PagerAdapter_forchat extends FragmentStatePagerAdapter {
    int mNumOfTabs;


    public PagerAdapter_forchat(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
               Chat_list_fragment tab1 = new Chat_list_fragment();
                return tab1;
            case 1:
                Chat_contact_list_fragment tab2 = new Chat_contact_list_fragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
