package igs.com.cenason.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;

import igs.com.cenason.Activity_my_friends_list;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;

/**
 * Created by Infograins on 11/16/2016.
 */

public class MyfriendslistAdapter extends BaseAdapter {

    Context appContext;
    ArrayList<CategoryContactItem> list;

    public MyfriendslistAdapter(Context appContext, ArrayList<CategoryContactItem> list) {
        this.appContext = appContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.my_friends_list_item, null);
        }
        try {
            ((CustomTextView) convertView.findViewById(R.id.my_friend_list_name)).setText(list.get(position).getC_name());
            ((ImageView)convertView.findViewById(R.id.img)).setOnClickListener(new MyClick(position));
            ((CheckBox)convertView.findViewById(R.id.cb_share_cens)).setOnCheckedChangeListener(new ListClick(position));

            //convertView.setOnClickListener(new ListClick(position));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public void setItemSelected(int position, boolean isSelected) {

        if (position != -1) {
            list.get(position).setSelected(isSelected);
            notifyDataSetChanged();
        }
    }

    class MyClick implements View.OnClickListener {
        int position;

        public MyClick(int Pos) {
            position = Pos;
        }

        @Override
        public void onClick(View v) {
            ((Activity_my_friends_list) appContext).Sharecenas(position);
        }
    }

    class ListClick implements CompoundButton.OnCheckedChangeListener {
        int position;

        public ListClick(int Pos) {
            position = Pos;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            ((Activity_my_friends_list) appContext).ContactItemChecked(position);
        }
    }

}
