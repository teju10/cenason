package igs.com.cenason.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import igs.com.cenason.Activity_View_Cenas;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;

/**
 * Created by Infograins on 9/9/2016.
 */
public class ListViewAdapter extends BaseAdapter {

    Context appContext;
    LinkedList<JSONObject> list;

    public ListViewAdapter(Context appContext, int activity_list_by_category, LinkedList<JSONObject> list) {
        this.appContext = appContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.all_category_list_item, null);
        }
        try {
            final View finalConvertView = convertView;
            ((CustomTextView) convertView.findViewById(R.id.title_name)).setText(list.get(position).getString("name"));
            ((CustomTextView) convertView.findViewById(R.id.subtitle)).setText("$ "+list.get(position).getString("price"));
            ((CustomTextView) convertView.findViewById(R.id.date)).setText(list.get(position).getString("date"));
            if (list.get(position).getString("cenas_image").equals("")){

                Glide.with(appContext).
                        load(list.get(position).getString("cenas_thumbnail")).
                        listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                ((ProgressBar) finalConvertView.findViewById(R.id.pb_home)).setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                ((ProgressBar) finalConvertView.findViewById(R.id.pb_home)).setVisibility(View.GONE);
                                return false;
                            }
                        }).
                        into(((ImageView) convertView.findViewById(R.id.icon_list)));
            }else {
                Glide.with(appContext).load(list.get(position).getString("cenas_image")).
                        listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                                ((ProgressBar) finalConvertView.findViewById(R.id.pb_home)).setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                ((ProgressBar) finalConvertView.findViewById(R.id.pb_home)).setVisibility(View.GONE);

                                return false;
                            }
                        }).
                        into(((ImageView) convertView.findViewById(R.id.icon_list)));
            }
            convertView.setOnClickListener(new MyClick(position));
        }catch (Exception e){
            e.printStackTrace();
        }
        return convertView;
    }

    class MyClick implements View.OnClickListener {
        int position;

        public MyClick(int Pos) {
            position = Pos;
        }

        @Override
        public void onClick(View v) {
            try {
                System.out.println("list position ====> "+position);
                Intent intent = new Intent(appContext, Activity_View_Cenas.class);
                intent.putExtra("catgory_id", list.get(position).getString("id"));
                appContext.startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
