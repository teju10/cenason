package igs.com.cenason.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import igs.com.cenason.Activity_Upload_Cenas;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;

/**
 * Created by Infograins on 9/29/2016.
 */

public class DayTimeAdapter extends BaseAdapter {

    Context appContext;
    ArrayList<String> list;

    public DayTimeAdapter(Context appContext, ArrayList<String> list) {
        this.appContext = appContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
        LayoutInflater li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = li.inflate(R.layout.day_time_list_item, null);
    }
        try {
            ((CustomTextView) convertView.findViewById(R.id.day_textview)).setText(list.get(position).split("-")[0]);
            ((CustomTextView) convertView.findViewById(R.id.selected_time)).setText(list.get(position).split("-")[1]+" To "+list.get(position).split("-")[2]);
            (convertView.findViewById(R.id.hours_delet_image)).setOnClickListener(new MyClick(position));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    class MyClick implements View.OnClickListener {
        int position;

        public MyClick(int mposition) {
            position = mposition;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.hours_delet_image:
                    ((Activity_Upload_Cenas)appContext).removeDatetime(position);
                    break;
            }
        }
    }


}
