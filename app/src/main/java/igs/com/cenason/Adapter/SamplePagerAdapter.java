package igs.com.cenason.Adapter;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import igs.com.cenason.Activity_C_Detail_ImageZooming;


public class SamplePagerAdapter extends PagerAdapter {

    JSONArray js;
    Activity appContexr;
    private int mSize;

    public SamplePagerAdapter(Activity appContexr, JSONArray js) {
        this.js = js;
        this.appContexr = appContexr;
    }

    @Override
    public int getCount() {
        return js.length();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        LinearLayout layout = new LinearLayout(appContexr);
        try {
            ImageView img = new ImageView(appContexr);
            Glide.with(appContexr).load(js.getString(position)).into(img);
            img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            layout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            layout.setLayoutParams(layoutParams);
            layout.addView(img);

            layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {

                        Intent i = new Intent(appContexr, Activity_C_Detail_ImageZooming.class);
                        i.putExtra("file_array", js.toString());
                        appContexr.startActivity(i);
                        Log.e("TAG", "This page was clicked: " + js.toString());
                    } catch (Exception j) {
                        j.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {

        }
        ((ViewPager) view).addView(layout, 0);
        return layout;
    }

}