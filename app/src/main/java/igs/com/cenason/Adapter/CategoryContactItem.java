package igs.com.cenason.Adapter;

import bolts.Bolts;

/**
 * Created by sandeep on 9/2/17.
 */

public class CategoryContactItem {
    public String c_id,c_name;

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    boolean selected;

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
