package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import igs.com.cenason.Adapter.OrderhistoryAdapter;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 9/21/2016.
 */
public class Activity_order_history extends AppCompatActivity  {

    Context appContext;
    ResponseTask rt;
    LinkedList<JSONObject> orderlist=new LinkedList<>();
    SharedPreferences share;
    SharedPreferences.Editor edit;
    Toolbar mToolbar;
    OrderhistoryAdapter listAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cenas);
        appContext = this;
        Init();
    }

    private void Init() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        (findViewById(R.id.fab_btn)).setVisibility(View.GONE);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.order_history));
        if (Utility.isConnectingToInternet(appContext)) {
            Getorderlist();
        } else {
            Utility.showCroutonWarn(getResources().getString(R.string.error_internet), Activity_order_history.this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext,HomeActivity.class));
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }


    public void Getorderlist() {
        System.out.println("user id === > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.ORDERLIST);
//            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_order_history.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                for (int i = 0; i < jobj.getJSONArray(ConstantsUrlKey.OBJECT).length(); i++) {
                                    orderlist.add(jobj.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i));
                                }
                                listAdapter = new OrderhistoryAdapter(appContext, orderlist);
                                ((ListView) findViewById(R.id.mycenas_list_view)).setAdapter(listAdapter);
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_order_history.this);
                            }
                        } catch (JSONException e2) {
                         Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_order_history.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }
}
