package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.SimpleAdapter;


import com.google.firebase.iid.FirebaseInstanceId;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import igs.com.cenason.CustomWidget.CustomCheckBox;
import igs.com.cenason.CustomWidget.MyAutoCompleteTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.GPSTracker;
import igs.com.cenason.Utilities.PlaceJSONParser;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by LakhanPatidar on 28-Jul-16.
 */
public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    public String TAG = SignupActivity.class.getCanonicalName();
    ResponseTask rt;
    private Context appContext;
    double lat, lang;
    GPSTracker mGPS;
    LocationManager mManager;
//    AutoLabelUI label_view;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    ParserTask parserTask;
    MyAutoCompleteTextView address;
    PlacesTask placesTask;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = this;
        setContentView(R.layout.activity_signup);
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("token is"+token);
        Init();
    }

    private void Init() {
        (findViewById(R.id.btn_sign)).setOnClickListener(this);
        //(findViewById(R.id.select_category)).setOnClickListener(this);
        address = (MyAutoCompleteTextView) findViewById(R.id.add_sign);
        mManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!mManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Utility.AlertNoGPS(appContext);
        }

        mGPS = new GPSTracker(appContext);
        lat = mGPS.getLatitude();
        lang = mGPS.getLongitude();
        SetListenersforadd();
//        label_view = (AutoLabelUI) findViewById(R.id.label_view);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(appContext, LoginActivity.class));
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign:
                Validation();
                break;
        }
    }

    public void Validation() {
            if (((MaterialEditText) findViewById(R.id.email_sign)).getText().toString().equals("")) {
                ((MaterialEditText) findViewById(R.id.email_sign)).setError(getResources().getString(R.string.error_blank_email));
                (findViewById(R.id.email_sign)).requestFocus();

            } else if (((MaterialEditText) findViewById(R.id.uname_sign)).getText().toString().equals("")) {
                ((MaterialEditText) findViewById(R.id.uname_sign)).setError(getResources().getString(R.string.error_blank_username));
                (findViewById(R.id.uname_sign)).requestFocus();

            } else if (((MaterialEditText) findViewById(R.id.pass_sign)).getText().toString().equals("")) {
                ((MaterialEditText) findViewById(R.id.pass_sign)).setError(getResources().getString(R.string.error_blank_pass));
                (findViewById(R.id.pass_sign)).requestFocus();

            } else if (((MyAutoCompleteTextView) findViewById(R.id.add_sign)).getText().toString().equals("")) {
                ((MyAutoCompleteTextView) findViewById(R.id.add_sign)).setError(getResources().getString(R.string.error_blank_address));
                (findViewById(R.id.add_sign)).requestFocus();

            } else if (!Utility.isValidEmail(((MaterialEditText) findViewById(R.id.email_sign)).getText().toString())) {
                ((MaterialEditText) findViewById(R.id.email_sign)).setError(getResources().getString(R.string.error_email));
                (findViewById(R.id.email_sign)).requestFocus();

            } else if (!((CustomCheckBox) findViewById(R.id.terms_condition)).isChecked()) {
                Utility.showCrouton(getResources().getString(R.string.please_sel), SignupActivity.this);
            }else
                if (Utility.isConnectingToInternet(appContext)) {

                    CallApiWithResponse(((MaterialEditText) findViewById(R.id.uname_sign)).getText().toString(),
                            ((MaterialEditText) findViewById(R.id.email_sign)).getText().toString(),
                            ((MaterialEditText) findViewById(R.id.pass_sign)).getText().toString(),
                            ((MyAutoCompleteTextView) findViewById(R.id.add_sign)).getText().toString(), String.valueOf(lat), String.valueOf(lang));
                } else {
                    Utility.showCrouton(getResources().getString(R.string.error_internet), SignupActivity.this);
                }

    }

    public void CallApiWithResponse(String name, String email, String pass, String add,  String lati, String longi) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.REGISTRATION);
            jsonObject.put(ConstantsUrlKey.U_NAME, name);
            jsonObject.put(ConstantsUrlKey.EMAIL, email);
            jsonObject.put(ConstantsUrlKey.PASSWORD, pass);
            jsonObject.put(ConstantsUrlKey.ADD, add);
            jsonObject.put(ConstantsUrlKey.D_TYPE, ConstantsUrlKey.ANDROID);
            jsonObject.put(ConstantsUrlKey.LAT, lati);
            jsonObject.put(ConstantsUrlKey.LONG, longi);
            jsonObject.put(ConstantsUrlKey.U_TYPE, getIntent().getStringExtra(ConstantsUrlKey.U_TYPE));
            jsonObject.put(ConstantsUrlKey.GCM_ID, token);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server result ====> " + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), SignupActivity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                JSONObject userdetail = jobj.getJSONObject(ConstantsUrlKey.OBJECT);
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.USERID, userdetail.getString("userid"));
                                Utility.setSharedPreference(appContext, "login", "1");
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.U_TYPE, userdetail.getString("user_type"));
                               Utility.Alert(appContext,appContext.getResources().getString(R.string.checkmail));
                                SwitchActivity();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), SignupActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), SignupActivity.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
        }
    }

    public void SwitchActivity() {
        System.out.println("Server result====> " + "go");
        startActivity(new Intent(appContext, LoginActivity.class));
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception while dol", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
//            String key = "key=AIzaSyBurAiGZDz55TQmYPTMWsYYi85ko7AoJtI";
            String key =ConstantsUrlKey.KEY;

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input + "&" + types + "&" + sensor + "&" + key;

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;

            try {
                // Fetching the data from we service
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {
            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();
            try {
                jObject = new JSONObject(jsonData[0]);
                System.out.println("JSON OBJECT IS" + jObject);
                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            String[] from = new String[]{"description"};
            int[] to = new int[]{android.R.id.text1};
            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(appContext, result, android.R.layout.simple_list_item_1, from, to);
            // Setting the adapter
            address.setAdapter(adapter);

        }
    }

    private void SetListenersforadd() {
        address.setThreshold(1);
        address.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
    }
}
