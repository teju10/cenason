package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.costum.android.widget.LoadMoreListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import igs.com.cenason.Adapter.AdapterBookingHistory;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by sandeep on 22/2/17.
 */

public class Activity_BookingHistoryList extends AppCompatActivity {

    Context mContext;
    int page = 0;
    ResponseTask rt;
    LinkedList<JSONObject> cenashis_list = new LinkedList<>();
    AdapterBookingHistory listAdapter;
    Bundle b;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_bookinghistory_list);
        mContext = this;
        b = new Bundle();
        Init();
    }

    public void Init() {

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.book_history));

        b = getIntent().getExtras();
        listAdapter = new AdapterBookingHistory(mContext, R.layout.adap_user_bookinghistory_itemview, cenashis_list);
        ((LoadMoreListView) findViewById(R.id.bookhistory_list_view)).setAdapter(listAdapter);

        GetBookingHistory(b.getString("C_ID"), page);

        ((LoadMoreListView) findViewById(R.id.bookhistory_list_view)).setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (Utility.isConnectingToInternet(mContext)) {
                    page = page + 1;
                    System.out.println("page ====> " + page);
                    GetBookingHistory(b.getString("C_ID"), page);
                } else {
                    Toast.makeText(mContext, getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void SetAdapter() {
        listAdapter.notifyDataSetChanged();
    }

    public void GetBookingHistory(String cid, int pge) {
        try {
            JSONObject j = new JSONObject();
            j.put(ConstantsUrlKey.ACTION, "getCenasOrderDetails");
            j.put("cenas_id", cid);
            j.put("page", pge);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    ((LoadMoreListView) findViewById(R.id.bookhistory_list_view)).onLoadMoreComplete();
                    if (result == null) {
                        page = page - 1;
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_BookingHistoryList.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                JSONArray j = jobj.getJSONArray(ConstantsUrlKey.OBJECT);
                                for (int i = 0; i < j.length(); i++) {
                                    cenashis_list.add(j.getJSONObject(i));
                                }
                                SetAdapter();
                            } else {
                                page = page - 1;
                                if (page == 0) {
                                    Utility.ShowToastMessage(mContext, getResources().getString(R.string.no_cenas_found));
                                } else {
                                    Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_BookingHistoryList.this);
                                }
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_BookingHistoryList.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(mContext, My_cenas_Activity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
