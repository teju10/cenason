package igs.com.cenason;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

import igs.com.cenason.Adapter.UpgradePlan_Adapter;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 11/5/2016.
 */
public class Activity_upgrade_plan_list extends AppCompatActivity {

    private static final String TAG = "paymentExample";

    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     * <p>
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     * <p>
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID =
            "ASnNel0bqtTHzztVMCxho6Wrn5RGLyV4brfFhQz9HTl5zx4Y8SoTAgqPBbwV-KZeGwHpN-P9ynF8isoo";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            .merchantName("Example Merchant")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    Context appContext;
    Toolbar mToolbar;
    ResponseTask rt;
    ArrayList<JSONObject> packages = new ArrayList<>();
    SharedPreferences share;
    SharedPreferences.Editor edit;
    String amount, planid, duration;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cenas);
        appContext = this;
        Init();
    }

    private void Init() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);https://www.google.co.in/#q=vegfruit.in
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.upgrade_plan_list));
        (findViewById(R.id.fab_btn)).setVisibility(View.GONE);
        if (Utility.isConnectingToInternet(appContext)) {
            Getsubscriptionplan();
        } else {
            Utility.showCrouton(getResources().getString(R.string.error_internet), Activity_upgrade_plan_list.this);
        }
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
    }

    public void Getsubscriptionplan() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.UPGRADEPLANLIST);
            jsonObject.put("usertype", Utility.getSharedPreferences(appContext, ConstantsUrlKey.U_TYPE));
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_upgrade_plan_list.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                JSONArray j = jobj.getJSONArray(ConstantsUrlKey.OBJECT);
                                for (int i = 0; i < j.length(); i++) {
                                    packages.add(j.getJSONObject(i));
                                }
                                SetListView();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_upgrade_plan_list.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_upgrade_plan_list.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    private void SetListView() {
        ((ListView) findViewById(R.id.mycenas_list_view)).setAdapter(new UpgradePlan_Adapter(appContext,
                packages, R.layout.activity_upgrade_plan_item));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        startActivity(new Intent(appContext, SettingActivity.class));
        finish();
    }

    public void onBuyPressed(int pos) {
        try {
            amount = packages.get(pos).getString("amount");
            planid = packages.get(pos).getString("id");
            duration = packages.get(pos).getString("duration");
            PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
            Intent intent = new Intent(appContext, PaymentActivity.class);
            intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
            intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
            startActivityForResult(intent, REQUEST_CODE_PAYMENT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(amount),
                "USD", "paypal", paymentIntent);
    }

    protected void displayResultText(String result) {
        Utility.ShowToastMessage(appContext, result);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        Log.i(TAG, "response ===== " + confirm.toJSONObject().getJSONObject("response").toString());
                        Log.i(TAG, "response ===== " + confirm.toJSONObject().getJSONObject("response").getString("create_time"));
                        Log.i(TAG, "response ===== " + confirm.toJSONObject().getJSONObject("response").getString("id"));
                        Log.i(TAG, "response ===== " + confirm.toJSONObject().getJSONObject("response").getString("state"));
                        Log.i(TAG, confirm.getPayment().toJSONObject().getString("amount"));
                        Log.i(TAG, confirm.getPayment().toJSONObject().getString("currency_code"));
                        Log.i(TAG, confirm.getPayment().toJSONObject().getString("short_description"));

                        if (confirm.toJSONObject().getJSONObject("response").getString("state").equalsIgnoreCase("approved")) {
                            if (Utility.isConnectingToInternet(appContext)) {
                                Purchaseupgradeplan(confirm.toJSONObject().getJSONObject("response").getString("id"),
                                        confirm.getPayment().toJSONObject().getString("currency_code"), "complete", "Paypal");
                            } else {
                                Utility.ShowToastMessage(appContext, getResources().getString(R.string.error_internet));
                            }
                        } else {
                            Utility.ShowToastMessage(appContext, "Payment Not Done Please Try Again Later");
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        displayResultText("Future Payment code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("FuturePaymentExample", "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    public void Purchaseupgradeplan(String transactionid, String currency,
                                    String status, String paymentmode) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.UPGRADEPLANPURCHASE);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.TRANSSACTION_ID, transactionid);
            System.out.println("transactionid ====> " + transactionid);
            jsonObject.put(ConstantsUrlKey.AMOUNT, amount);
            System.out.println("Amount ====> " + amount);
            jsonObject.put(ConstantsUrlKey.CURRENCY, currency);
            System.out.println("Currency ====> " + currency);
            jsonObject.put(ConstantsUrlKey.PLANID, planid);
            System.out.println("Planid ====> " + planid);
            jsonObject.put(ConstantsUrlKey.DURATION, duration);
            System.out.println("Duration ====> " + duration);
            jsonObject.put(ConstantsUrlKey.STATUS, status);
            System.out.println("Status ====> " + status);
            jsonObject.put(ConstantsUrlKey.PAYMENTMODE, paymentmode);
            System.out.println("Payment type ====> " + paymentmode);
            jsonObject.put("usertype", Utility.getSharedPreferences(appContext, ConstantsUrlKey.U_TYPE));
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("SERVER RESULT" + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_upgrade_plan_list.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                JSONObject j = jobj.getJSONObject(ConstantsUrlKey.OBJECT);
                                    Utility.ShowToastMessage(appContext, getResources().getString(R.string.plan_purchade));
                                    Utility.setSharedPreference(appContext, j.getString("user_type"), ConstantsUrlKey.U_TYPE);
                                    System.out.println("User type ========> " + jobj.getString("user_type"));

                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_upgrade_plan_list.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_upgrade_plan_list.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
