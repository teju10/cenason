package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import igs.com.cenason.Adapter.MycenaslistAdapter;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Shoaib on 26-Aug-16.
 */
public class My_cenas_Activity extends AppCompatActivity implements View.OnClickListener {

    Context appContext;
    Toolbar mToolbar;
    ResponseTask rt;
    LinkedList<JSONObject> mycenaslist = new LinkedList<>();
    MycenaslistAdapter listAdapter;
    SharedPreferences share;
    SharedPreferences.Editor edit;
String PlanCheck = "";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cenas);
        appContext = this;
        Init();
    }

    private void Init() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.my_cenas));
        (findViewById(R.id.fab_btn)).setOnClickListener(this);
        if (Utility.isConnectingToInternet(appContext)) {
            getmycenaslist();
        } else {
            Utility.showCroutonWarn(getResources().getString(R.string.error_internet), My_cenas_Activity.this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_btn:
                SwitchActivity();
                break;
        }
    }

    private void SwitchActivity() {
        System.out.println("PLANCHECK -------"+Utility.getSharedPreferences(appContext, ConstantsUrlKey.UPGRADEPLANCCHECK));
        if (Utility.getSharedPreferences(appContext, ConstantsUrlKey.UPGRADEPLANCCHECK).equals("0")) {
            startActivity(new Intent(appContext, Activity_select_cenas_type.class));
            finish();
        } else {
            Utility.ShowToastMessage(appContext, appContext.getResources().getString(R.string.upgradeplan_noti));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext, HomeActivity.class));
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    public void getmycenaslist() {
        System.out.println("user id === > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.MYCENASLIST);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), My_cenas_Activity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                for (int i = 0; i < jobj.getJSONArray(ConstantsUrlKey.OBJECT).length(); i++) {
                                    mycenaslist.add(jobj.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i));
                                }
                                listAdapter = new MycenaslistAdapter(appContext, mycenaslist);
                                ((ListView) findViewById(R.id.mycenas_list_view)).setAdapter(listAdapter);
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), My_cenas_Activity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), My_cenas_Activity.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void deletemycenas(final int pos) {
        try {
            String id = mycenaslist.get(pos).getString("id");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.DELETECENAS);
            jsonObject.put(ConstantsUrlKey.CENASID, id);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), My_cenas_Activity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                // Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), My_cenas_Activity.this);
                                mycenaslist.remove(pos);
                                listAdapter.MyListNotify(mycenaslist);
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), My_cenas_Activity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), My_cenas_Activity.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void editCenas(final int pos) {
        startActivity(new Intent(appContext, Activity_Edit_MyCenas.class)
                .putExtra(ConstantsUrlKey.CENAS_DATA, mycenaslist.get(pos).toString()));
    }

    public void infoCenas(final int pos) {
        startActivity(new Intent(appContext, Activity_Cenas_info.class)
                .putExtra(ConstantsUrlKey.CENAS_DATA, mycenaslist.get(pos).toString()));
    }
}
