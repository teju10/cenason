package igs.com.cenason;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import igs.com.cenason.CustomWidget.CustomButton;
import igs.com.cenason.CustomWidget.CustomRadioButton;
import igs.com.cenason.PackForPermissions.AfterPermissionGranted;
import igs.com.cenason.PackForPermissions.EasyPermissions;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.GPSTracker;
import igs.com.cenason.Utilities.Utility;
import retrofit2.Call;

/**
 * Created by LakhanPatidar on 28-Jul-16.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        FacebookCallback<LoginResult>, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.
                OnConnectionFailedListener, EasyPermissions.PermissionCallbacks {
    private static final int SIGN_IN_CODE = 0;
    private static final int PROFILE_PIC_SIZE = 200;
    android.support.v7.app.AlertDialog alertDialog;
    ResponseTask rt;
    CallbackManager callbackManager;
    AccessToken f_token;
    GoogleApiClient google_api_client;
    GoogleApiAvailability google_api_availability;
    TwitterSession t_session;
    String twitter_id, twitter_name, t_full_name, t_profile_image;
    private Context appContext;
    private boolean is_signInBtn_clicked = false;
    private int request_code;
    private boolean is_intent_inprogress;
    private ConnectionResult connection_result;
    private TwitterAuthClient client;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    double lat, lang;
    GPSTracker mGPS;
    LocationManager mManager;
    Dialog selectDialog, providerDialog;
    RadioGroup dialog_register_select_radio_group, dialog_provider_select_radio_group;
    CustomRadioButton dialog_register_select_radio_regular, dialog_register_select_radio_provider,
            dialog_provider_select_radio_freelance, dialog_provider_select_radio_companies, dialog_provider_select_radio_public;
    CustomButton dialog_register_select_btn, dialog_provider_select_btn;
    String userType = "";
    LinearLayoutManager llm;
    String gcmid;
    private static final int RC_LOCATION_CONTACTS_PERM = 1;
    private static final String TAG = "LoginActivity";
    String token;
    private static final String REGISTER_URL = "https://infograins.com/INFO01/cenason/api/api.php?";
    String tag_json_obj = "Login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = this;
        setContentView(R.layout.activity_login);

        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
       /* GCMRegistrar.checkDevice(this);
        GCMRegistrar.checkManifest(this);
        GCMRegistrar.register(appContext, NotificationUtility.SENDER_ID);*/
        // gcmid = share.getString("gcmid", "");
        Init();
        printKeyHash();
    }

    private void Init() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        (findViewById(R.id.btn_signup)).setOnClickListener(this);
        (findViewById(R.id.btn_f_pass)).setOnClickListener(this);
        (findViewById(R.id.btn_login)).setOnClickListener(this);
        (findViewById(R.id.gp_img)).setOnClickListener(this);
        (findViewById(R.id.tw_img)).setOnClickListener(this);
        (findViewById(R.id.fb_img)).setOnClickListener(this);
        locationAndContactsTask();
        client = new TwitterAuthClient();
        FacebookSdk.sdkInitialize(getApplicationContext());
        disconnectFromFacebook();
        // AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        System.out.println("access token is" + AccessToken.getCurrentAccessToken());
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }
        LoginManager.getInstance().registerCallback(callbackManager, this);
        buidNewGoogleApiClient();

        gPlusSignOut();
        mManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!mManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Utility.AlertNoGPS(appContext);
        }

        mGPS = new GPSTracker(appContext);
        lat = mGPS.getLatitude();
        lang = mGPS.getLongitude();
        token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("token is" + token);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_signup:
                startActivity(new Intent(appContext, Selection_register.class));
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                finish();
                break;
            case R.id.btn_f_pass:
                startActivity(new Intent(appContext, ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                finish();
                break;
            case R.id.btn_login:
                Validation();
                break;
            case R.id.fb_img:
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email"));
                break;
            case R.id.tw_img:
                TwitterLogin();
                //Utility.showCrouton(getResources().getString(R.string.comming_soon), LoginActivity.this);
                break;
            case R.id.gp_img:
                gPlusSignIn();
                break;
        }
    }

    //ToDo Google Start

    private void buidNewGoogleApiClient() {
        google_api_client = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
    }

    protected void onStart() {
        super.onStart();
        google_api_client.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            try {
                google_api_availability.getErrorDialog(this, result.getErrorCode(), request_code).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

        if (!is_intent_inprogress) {
            connection_result = result;
            if (is_signInBtn_clicked) {
                resolveSignInError();
            }
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        is_signInBtn_clicked = false;
        // Get user's information and set it into the layout
        getProfileInfo();
        // Update the UI after signin
        changeUI(true);
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        google_api_client.connect();
        changeUI(false);
    }

    /*
      Sign-in into the Google + account
     */
    private void gPlusSignIn() {
        if (!google_api_client.isConnecting()) {
            Log.e("user connected", "connected");
            is_signInBtn_clicked = true;
            // progress_dialog.show();
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            resolveSignInError();
        }
        if (google_api_client.isConnected()) {
            gPlusRevokeAccess();
        }
    }

    /*
      Method to resolve any signin errors
     */
    private void resolveSignInError() {
        try {
            if (!(connection_result == null)) {
                if (connection_result.hasResolution()) {
                    try {
                        is_intent_inprogress = true;
                        connection_result.startResolutionForResult(this, SIGN_IN_CODE);
                        Log.d("resolve error", "sign in error resolved");
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                        Log.e("IntentSender", "IntentSender.SendIntentException == " + e);
                        is_intent_inprogress = false;
                        google_api_client.connect();
                    }
                }
            } else {
                //progress_dialog.dismiss();
                Utility.HideDialog();
                google_api_availability.getErrorDialog(this, connection_result.getErrorCode(), request_code).show();
            }
        } catch (Exception e) {
            // progress_dialog.dismiss();
            Utility.HideDialog();
            e.printStackTrace();
        }
    }

    private void gPlusSignOut() {
        if (google_api_client.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(google_api_client);
            google_api_client.disconnect();
            google_api_client.connect();
            changeUI(false);
        }
    }

    /*
     Revoking access from Google+ account
     */
    private void gPlusRevokeAccess() {
        if (google_api_client.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(google_api_client);
            Plus.AccountApi.revokeAccessAndDisconnect(google_api_client).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status arg0) {
                    Log.d("MainActivity", "User access revoked!");
                    buidNewGoogleApiClient();
                    google_api_client.connect();
                    changeUI(false);
                }
            });
        }
    }

    /*
     get user's information name, email, profile pic,Date of birth,tag line and about me
     */
    private void getProfileInfo() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(google_api_client) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(google_api_client);
                setPersonalInfo(currentPerson);
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     By default the profile pic url gives 50x50 px image.
     If you need a bigger image we have to change the query parameter value from 50 to the size you want
    */

    /*
     set the User information into the views defined in the layout
     */

    private void setPersonalInfo(Person currentPerson) {
        int persongender = 0;
        String personName = currentPerson.getDisplayName();
        String personPhotoUrl = currentPerson.getImage().getUrl();
        String email = Plus.AccountApi.getAccountName(google_api_client);
        String Name = currentPerson.getNickname();
        personPhotoUrl = personPhotoUrl.substring(0, personPhotoUrl.length() - 2) + PROFILE_PIC_SIZE;
        if (currentPerson.hasGender()) {
            // it's not guaranteed
            persongender = currentPerson.getGender();
        }
        Log.e(TAG, "Person info is" + personPhotoUrl + "\n" + "personname->" + personName + "\n"
                + "email==>" + email + "\n" + "id" + currentPerson.getId());
        System.out.println("Person info is-------------------------->>--------------" + personPhotoUrl + "\n" + Name);
        // progress_dialog.dismiss();
        Utility.HideDialog();
        String imageurl = personPhotoUrl;
        if (!imageurl.equals("")) {
            int lastindex = personPhotoUrl.length();
            imageurl = personPhotoUrl.substring(8, lastindex);
        }
        gPlusSignOut();
        CallSocialApiWithResponse(email, personName, imageurl,
                currentPerson.getId(), ConstantsUrlKey.GOOGLE_KEY);
    }

    /*
     Show and hide of the Views according to the user login status
     */

    private void setProfilePic(String profile_pic) {
        profile_pic = profile_pic.substring(0, profile_pic.length() - 2) + PROFILE_PIC_SIZE;
    }

    private void changeUI(boolean signedIn) {

    }
    //TODO Google Login Ends Here

    private void TwitterLogin() {
        client.authorize(LoginActivity.this, new Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> result) {
                Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
                t_session = Twitter.getSessionManager().getActiveSession();
                twitter_name = result.data.getUserName();
                twitter_id = result.data.getUserId() + "";
//                TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();

                Call<User> call = Twitter.getApiClient(t_session).getAccountService()
                        .verifyCredentials(true, false);
                call.enqueue(new Callback<User>() {
                    @Override
                    public void failure(TwitterException e) {
                        Utility.HideDialog();
                        Toast.makeText(appContext, "Twitter login error ", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void success(Result<User> userResult) {
                        User user = userResult.data;
                        //t_email = userResult.data.email;
                        t_full_name = userResult.data.name;
                        t_profile_image = userResult.data.profileImageUrl;
                        t_profile_image = t_profile_image.replace("_normal", "");
                        Log.e("Twitter details", "twitter name = " + twitter_name);
                        Log.e("Twitter details", "twitter id = " + twitter_id);
                        Log.e("Twitter details", "twitter full name = " + t_full_name);
                        Log.e("Twitter details", "twitter profile image = " + t_profile_image);
                        CallSocialApiWithResponse("nileshkansal1@gmail.com", t_full_name, t_profile_image, twitter_id, ConstantsUrlKey.TWITTER_KEY);
                    }
                });

            }

            @Override
            public void failure(TwitterException e) {
                Toast.makeText(appContext, "Twitter login failed ", Toast.LENGTH_SHORT).show();
                Utility.HideDialog();
            }
        });

               /* Call<User> call = Twitter.getApiClient(t_session).getAccountService()
                        .verifyCredentials(true, false);
                call.enqueue(new Callback<User>() {
                    @Override
                    public void failure(TwitterException e) {
                        //If any error occurs handle it here
                    }

                    @Override
                    public void success(Result<User> userResult) {
                        //If it succeeds creating a User object from userResult.data
                        User user = userResult.data;
                        //t_email = userResult.data.email;
                        t_full_name = userResult.data.name;
                        t_profile_image = userResult.data.profileImageUrl;
                        t_profile_image = t_profile_image.replace("_normal", "");
                        Log.e("Twitter details", "twitter name = " + twitter_name);
                        Log.e("Twitter details", "twitter id = " + twitter_id);
                        Log.e("Twitter details", "twitter full name = " + t_full_name);
                        Log.e("Twitter details", "twitter profile image = " + t_profile_image);
                        CallSocialApiWithResponse("nileshkansal1@gmail.com", t_full_name, t_profile_image, twitter_id, ConstantsUrlKey.TWITTER_KEY);

                    }
                });*/
              /*  twitterApiClient.getAccountService().verifyCredentials(true, false, new Callback<User>() {

                    @Override
                    public void success(Result<User> userResult) {
                        System.out.println("Twitter email------------------------"+ userResult.data.email+userResult.data.name);
                        t_email = userResult.data.email;
                        t_full_name = userResult.data.name;
                        t_profile_image = userResult.data.profileImageUrl;
                        t_profile_image = t_profile_image.replace("_normal", "");
                        Log.e("Twitter details", "twitter name = " + twitter_name);
                        Log.e("Twitter details", "twitter id = " + twitter_id);
                        Log.e("Twitter details", "twitter full name = " + t_full_name);
                        Log.e("Twitter details", "twitter profile image = " + t_profile_image);
                        CallSocialApiWithResponse("nileshkansal1@gmail.com", t_full_name, t_profile_image, twitter_id, ConstantsUrlKey.TWITTER_KEY);
                    }

                    @Override
                    public void failure(TwitterException e) {
                        Toast.makeText(appContext, "Twitter Exception raised ", Toast.LENGTH_SHORT).show();
                        Utility.HideDialog();
                    }
                });
            }*/
    }

    public void SelectDialog() {
        selectDialog = new Dialog(appContext);
        selectDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectDialog.setContentView(R.layout.dialog_register_select);
        selectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog_register_select_radio_group = (RadioGroup) selectDialog.findViewById(R.id.dialog_register_select_radio_group);
        dialog_register_select_radio_regular = (CustomRadioButton) selectDialog.findViewById(R.id.dialog_register_select_radio_regular);
        dialog_register_select_radio_provider = (CustomRadioButton) selectDialog.findViewById(R.id.dialog_register_select_radio_provider);

        dialog_register_select_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (dialog_register_select_radio_regular.isChecked()) {
                    userType = "1";
                } else if (dialog_register_select_radio_provider.isChecked()) {
                    userType = "0";
                }
            }
        });

        dialog_register_select_btn = (CustomButton) selectDialog.findViewById(R.id.dialog_register_select_btn);

        dialog_register_select_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userType.equals("1")) {
                    selectDialog.dismiss();
                    Updateuserid("1");
                } else if (userType.equals("0")) {
                    selectDialog.dismiss();
                    ProviderDialog();
                } else {
                    Utility.ShowToastMessage(appContext, getResources().getString(R.string.please_selcect_one));
                }
            }
        });
        selectDialog.show();
    }

    public void ProviderDialog() {
        providerDialog = new Dialog(appContext);
        providerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        providerDialog.setContentView(R.layout.dialog_provider_select);
        providerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog_provider_select_radio_group = (RadioGroup) providerDialog.findViewById(R.id.dialog_provider_select_radio_group);
        dialog_provider_select_radio_freelance = (CustomRadioButton) providerDialog.findViewById(R.id.dialog_provider_select_radio_freelance);
        dialog_provider_select_radio_companies = (CustomRadioButton) providerDialog.findViewById(R.id.dialog_provider_select_radio_companies);
        dialog_provider_select_radio_public = (CustomRadioButton) providerDialog.findViewById(R.id.dialog_provider_select_radio_public);
        dialog_provider_select_btn = (CustomButton) providerDialog.findViewById(R.id.dialog_provider_select_btn);

        dialog_provider_select_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (dialog_provider_select_radio_freelance.isChecked()) {
                    userType = "3";
                } else if (dialog_provider_select_radio_companies.isChecked()) {
                    userType = "2";
                } else if (dialog_provider_select_radio_public.isChecked()) {
                    userType = "4";
                }
            }
        });

        dialog_provider_select_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userType.equals("2")) {
                    providerDialog.dismiss();
                    Updateuserid("2");
                } else if (userType.equals("3")) {
                    providerDialog.dismiss();
                    Updateuserid("3");
                } else if (userType.equals("4")) {
                    providerDialog.dismiss();
                    Updateuserid("4");
                } else {
                    Utility.ShowToastMessage(appContext, getResources().getString(R.string.please_selcect_one));
                    //providerDialog.dismiss();
                }
            }
        });
        providerDialog.show();
    }

    public void Validation() {
        if (!((MaterialEditText) findViewById(R.id.uname_log)).getText().toString().equals("")) {
            if (!((MaterialEditText) findViewById(R.id.pass_log)).getText().toString().equals("")) {
                if (Utility.isConnectingToInternet(appContext)) {
                    CallApiWithResponse(((MaterialEditText) findViewById(R.id.uname_log)).getText().toString(),
                            ((MaterialEditText) findViewById(R.id.pass_log)).getText().toString());
                    //Login();
                } else {
                    Utility.showCrouton(getResources().getString(R.string.error_internet), LoginActivity.this);
                }
            } else {
                ((MaterialEditText) findViewById(R.id.pass_log)).setError(getResources().getString(R.string.error_blank_pass));
            }
        } else {
            ((MaterialEditText) findViewById(R.id.uname_log)).setError(getResources().getString(R.string.error_blank_username));
        }
    }

    public void CallApiWithResponse(String email, String pass) {
        System.out.println("result normal");
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.LOGIN);
            jsonObject.put(ConstantsUrlKey.EMAIL, email);
            jsonObject.put(ConstantsUrlKey.PASSWORD, pass);
            jsonObject.put(ConstantsUrlKey.GCM_ID, token);
            jsonObject.put(ConstantsUrlKey.D_TYPE, ConstantsUrlKey.ANDROID);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), LoginActivity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {

                                JSONObject userdetail = jobj.getJSONObject(ConstantsUrlKey.OBJECT);
                                Utility.setSharedPreference(appContext, "login", "1");
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.USERID, userdetail.getString("userid"));
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.U_TYPE, userdetail.getString("user_type"));
                                SwitchActivity();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), LoginActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), LoginActivity.this);
                        }
                    }

                }
            });
            rt.execute();
        } catch (JSONException e) {

        }
    }

    public void SwitchActivity() {
        if (Utility.getSharedPreferences(appContext, ConstantsUrlKey.U_TYPE).equals("4")) {
            startActivity(new Intent(appContext, My_cenas_Activity.class));
            finish();
        } else {
            startActivity(new Intent(appContext, HomeActivity.class));
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            finish();
        }
    }

    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("igs.com.cenason", PackageManager.GET_SIGNATURES); //replace com.demo with your package name
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("DeveloperKeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                MessageDigest mda = MessageDigest.getInstance("SHA-1");
                mda.update(signature.toByteArray());
                Log.d("releseKeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.d("KeyHash:", e.toString());
        }
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        f_token = loginResult.getAccessToken();
        Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                //Start new activity or use this info in your project.
                try {
                    System.out.println("facebook img  =  >" + "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large");
                    CallSocialApiWithResponse(object.getString("email"),
                            object.getString("name"), "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large", object.getString("id"), ConstantsUrlKey.FACEBOOK_KEY);

                     /*SocialLoginTask(object.getString("name"), object.getString("email"),
                           object.getString("name"), "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large",
                            object.getString("id"), ConstantsKey.PROVIDER_FACEBOOK, Utility.getSharedPreferences(appContext, "GCMID"));*/
                } catch (Exception e) {
                    Utility.HideDialog();
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, email, gender, birthday");
        request.setAccessToken(f_token);
        request.setParameters(parameters);
        request.executeAsync();
    }

    /* Facebook Access Token Clear Permanently  */

    public void disconnectFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
            }
        }).executeAsync();
    }

    @Override
    public void onCancel() {
        Toast.makeText(appContext, "Facebook login Canceled", Toast.LENGTH_SHORT).show();
        Utility.HideDialog();
    }

    @Override
    public void onError(FacebookException error) {
        error.printStackTrace();
        Log.e("FacebookException", "FacebookException ====>" + error);
        Toast.makeText(appContext, "Facebook login Error", Toast.LENGTH_SHORT).show();
        Utility.HideDialog();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        client.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGN_IN_CODE) {
            request_code = requestCode;
            if (resultCode != RESULT_OK) {
                is_signInBtn_clicked = false;
                Utility.HideDialog();
            }
            is_intent_inprogress = false;
            if (!google_api_client.isConnecting()) {
                google_api_client.connect();
            }
        }
    }


    //Todo Volley Async
 /*   public void Login() {
        Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Utility.HideDialog();
                        System.out.println("Result============" + response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utility.HideDialog();
                Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getPostParams() throws AuthFailureError {
                Utility.HideDialog();
                Map<String, String> params = new HashMap<String, String>();
                params.put(ConstantsUrlKey.EMAIL, ((MaterialEditText) findViewById(R.id.uname_log)).getText().toString());
                params.put(ConstantsUrlKey.PASSWORD, ((MaterialEditText) findViewById(R.id.pass_log)).getText().toString());
                params.put(ConstantsUrlKey.GCM_ID, token);
                params.put(ConstantsUrlKey.D_TYPE, ConstantsUrlKey.ANDROID);
                System.out.println("Params=========== " + params);
                return super.getPostParams();
            }

        };
        System.out.println("stringRequest =========" + stringRequest);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }*/

    public void CallSocialApiWithResponse(String email, String u_name, String propic, String o_id, String o_pro) {
        System.out.println("result social" + o_pro);
        //  profile_image=cv&outhid=54v&outh_provider=facebook&device_id=1&device_type=1&lat=1&long=1
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.S_LOGIN);
            jsonObject.put(ConstantsUrlKey.EMAIL, email);
            jsonObject.put(ConstantsUrlKey.U_NAME, u_name);
            jsonObject.put(ConstantsUrlKey.PRO_IMG, propic);
            jsonObject.put(ConstantsUrlKey.OUTH_ID, o_id);
            jsonObject.put(ConstantsUrlKey.OUTH_PROVIDER, o_pro);
            jsonObject.put(ConstantsUrlKey.GCM_ID, token);
            jsonObject.put(ConstantsUrlKey.D_TYPE, ConstantsUrlKey.ANDROID);
            jsonObject.put(ConstantsUrlKey.LAT, String.valueOf(lat));
            jsonObject.put(ConstantsUrlKey.LONG, String.valueOf(lang));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), LoginActivity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                JSONObject userdetail = jobj.getJSONObject(ConstantsUrlKey.OBJECT);
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.USERID, userdetail.getString("userid"));
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.LOGINTYPE, "social_login");
                                if (userdetail.getString("user_type").equals("0")) {
                                    SelectDialog();
                                } else {
                                    Utility.setSharedPreference(appContext, ConstantsUrlKey.U_TYPE, userdetail.getString("user_type"));
                                    SwitchActivity();
                                }

                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), LoginActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), LoginActivity.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void Updateuserid(String usertype) {
        System.out.println("user id=== >" + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.UPADTEUSERTYPE);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.U_TYPE, usertype);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), LoginActivity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                JSONObject userdetail = jobj.getJSONObject(ConstantsUrlKey.OBJECT);
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.USERID, userdetail.getString("userid"));
                                Utility.setSharedPreference(appContext, "login", "1");
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.U_TYPE, userdetail.getString("user_type"));
                                /*if (userdetail.getString("is_category").equals("0")) {
                                    CategoryDialog();
                                } else {*/
                                SwitchActivity();
                                //    }

                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), LoginActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), LoginActivity.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    @AfterPermissionGranted(RC_LOCATION_CONTACTS_PERM)
    public void locationAndContactsTask() {
        String[] perms = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.
                READ_CONTACTS, Manifest.permission.
                WRITE_CONTACTS, android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Have permissions, do the thing!
            // Toast.makeText(this, "TODO: Location and Contacts things", Toast.LENGTH_LONG).show();
        } else {
            // Ask for permissions
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location_contacts),
                    RC_LOCATION_CONTACTS_PERM, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());
    }
}
