package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.CustomWidget.SweetAlertDialog;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 11/10/2016.
 */
public class Activity_cancle_order extends AppCompatActivity implements View.OnClickListener {

    Context appContext;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    Toolbar mToolbar;
    String data, order_id;
    ResponseTask rt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        appContext = this;
        Init();
    }

    private void Init() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.order_detail));
        GetDate();
        (findViewById(R.id.cancle_cenas_btn)).setOnClickListener(this);
    }

    private void GetDate() {
        try {
            data = getIntent().getStringExtra(ConstantsUrlKey.CENAS_DATA);
            JSONObject jobj = new JSONObject(data);
            System.out.println("DATA ================> " + data);
            order_id = jobj.getString("order_id");
            ((CustomTextView) findViewById(R.id.orderhist_cenas_name)).setText(jobj.getString("name"));
            ((CustomTextView) findViewById(R.id.orderhist_cenas_price)).setText("$ " + jobj.getString("price"));
            ((CustomTextView) findViewById(R.id.orderhist_cenas_number)).setText(jobj.getString("contact_number"));
            ((CustomTextView) findViewById(R.id.orderhist_cenas_quantity)).setText(jobj.getString("quantity"));
            ((CustomTextView) findViewById(R.id.orderhist_cenas_date_time)).setText(parseDateToyyyyMMdd(jobj.getString("order_date")) + " , " + Time(jobj.getString("order_time")));
            Glide.with(appContext).load(jobj.getString("cenas_image")).into(((ImageView) findViewById(R.id.mycenas_image_view)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String parseDateToyyyyMMdd(String time) {
        String outputPattern = "dd/MM/yyyy";
        String inputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        String dayOfTheWeek = null;
        try {
            date = inputFormat.parse(time);
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            dayOfTheWeek = sdf.format(date);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dayOfTheWeek + " " + str;
    }

    public String Time(String time) {
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
        String str = null;
        try {
            Date date = parseFormat.parse(time);
            str = displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void Sweetdialog() {
        new SweetAlertDialog(appContext)
                .setTitleText("Cancel Order ?")
                .setContentText("Do you want to cancel this order")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                }).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                Cancleorder();
                sweetAlertDialog.dismissWithAnimation();
            }
        })
                .show();
    }

    public void Cancleorder() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.CANCELORDER);
            jsonObject.put(ConstantsUrlKey.ORDERID, order_id);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_cancle_order.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                Utility.showCrouton(getResources().getString(R.string.order_cancle), Activity_cancle_order.this);
                                finish();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_cancle_order.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_cancle_order.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancle_cenas_btn:
                Sweetdialog();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext, Activity_order_history.class));
        finish();
    }
}
