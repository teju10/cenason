package igs.com.cenason;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import igs.com.cenason.Utilities.GPSTracker;
import igs.com.cenason.Utilities.PathJSONParser;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Tejas on 21/2/17.
 */

public class Activity_ViewCenas_OnMap extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    GoogleMap googleMap;
    Context appContext;
    LocationManager mManager;
    GPSTracker mGPS;
    double lati, longi, tolant, tolong;
    SupportMapFragment fm;
    Bundle b;
    ImageView map_imgMyLocation;
    ArrayList<LatLng> mpoints = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewcanas_onmap);
        appContext = this;

        map_imgMyLocation = (ImageView) findViewById(R.id.map_imgMyLocation);
        map_imgMyLocation.setOnClickListener(this);

        Init();
    }

    private void Init() {
        mManager = (LocationManager) appContext.getSystemService(Context.LOCATION_SERVICE);
        if (!mManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Utility.AlertNoGPS(appContext);
        }
        mGPS = new GPSTracker(appContext);
        lati = mGPS.getLatitude();
        longi = mGPS.getLongitude();

        googleMapCheck();

        b = new Bundle();
        b = getIntent().getExtras();
        tolant = Double.parseDouble(b.getString("lat"));
        tolong = Double.parseDouble(b.getString("long"));
    }

    private void googleMapCheck() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(appContext);
// Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.viewcnas_map);
// Getting GoogleMap object from the fragment
            fm.getMapAsync(this);
// Enabling MyLocation Layer of Google Map
// Drawing marker on the map
        }
    }

    private void getMyLocation() {
        LatLng latLng = new LatLng(lati, longi);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
        if (googleMap != null) {
            googleMap.animateCamera(cameraUpdate);
        }
    }

    private void getLatLong() {
        System.out.println("GETLatLong============>>>>>> " + tolant + tolong);
        LatLng l = new LatLng(lati, longi);
        googleMap.addMarker(new MarkerOptions().position(l)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));

        LatLng source = new LatLng(lati, longi);
        LatLng des = new LatLng(tolant, tolong);

        mpoints.add(source);
        mpoints.add(des);
        drawRoute();
    }

    @Override
    public void onMapReady(GoogleMap mgoogleMap) {
        googleMap = mgoogleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);

        // Enable / Disable Zooming Controls
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        // Enable / Disable My Location Button
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        // Enable / Disable Compass Icon
        googleMap.getUiSettings().setCompassEnabled(true);

        // Enable / Disable Rotate Gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);

        // Enable / Disable Zooming Functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        getMyLocation();
        if (Utility.isConnectingToInternet(appContext)) {
            getLatLong();
        } else {
            Utility.showCrouton(getResources().getString(R.string.error_internet), Activity_ViewCenas_OnMap.this);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.map_imgMyLocation:
                getMyLocation();
        }
    }

    private void drawRoute() {
        System.out.println("Arraylst =====" + mpoints);
        MarkerOptions options = new MarkerOptions();

        options.position(mpoints.get(0));
        options.position(mpoints.get(1));

        if (mpoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else if (mpoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }

        // Add new marker to the Google Map Android API V2
        googleMap.addMarker((options).title(b.getString("cname") + ","
                + b.getString("address")));

        if (mpoints.size() >= 2) {
            LatLng origin = mpoints.get(0);
            LatLng dest = mpoints.get(1);

            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(origin, dest);
            DownloadTask downloadTask = new DownloadTask();

            downloadTask.execute(url);
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        System.out.println("jaane kitneeeee parameter hwugwh>>>>" + parameters);
        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {
            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            try {
                // Traversing through all the routes
                for (int i = 0; i < routes.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = routes.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        if (j == 0) {    // Get distance from the list
                            distance = (String) point.get("distance");
                            continue;
                        } else if (j == 1) { // Get duration from the list
                            duration = (String) point.get("duration");
                            continue;
                        }

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }
                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(5);
                    lineOptions.color(Color.RED);
                }

                    Log.e("DURATION-->>>>>>>>", "Distance:" + distance + ", Duration:" + duration);
                    // Drawing polyline in the Google Map for the i-th route
                    googleMap.addPolyline(lineOptions);

              /*  onStartUpdate();*/
                //startActivity(new Intent(appContext, LocationUpdateService.class));

            } catch (Exception e) {
                e.printStackTrace();
//                Utility.ShowToastMessage(appContext, "Please enter proper address");
                Utility.showCroutonInfo("No route available!", Activity_ViewCenas_OnMap.this);
                Utility.ShowToastMessage(appContext,"No route available!");
            }

        }
    }
}
