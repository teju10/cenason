package igs.com.cenason;

import android.content.Context;
import android.media.MediaPlayer;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.MediaController;

import igs.com.cenason.Utilities.VodView;


/**
 * Created by Tejas on 27/2/17.
 */

public class Activity_VideoViewZoom extends AppCompatActivity {

    Context mContext;
    static final int MIN_WIDTH = 100;
    FrameLayout.LayoutParams mRootParam;
    VodView vodview;
    private ScaleGestureDetector mScaleGestureDetector;
    private GestureDetector mGestureDetector;
    Bundle b;
    MediaController mdc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_c_detail_videozoom);

        mContext = this;
        vodview = (VodView) findViewById(R.id.vodview);
        mRootParam = (FrameLayout.LayoutParams) ((View) findViewById(R.id.root_view)).getLayoutParams();

        b = new Bundle();
        b = getIntent().getExtras();

        /*mdc = new MediaController(mContext);
        mdc.setAnchorView(vodview);
        vodview.setMediaController(mdc);*/

        if (!b.getString("VideoUrl").equals("") && !b.getString("VideoUrl").equals(null)) {
            Uri video = Uri.parse(b.getString("VideoUrl"));
            vodview.setVideoURI(video);
          /*  vodview.requestFocus();
            vodview.setKeepScreenOn(true);*/
        }

        findViewById(R.id.video_pb).setVisibility(View.VISIBLE);

        if (Build.VERSION.SDK_INT > 16) {
            vodview.setOnInfoListener(onInfoToPlayStateListener);

        /*vodview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                       *//* findViewById(R.id.video_pb).setVisibility(View.GONE);
                        mp.start();*//*

                        if(MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START = width){

                        }
                    }
                });
            }
        });
*/
        }
        mScaleGestureDetector = new ScaleGestureDetector(this, new MyScaleGestureListener());
        mGestureDetector = new GestureDetector(this, new MySimpleOnGestureListener());

        vodview.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mGestureDetector.onTouchEvent(event);
                mScaleGestureDetector.onTouchEvent(event);
                return true;
            }
        });
    }

    private final MediaPlayer.OnInfoListener onInfoToPlayStateListener = new MediaPlayer.OnInfoListener() {
        @Override
        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            if (MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START == what) {
                findViewById(R.id.video_pb).setVisibility(View.GONE);
            }
            if (MediaPlayer.MEDIA_INFO_BUFFERING_START == what) {
                findViewById(R.id.video_pb).setVisibility(View.VISIBLE);
            }
            if (MediaPlayer.MEDIA_INFO_BUFFERING_END == what) {
                findViewById(R.id.video_pb).setVisibility(View.GONE);
            }
            return false;
        }
    };

    @Override
    protected void onResume() {
        vodview.start();
        super.onResume();
    }

    @Override
    protected void onPause() {
        vodview.pause();
        super.onPause();
    }

    private class MySimpleOnGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (vodview == null)
                return false;
            if (vodview.isPlaying())
                vodview.pause();
            else
                vodview.start();
            return true;
        }
    }

    private class MyScaleGestureListener implements ScaleGestureDetector.OnScaleGestureListener {
        private int mW, mH;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            // scale our video view
            mW *= detector.getScaleFactor();
            mH *= detector.getScaleFactor();
            if (mW < MIN_WIDTH) { // limits width
                mW = vodview.getWidth();
                mH = vodview.getHeight();
            }
            Log.d("onScale", "scale=" + detector.getScaleFactor() + ", w=" + mW + ", h=" + mH);
            vodview.setFixedVideoSize(mW, mH); // important
            mRootParam.width = mW;
            mRootParam.height = mH;
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            mW = vodview.getWidth();
            mH = vodview.getHeight();
            Log.d("onScaleBegin", "scale=" + detector.getScaleFactor() + ", w=" + mW + ", h=" + mH);
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            Log.d("onScaleEnd", "scale=" + detector.getScaleFactor() + ", w=" + mW + ", h=" + mH);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);

    }
}


