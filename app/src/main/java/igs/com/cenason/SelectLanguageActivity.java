package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;

import com.mylibrary.LabelledSpinner;

import igs.com.cenason.Adapter.SpinAdapter;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by LakhanPatidar on 28-Jul-16.
 */
public class SelectLanguageActivity extends AppCompatActivity implements View.OnClickListener,LabelledSpinner.OnItemChosenListener {
    private Context appContext;
    public SharedPreferences appPreferences;
    boolean isAppInstalled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_language);

        appPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        isAppInstalled = appPreferences.getBoolean("isAppInstalled",false);
        CreateShortCut();
        appContext=this;
        Init();
    }

    private void Init() {
        ((LabelledSpinner) findViewById(R.id.language_spin)).setCustomAdapter(new SpinAdapter(appContext, getResources().getStringArray(R.array.lang), new int[]{R.drawable.usa_flag}));
        (findViewById(R.id.btn_done)).setOnClickListener(this);
        ((LabelledSpinner) findViewById(R.id.language_spin)).setOnItemChosenListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_done:
                ChangeLanguage();
                break;
        }
    }

    /**
     * Method gets invoked when 'Create Shortcut' button is clicked
     *
     * @param
     */

    public void CreateShortCut(){
        if(isAppInstalled==false){
            Intent shortcutIntent = new Intent(getApplicationContext(),MainActivity.class);
            shortcutIntent.setAction(Intent.ACTION_MAIN);
            Intent intent = new Intent();
            intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getResources().getString(R.string.app_name));
            intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource
                    .fromContext(getApplicationContext(), R.drawable.ic_launcher));
            intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            getApplicationContext().sendBroadcast(intent);
            SharedPreferences.Editor editor = appPreferences.edit();
            editor.putBoolean("isAppInstalled", true);
            editor.commit();
        }
    }

    private void ChangeLanguage() {
        startActivity(new Intent(appContext,LoginActivity.class));
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }

    @Override
    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
        Utility.ChangeLang(appContext,position);
        Utility.setIntegerSharedPreference(appContext,"LANG",position);
    }

    @Override
    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {
    }
}
