package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by LakhanPatidar on 28-Jul-16.
 */
public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private Context appContext;
    ResponseTask rt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = this;
      /*  Utility.ChangeLang(appContext, Utility.getIngerSharedPreferences(appContext, "LANG"));
        if (Utility.getIngerSharedPreferences(appContext, "LANG") == 0) {*/
            setContentView(R.layout.ac_for_pass);
       /* } else {
            setContentView(R.layout.activity_for_pass_ar);
        }*/
        Init();
    }

    private void Init() {
        (findViewById(R.id.btn_forgot)).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(appContext, LoginActivity.class));
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_forgot:
                Validation();
                break;
        }
    }

    public void Validation() {
        if (!((MaterialEditText) findViewById(R.id.email_forgot)).getText().toString().equals("")) {
            if (Utility.isValidEmail(((MaterialEditText) findViewById(R.id.email_forgot)).getText().toString())) {
                if (Utility.isConnectingToInternet(appContext)) {
                    CallApiWithResponse(((MaterialEditText) findViewById(R.id.email_forgot)).getText().toString());
                } else {
                    Utility.showCrouton(getResources().getString(R.string.error_internet),ForgotPasswordActivity.this);
                }
            } else {
                ((MaterialEditText) findViewById(R.id.email_forgot)).setError(getResources().getString(R.string.error_email));
            }
        } else {
            ((MaterialEditText) findViewById(R.id.email_forgot)).setError(getResources().getString(R.string.error_blank_email));
        }
    }

    public void CallApiWithResponse(String email){
        try{
            JSONObject jsonObject=new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.FORGOT_PASS);
            jsonObject.put(ConstantsUrlKey.EMAIL,email);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt= new ResponseTask(ConstantsUrlKey.SERVER_URL,jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result  == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), ForgotPasswordActivity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                Utility.showCroutonWarn(getResources().getString(R.string.password_detail), ForgotPasswordActivity.this);
                                ((MaterialEditText) findViewById(R.id.email_forgot)).setText("");
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), ForgotPasswordActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), ForgotPasswordActivity.this);
                        }
                    }
                }
            });
            rt.execute();
        }catch (JSONException e){

        }
    }
}
