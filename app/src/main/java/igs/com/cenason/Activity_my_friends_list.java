package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;

import igs.com.cenason.Adapter.CategoryContactItem;
import igs.com.cenason.Adapter.MyfriendslistAdapter;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 11/16/2016.
 */

public class Activity_my_friends_list extends AppCompatActivity {

    Context appContext;
    Toolbar mToolbar;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    ResponseTask rt;
    ArrayList<CategoryContactItem> friendslist = new ArrayList<>();
    MyfriendslistAdapter listAdapter;
    CategoryContactItem categoryContactItem;
    ArrayList<String> ids = new ArrayList<>();
    String Ids = "", CenasId = "";
    Bundle b;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_friends_listview);
        appContext = this;
        b = new Bundle();
        Init();
        ClickShareCenas();
    }

    private void Init() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.share_cenas));
        if (Utility.isConnectingToInternet(appContext)) {
            Getmyfrienslist();
        } else {
            Utility.showCroutonWarn(getResources().getString(R.string.error_internet), Activity_my_friends_list.this);
        }
        if (!b.equals("")) {
            b = getIntent().getExtras();
            CenasId = b.getString("CenasId");
        }
    }

    public void Getmyfrienslist() {
        System.out.println("user id === > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.MYFRIENDSLIST);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_my_friends_list.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                for (int i = 0; i < jobj.getJSONArray(ConstantsUrlKey.OBJECT).length(); i++) {
                                    categoryContactItem = new CategoryContactItem();
                                    categoryContactItem.setC_name(jobj.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i).getString("fullname"));
                                    categoryContactItem.setC_id(jobj.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i).getString("userid"));
                                    friendslist.add(categoryContactItem);
                                }
                                System.out.println("friendslist " + friendslist);
                                listAdapter = new MyfriendslistAdapter(appContext, friendslist);
                                ((ListView) findViewById(R.id.myfriend_list_view)).setAdapter(listAdapter);
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_my_friends_list.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_my_friends_list.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void ContactItemChecked(int pos) {
        CategoryContactItem contact = friendslist.get(pos);
        boolean selected = contact.isSelected();
        if (selected) {
            ids.remove(contact.getC_id());
        } else {
            ids.add(contact.getC_id());
        }
        listAdapter.setItemSelected(pos, !selected);
        Ids = ids.toString().substring(1, ids.toString().length() - 1);
        System.out.println("IDS=========******* " + Ids);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void ClickShareCenas() {
        findViewById(R.id.btn_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareCenas();
            }
        });
    }

    public void ShareCenas() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.CHAT);
            jsonObject.put(ConstantsUrlKey.FROM_USER, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.TO_USER, Ids);
            jsonObject.put(ConstantsUrlKey.TYPE, "share");
            jsonObject.put(ConstantsUrlKey.CENAS_ID, CenasId);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_my_friends_list.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                Utility.showCrouton(getResources().getString(R.string.share_Successfuly), Activity_my_friends_list.this);
                                Utility.ShowToastMessage(appContext, getResources().getString(R.string.share_Successfuly));
                                startActivity(new Intent(appContext, HomeActivity.class));
                                finish();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_my_friends_list.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_my_friends_list.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void Sharecenas(final int pos) {
        try {
            Intent i = new Intent();
            i.putExtra("ID", friendslist.get(pos).getC_id());
            setResult(RESULT_OK, i);
            finish();
        } catch (Exception e) {

        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        setResult(RESULT_CANCELED, i);
        finish();
    }
}
