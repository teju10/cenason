package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import igs.com.cenason.CustomWidget.CircleImageView;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 10/19/2016.
 */

public class Activity_invitation_forchat extends AppCompatActivity implements View.OnClickListener {

    Context appContext;
    ResponseTask rt;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    String date,user_id;
    Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_chat_invitation);
        appContext = this;
        Inti();
    }

    private void Inti(){
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        ((findViewById(R.id.accept_button))).setOnClickListener(this);
        ((findViewById(R.id.reject_button))).setOnClickListener(this);
      /*  mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.friend_request));*/
        GetDate();
    }

    public void AcceptorRejecttask(String status,String to_user_id) {
        System.out.println("User id =====> " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.COMFIRM_OR_DELETE_REQUEST);
            jsonObject.put(ConstantsUrlKey.FROM_USER, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.TO_USER, to_user_id);
            jsonObject.put(ConstantsUrlKey.TYPE, status);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_invitation_forchat.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                startActivity(new Intent(appContext,ChatActivity.class));
                                finish();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_invitation_forchat.this);
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_invitation_forchat.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.accept_button:
                AcceptorRejecttask("1",user_id);
                break;

            case R.id.reject_button:
                AcceptorRejecttask("0",user_id);
                break;
        }
    }

    private void GetDate() {
        try {
            date = getIntent().getStringExtra(ConstantsUrlKey.CENAS_DATA);
            JSONObject jobj = new JSONObject(date);
            ((CustomTextView) findViewById(R.id.user_name)).setText(jobj.getString("username"));
            ((CustomTextView) findViewById(R.id.message)).setText(jobj.getString("message"));
            user_id = jobj.getString("userid");
            System.out.println("To user ===> "+user_id);
            Glide.with(appContext)
                    .load(jobj.getString("profile_image")).into((CircleImageView) findViewById(R.id.user_image));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
