package igs.com.cenason;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import igs.com.cenason.CustomWidget.CircleImageView;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.CustomWidget.MyAutoCompleteTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.MultipartUtility;
import igs.com.cenason.Utilities.PlaceJSONParser;
import igs.com.cenason.Utilities.Utility;
import igs.com.cenason.Utilities.ZoomageView;

/**
 * Created by Tejas on 23-Aug-16.
 */
public class Edit_profile_Activity extends AppCompatActivity implements View.OnClickListener {

    Context appContext;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    ResponseTask rt;
    String picturePath = "", mCropImageUri, path = null;
    CircleImageView user_image;
    Toolbar mToolbar;
    ParserTask parserTask;
    MyAutoCompleteTextView address;
    PlacesTask placesTask;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        appContext = this;
        Init();
    }

    private void Init() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.edit_pro));
        (findViewById(R.id.btn_update)).setOnClickListener(this);
        (findViewById(R.id.upload_img)).setOnClickListener(this);
        address = (MyAutoCompleteTextView) findViewById(R.id.edit_address);
        user_image = (CircleImageView) findViewById(R.id.profile_user_image);
        if (Utility.isConnectingToInternet(appContext)) {
            getprofiletask();
        } else {
            Utility.showCroutonWarn(getResources().getString(R.string.error_internet), Edit_profile_Activity.this);
        }
        SetListenersforadd();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.upload_img:
                selectImage();
                break;

            case R.id.btn_update:
                CheckValidationforupdate();
                break;
        }
    }

    public void CheckValidationforupdate() {
        if (((MaterialEditText) findViewById(R.id.user_name)).getText().toString().equals("")) {
            ((MaterialEditText) findViewById(R.id.user_name)).setError(getResources().getString(R.string.error_blank_username));

        } else if (((MaterialEditText) findViewById(R.id.edit_phone)).getText().toString().equals("")) {
            ((MaterialEditText) findViewById(R.id.edit_phone)).setError(getResources().getString(R.string.error_blank_phone));

        } else if (((MyAutoCompleteTextView) findViewById(R.id.edit_address)).getText().toString().equals("")) {
            ((MyAutoCompleteTextView) findViewById(R.id.edit_address)).setError(getResources().getString(R.string.error_blank_address));

        } else if (Utility.isConnectingToInternet(appContext)) {
            new Update_profile().execute(((MaterialEditText) findViewById(R.id.user_name)).getText().toString(),
                    ((MaterialEditText) findViewById(R.id.edit_phone)).getText().toString(),
                    ((MyAutoCompleteTextView) findViewById(R.id.edit_address)).getText().toString());
        } else {
            Utility.showCroutonWarn(getResources().getString(R.string.error_internet), Edit_profile_Activity.this);
        }
    }

    public void getprofiletask() {
        System.out.println("user id=== > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.GETPROFILE);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Edit_profile_Activity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                JSONObject userdetail = jobj.getJSONObject(ConstantsUrlKey.OBJECT);
                                ((MaterialEditText) findViewById(R.id.user_name)).setText(userdetail.getString("username"));
                                ((MaterialEditText) findViewById(R.id.edit_email)).setText(userdetail.getString("email"));
                                ((MaterialEditText) findViewById(R.id.edit_phone)).setText(userdetail.getString("phone"));
                                ((MyAutoCompleteTextView) findViewById(R.id.edit_address)).setText(userdetail.getString("address"));
                                System.out.println("Profile image ==> " + userdetail.getString("profile_image"));

                                if (userdetail.getString("profile_image").equals("")) {
                                    Glide.with(appContext)
                                            .load(R.drawable.user)
                                            .into(user_image);
                                } else {

                                    Glide.with(appContext)
                                            .load(userdetail.getString("profile_image"))
                                            .listener(new RequestListener<String, GlideDrawable>() {
                                                @Override
                                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                                    findViewById(R.id.profile_pb).setVisibility(View.GONE);
                                                    return false;
                                                }

                                                @Override
                                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                                    findViewById(R.id.profile_pb).setVisibility(View.GONE);
                                                    return false;
                                                }
                                            }).
                                            into(user_image);
                                }
                                /*Picasso.with(appContext).load(userdetail.getString("profile_image")).placeholder(R.drawable.user)
                                        .into((ImageView)findViewById(R.id.user_image));*/
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Edit_profile_Activity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Edit_profile_Activity.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void selectImage() {
        final CharSequence[] options = {appContext.getResources().getString(R.string.from_camera), appContext.getResources().getString(R.string.from_gallery),
                appContext.getResources().getString(R.string.Close)};
        AlertDialog.Builder builder = new AlertDialog.Builder(appContext);
        builder.setTitle(appContext.getResources().getString(R.string.add_photo));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(appContext.getResources().getString(R.string.from_camera))) {
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    // File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                } else if (options[item].equals(appContext.getResources().getString(R.string.from_gallery))) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals(appContext.getResources().getString(R.string.Close))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {

                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped.jpeg"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            File f = new File(Crop.getOutput(result).getPath());
            String croped_file = "" + Crop.getOutput(result).getPath();
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bmOptions);
            bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
            bitmap = Utility.rotateImage(bitmap, f);
            picturePath = croped_file;
            // PicturePath = Utility.encodeTobase64(bitmap);
            user_image.setImageBitmap(bitmap);

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        } else {
            Toast.makeText(this, "Required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext, SettingActivity.class));
        finish();
    }

    public class Update_profile extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utility.ShowLoading(appContext, getResources().getString(R.string.loading_msg));
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                MultipartUtility multipart = new MultipartUtility(ConstantsUrlKey.SERVER_URL, "UTF-8");
                multipart.addFormField(ConstantsUrlKey.ACTION, ConstantsUrlKey.UPDATEPROFILE);
                multipart.addFormField(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
                multipart.addFormField(ConstantsUrlKey.U_NAME, params[0]);
                multipart.addFormField(ConstantsUrlKey.PHONENO, params[1]);
                multipart.addFormField(ConstantsUrlKey.ADDRESS, params[2]);
                if (picturePath.equals("")) {
                    multipart.addFormField("profile_image", "");
                } else {
                    multipart.addFilePart("profile_image", picturePath);
                }
                return multipart.finish();
            } catch (Exception e) {
                e.printStackTrace();
                return "Server Not Responding !";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Utility.HideDialog();
            System.out.println("SERVER RESULT" + result);
            if (result == null) {
                Utility.ShowToastMessage(appContext, "Please check your Internet connection");
            } else {
                try {
                    JSONObject json = new JSONObject(result);
                    if (json.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                        //  Utility.showCroutonWarn(getResources().getString(R.string.update_pro), Edit_profile_Activity.this);
                        Utility.ShowToastMessage(appContext, getResources().getString(R.string.update_pro));
                        startActivity(new Intent(appContext, SettingActivity.class));
                        finish();

                    } else {
                        Utility.showCroutonWarn(json.getString(ConstantsUrlKey.SERVER_MSG), Edit_profile_Activity.this);
                    }
                } catch (Exception e) {
                    Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Edit_profile_Activity.this);
                }
            }
            super.onPostExecute(result);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception while dol", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service

    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
//            String key = "key=AIzaSyDs_b8YyzDN-bFZnj4gSH7eqsEXQvEUGjw";
            String key = ConstantsUrlKey.KEY;

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input + "&" + types + "&" + sensor + "&" + key;

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;

            try {
                // Fetching the data from we service
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */

    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {
            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();
            try {
                jObject = new JSONObject(jsonData[0]);
                System.out.println("JSON OBJECT IS" + jObject);
                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            String[] from = new String[]{"description"};
            int[] to = new int[]{android.R.id.text1};
            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(appContext, result, android.R.layout.simple_list_item_1, from, to);
            // Setting the adapter
            address.setAdapter(adapter);

        }
    }

    private void SetListenersforadd() {
        address.setThreshold(1);
        address.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
    }
}
