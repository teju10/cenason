package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Shoaib on 25-Aug-16.
 */
public class Change_pass_Activity extends AppCompatActivity implements View.OnClickListener {

    Context appContext;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    ResponseTask rt;
    Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_change_password);
        appContext = this;
        Inti();
    }

    private void Inti() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.change_password));
        (findViewById(R.id.btn_change_pass)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
      switch (view.getId()){
          case R.id.btn_change_pass:
              Validation();
              break;
      }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void Validation() {
        if (((MaterialEditText) findViewById(R.id.old_password)).getText().toString().equals("")) {
            ((MaterialEditText) findViewById(R.id.old_password)).setError(getResources().getString(R.string.error_blank_old_pass));

        } else if (((MaterialEditText) findViewById(R.id.new_password)).getText().toString().equals("")){
            ((MaterialEditText) findViewById(R.id.new_password)).setError(getResources().getString(R.string.error_blank_new_pass));

        }else if (((MaterialEditText) findViewById(R.id.re_enter_new_password)).getText().toString().equals("")){
            ((MaterialEditText) findViewById(R.id.re_enter_new_password)).setError(getResources().getString(R.string.error_blank_re_enter_new_pass));
        }
        else if (!((MaterialEditText) findViewById(R.id.new_password)).getText().toString()
                .equals(((MaterialEditText) findViewById(R.id.re_enter_new_password)).getText().toString())){
            ((MaterialEditText) findViewById(R.id.re_enter_new_password)).setError(getResources().getString(R.string.error_blank_pass_not_match));
        }
        else {
            if (Utility.isConnectingToInternet(appContext)){
                ChangePasswordTask(((MaterialEditText) findViewById(R.id.old_password)).getText().toString(),
                        ((MaterialEditText) findViewById(R.id.new_password)).getText().toString());
            }else {
                Utility.showCrouton(getResources().getString(R.string.error_internet),Change_pass_Activity.this);
            }
        }
    }

    public void ChangePasswordTask(String oldpass,String newpass){
        try{
            JSONObject jsonObject=new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.CHANGEPASSWORD);
            jsonObject.put(ConstantsUrlKey.USERID,Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.NEWPASSWORD,oldpass);
            jsonObject.put(ConstantsUrlKey.OLDPASSWORD,newpass);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt= new ResponseTask(ConstantsUrlKey.SERVER_URL,jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result  == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Change_pass_Activity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                Utility.showCroutonWarn(getResources().getString(R.string.successful_change_password), Change_pass_Activity.this);
                                ((MaterialEditText)findViewById(R.id.old_password)).setText("");
                                ((MaterialEditText)findViewById(R.id.new_password)).setText("");
                                ((MaterialEditText)findViewById(R.id.re_enter_new_password)).setText("");
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Change_pass_Activity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Change_pass_Activity.this);
                        }
                    }
                }
            });
            rt.execute();
        }catch (JSONException e){

        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(appContext, SettingActivity.class));
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }
}
