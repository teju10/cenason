package igs.com.cenason;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igs.com.cenason.Adapter.AdapterForCategoryListCenas;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.NavigationDrawer.FragmentDrawer;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Tejas on 29-Jul-16.
 */
public class HomeActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    public static final String TAG = HomeActivity.class.getSimpleName();
    Context appContext;
    String User_Type;
    ArrayList<JSONObject> cat_with_cenaslist;
    ResponseTask rt;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        appContext = this;

        Init();
    }

    public void Init() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        User_Type = Utility.getSharedPreferences(appContext, ConstantsUrlKey.U_TYPE);
        Log.e(TAG, "User Type On" + TAG + "=====>" + User_Type);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
       /* drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);*/
        SetTitle("Casa");
        if (Utility.isConnectingToInternet(appContext)) {
            LoadAllCenas();
        } else {
            Utility.showCroutonWarn(getResources().getString(R.string.error_internet), HomeActivity.this);
        }
    }

    public void LoadAllCenas() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.ALLCENASACCORDING_CAT);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), HomeActivity.this);
                    } else {
                        cat_with_cenaslist = new ArrayList<JSONObject>();
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                Utility.setSharedPreference(appContext,ConstantsUrlKey.UPGRADEPLANCCHECK,jobj.getString("plan_status"));
                                if (jobj.getString("plan_status").equals("1")) {
                                    Utility.Alert(appContext, appContext.getResources().getString(R.string.upgradeplan_noti));
                                }
                                for (int i = 0; i < jobj.getJSONArray(ConstantsUrlKey.OBJECT).length(); i++) {
                                    if (jobj.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i).getString("count").equals("1")) {
                                        cat_with_cenaslist.add(jobj.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i));
                                    }
                                }

                                setListCanes();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), HomeActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), HomeActivity.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {

        }
    }

    public void setListCanes() {
        ((ListView) findViewById(R.id.cat_with_cenaslist)).setAdapter(new AdapterForCategoryListCenas(appContext, cat_with_cenaslist));
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void SetTitle(String title) {
        ((CustomTextView) mToolbar.findViewById(R.id.app_name)).setText(title);
    }

    @Override
    protected void onResume() {
        super.onResume();
        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
    }

    private void displayView(int position) {
        if (User_Type.equals("1")) {
            switch (position) {
                case 0:
                    startActivity(new Intent(appContext, HomeActivity.class));
                    finish();
                    break;
                case 1:
                    startActivity(new Intent(appContext, Activity_hot_spot.class));
                    break;
                case 2:
                    startActivity(new Intent(appContext, TabActivity.class));
                    break;
                case 3:
                    startActivity(new Intent(appContext, Activity_order_history.class));
                    break;
                case 4:
                    startActivity(new Intent(appContext, ChatActivity.class));
                    break;
                case 5:
                    startActivity(new Intent(appContext, Activity_friend_request_list.class));
                    break;
                case 6:
                    startActivity(new Intent(appContext, SettingActivity.class));
                    break;
                case 7:
                    BuildAlertMessageexit();
                    break;
                default:
                    break;
            }
        } else if (User_Type.equals("2") || User_Type.equals("3")) {
            switch (position) {
                case 0:
                    startActivity(new Intent(appContext, HomeActivity.class));
                    finish();
                    break;
                case 1:
                    startActivity(new Intent(appContext, Activity_hot_spot.class));
                    break;
                case 2:
                    startActivity(new Intent(appContext, TabActivity.class));
                    break;
                case 3:
                    startActivity(new Intent(appContext, My_cenas_Activity.class));
                    finish();
                    break;
                case 4:
                    startActivity(new Intent(appContext, Activity_order_history.class));
                    break;
                case 5:
                    startActivity(new Intent(appContext, ChatActivity.class));
                    break;
                case 6:
                    startActivity(new Intent(appContext, Activity_friend_request_list.class));
                    break;
                case 7:
                    startActivity(new Intent(appContext, SettingActivity.class));
                    break;
                case 8:
                    BuildAlertMessageexit();
                    break;
                default:
                    break;
            }
        } else if (User_Type.equals("4")) {
            switch (position) {
                case 0:
                    startActivity(new Intent(appContext, My_cenas_Activity.class));
                    break;
                case 1:
                    startActivity(new Intent(appContext, ChatActivity.class));
                    break;
                case 2:
                    startActivity(new Intent(appContext, Activity_friend_request_list.class));
                    break;
                case 3:
                    startActivity(new Intent(appContext, SettingActivity.class));
                    break;
                case 4:
                    BuildAlertMessageexit();
                    break;
                default:
                    break;
            }
        }
    }

    public void logout() {
        System.out.println("user id=== > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.LOGOUT);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), HomeActivity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                Utility.clearSharedPreference(appContext);
                                SwitchActivity();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), HomeActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), HomeActivity.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    private void BuildAlertMessageexit() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.logout_title));
        builder.setMessage(getResources().getString(R.string.logout_msg)).setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.logout_yes), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        logout();
                    }
                }).setNegativeButton(getResources().getString(R.string.logout_no), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                dialog.cancel();
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void SwitchActivity() {
        Intent bac = new Intent(appContext, LoginActivity.class);
        startActivity(bac);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menus, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                startActivity(new Intent(appContext, Activity_search_and_filter.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
