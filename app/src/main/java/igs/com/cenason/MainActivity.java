package igs.com.cenason;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.iid.FirebaseInstanceId;

import igs.com.cenason.Utilities.Utility;

public class MainActivity extends AppCompatActivity {

    String login, gcmid;
    SharedPreferences share;
    private Context appContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appContext = this;
        share = appContext.getSharedPreferences("pref", MODE_PRIVATE);
        String token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("token is" + token);
        Utility.setSharedPreference(appContext, "FCM_ID", token);
        Init();
    }

    private void Init() {
        login = Utility.getSharedPreferences(appContext, "login");
        (findViewById(R.id.logo)).animate().rotationX(90).setDuration(1000).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                (findViewById(R.id.logo)).setRotationX(-90);
                (findViewById(R.id.logo)).animate().rotationX(0).setDuration(1000).setListener(null);
            }
        });
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (login.equals("1")) {
                    startActivity(new Intent(appContext, HomeActivity.class));
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                    finish();
                } else {
                    startActivity(new Intent(appContext, SelectLanguageActivity.class));
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                    finish();
                }
            }
        }, 2000);
    }
}