package igs.com.cenason.Utilities;

import android.os.Environment;

import java.io.File;

/**
 * Created by LakhanPatidar on 01-Aug-16.
 */
public class ConstantsUrlKey {

    public static final String SDCARD_ROOT = Environment.getExternalStorageDirectory().toString();

    public static final String SDCARD_FOLDER_PATH = SDCARD_ROOT + File.separator + "CenasOn";


//    public static String SERVER_URL = "https://infograins.com/INFO01/cenason/api/api.php?";
    public static String SERVER_URL = "https://www.infograins.com/INFO01/cenason/api/api.php?";
    public static String GCM_ID = "device_id";
    public static String U_NAME = "username";
    public static String PASSWORD = "password";
    public static String EMAIL = "email";
    public static String USERID = "userid";
    public static String ADD = "address";
    public static String CAT = "category";
    public static String D_TYPE = "device_type";
    public static String LAT = "lat";
    public static String LONG = "long";
    public static String ANDROID = "Android";
    public static String U_TYPE = "user_type";
    public static String ACTION = "action";
    public static String REGISTRATION = "Register";
    public static String S_LOGIN = "SocialLogin";
    public static String PRO_IMG = "profile_image";
    public static String OUTH_ID = "outhid";
    public static String OUTH_PROVIDER = "outh_provider";
    public static String LOGIN = "Login";
    public static String FORGOT_PASS = "ForgotPassword";
    public static String RESULT_KEY = "success";
    public static String SERVER_MSG = "msg";
    public static String GOOGLE_KEY = "google";
    public static String FACEBOOK_KEY = "facebook";
    public static String TWITTER_KEY = "twitter";
    public static String UPADTEUSERTYPE = "UpdateUserType";
    public static String LOGOUT = "Logout";
    public static String GETPROFILE = "GetProfile";
    public static String CHANGEPASSWORD = "ChangePassword";
    public static String NEWPASSWORD = "password";
    public static String OLDPASSWORD = "newpassword";
    public static String UPDATEPROFILE = "UpdateProfile";
    public static String PHONENO = "phone";
    public static String PROFILE_IMG = "profile_image";
    public static String ADDRESS = "address";
    public static String SpinnerAction = "GetAllCategory";
    public static String LOGINTYPE = "social_login";
    public static String POSTCENAS = "PostCenas";
    public static String CENAS_TYPE = "cenas_type";
    public static String ISEVENT = "isEvent";
    public static String QUANTITY_AVAILABLE = "available_qty";
    public static String EVENTNAME = "name";
    public static String DESCRIPTION = "description";
    public static String CO_NUMBER = "contact_number";
    public static String EVENTTYPE = "event_type";
    public static String ISEVENT_TYPE = "iseventtype";
    public static String QUANTITY = "quantity";
    public static String FROM_DATE = "from_date";
    public static String TO_DATE = "to_date";
    public static String START_TIME = "start_time";
    public static String END_TIME = "end_time";
    public static String CATEGORY = "category";
    public static String NEARBYCENAS = "GetNearByCenasList";
    public static String EVENTPRICE = "price";
    public static String MYCENASLIST = "ListCenasByUserID";
    public static String DELETECENAS = "DeleteCenas";
    public static String CENASID = "id";
    public static String CENAS_DATA = "CENAS_DATA";
    public static String UPDATECENAS = "UpdateCenas";
    public static String UPDATECATEGORY = "UpdateCategory";
    public static String CENASBYCAT = "CenasByCategory";
    public static String CENASDETAILBTID = "cenasDetailByID";
    public static String CENASIMAGE = "cenasimage";
    public static String CENASTYPE = "cenastype";
    public static String CENASDATE = "cenasdate";
    public static String CENASTIME = "cenastime";
    public static String ADDORDER = "AddOrder";
    public static String TRANSSACTION_ID = "transaction_id";
    public static String PRICE = "price";
    public static String CURRENCY = "currency";
    public static String BOOKING_DATE = "booking_date";
    public static String PAYMENT_MODE = "payment_mode";
    public static String PAYMENT_STATUS = "payment_status";
    public static String CENAS_ADD = "add";
    public static String ORDERLIST = "OrderList";
    public static String CENASENDTIME = "endtime";
    public static String CENASENDDATE = "enddate";
    public static String GETCONTACTLIST = "UserListByMatchedContact";
    public static String SENDFRIENDREQUEST = "send_friend_request";
    public static String FROM_USER = "from_user";
    public static String TO_USER = "to_user";
    public static String COMMENT = "message";
    public static String TYPE = "type";
    public static String COMFIRM_OR_DELETE_REQUEST = "confirm_delete_request";
    public static String FRIENDREQUESTLIST = "FriendNotificationByUser";
    public static String CHAT = "chatMessage";
    public static String MESSAGE = "message";
    public static String CHATDETAILBYUSER = "chatdetailsbyuserid";
    public static String CHATLIST = "chatlist";
    public static String UPDATEREADSTATUS = "updateisread";
    public static String HOTSPOT = "HotSpot";
    public static String LIKESTATUS = "status";
    public static String LIKEANDDISLIKE = "Like_Dislike";
    public static String UPGRADEPLANLIST = "GetUpgradePlanList";
    public static String GETRENEWPLAN = "GetRenewPlanList";
    public static String UPGRADEPLANPURCHASE = "PaymentUpgradePlan";
    public static String AMOUNT = "amount";
    public static String PLANID = "membershipid";
    public static String DURATION = "duration";
    public static String STATUS = "status";
    public static String PAYMENTMODE = "paymentmode";
    public static String RENEWPALPAYMENT = "PaymentRenewPlan";
    public static String PLANSTATUS = "RenewStatusPlan";
    public static String SEARCHCENAS = "SearchCenasList";
    public static String PAGE = "page";
    public static String QUERY = "query";
    public static String CANCELORDER = "cancelOrder";
    public static String ORDERID = "order_id";
    public static String ALLTRANSACTIOSN = "getAllTransaction";
    public static String CENAS_ID = "cenas_id";
    public static String MYFRIENDSLIST = "getmyAllFriend";
    public static String ALLCENASACCORDING_CAT = "allCenasListByCategory";
    public static String CAEGORIES = "categories";
    public static String UPGRADEPLANCCHECK = "plan_status";
    public static String OBJECT = "object";

                /*GOOGLE PLACE API KEY FOR AUTOCOMPLETE*/

    public static String KEY = "key=AIzaSyDnbDT3mdPnbqKTcDuOpvh9hxmvWUNDEKk";


}
