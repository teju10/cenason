package igs.com.cenason;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.Utilities.GPSTracker;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by sandeep on 22/3/17.
 */

public class Activity_GetLatLongMap extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap mMap;
    GPSTracker gps;
    Marker marker;
    Context appContext;
    LocationManager mManager;
    double lati, longi;
    SupportMapFragment fm;
    LatLng markerLocation;
    String FilterAddress = "";
    Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maplatlng);

        appContext = this;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.pickup_address));

        mManager = (LocationManager) appContext.getSystemService(Context.LOCATION_SERVICE);
        if (!mManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Utility.AlertNoGPS(appContext);
        }

        gps = new GPSTracker(appContext);
        lati = gps.getLatitude();
        longi = gps.getLongitude();
        gps = new GPSTracker(appContext);

        GoogleMapCheck();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();

        }
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();

        }
    }

    public void GoogleMapCheck() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(appContext);
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            fm.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);

        // Enable / Disable Zooming Controls
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        // Enable / Disable My Location Button
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        // Enable / Disable Compass Icon
        googleMap.getUiSettings().setCompassEnabled(true);

        // Enable / Disable Rotate Gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);

        // Enable / Disable Zooming Functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        getMyLocation();
        if (Utility.isConnectingToInternet(appContext)) {
            Init();
        } else {
            Utility.showCrouton(getResources().getString(R.string.error_internet), Activity_GetLatLongMap.this);
        }
    }

    public void Init() {
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                Log.w("Marker", "Started-----");
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                Log.w("Marker", "Dragging----");
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                markerLocation = marker.getPosition();
//                Toast.makeText(Activity_GetLatLongMap.this, markerLocation.toString(), Toast.LENGTH_LONG).show();
                Log.w("Marker", "finished--------");
                String filterAddress = "";
                Geocoder geoCoder = new Geocoder(
                        getBaseContext(), Locale.getDefault());
                try {
                    List<Address> addresses = geoCoder.getFromLocation(markerLocation.latitude, markerLocation.longitude, 1);

                    if (addresses.size() > 0) {
                        for (int index = 0;
                             index < addresses.get(0).getMaxAddressLineIndex(); index++)
                            filterAddress += addresses.get(0).getAddressLine(index) + " ";
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (Exception e2) {
                    // TODO: handle exception

                    e2.printStackTrace();
                }
                Log.w(getClass().getSimpleName(),
                        String.format("Dragging to %f:%f", markerLocation.latitude,
                                markerLocation.longitude));
                FilterAddress = filterAddress;
                System.out.println("Activity_GetLatLongMap.onMarkerDragEnd" + FilterAddress);
//                addMarker(mMap,lati,longi,FilterAddress,"");
            }
        });

        findViewById(R.id.get_ltlng_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(appContext, Activity_Upload_Cenas.class);
                i.putExtra("faddress", FilterAddress);
                setResult(Activity.RESULT_OK, i);
                finish();
            }
        });


        findViewById(R.id.map_imgMyLocation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMyLocation();
            }
        });
    }

    private void addMarker(GoogleMap map, double lat, double lon,
                           String title, String snippet) {

        map.addMarker(new MarkerOptions().position(new LatLng(lat, lon))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_pin))
                .draggable(true));
    }

    private void getMyLocation() {
        LatLng currentpos = new LatLng(lati, longi);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(currentpos, 18);
        if (mMap != null) {
            mMap.animateCamera(cameraUpdate);
        }
        marker = mMap.addMarker(new MarkerOptions().position(currentpos)
                .title(FilterAddress)
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_pin)));
        System.out.println("FILTER ADDRESSS-------" + FilterAddress);
    }

    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext, Activity_Upload_Cenas.class));
        finish();
    }
}
