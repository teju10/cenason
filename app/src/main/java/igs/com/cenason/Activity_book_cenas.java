package igs.com.cenason;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.mylibrary.CircleIndicator;
import com.mylibrary.PackForDatePiker.DatePickerDialog;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import igs.com.cenason.Adapter.SamplePagerAdapter;
import igs.com.cenason.CustomWidget.CustomEditText;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.InputFilterMinMax;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 9/19/2016.
 */

public class Activity_book_cenas extends AppCompatActivity implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener {

    private static final String TAG = "paymentExample";

    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     * <p/>
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     * <p/>
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID =
            "ASnNel0bqtTHzztVMCxho6Wrn5RGLyV4brfFhQz9HTl5zx4Y8SoTAgqPBbwV-KZeGwHpN-P9ynF8isoo";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            .merchantName("Example Merchant")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
    Context appContext;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    Toolbar mToolbar;
    ResponseTask rt;
    CustomTextView dateTextView;
    String cenas_id;
    ViewPager viewpager;
    CircleIndicator indicator;
    ProgressBar progressBar = null;
    private Calendar calendar;
    private DateFormat dateFormat;
    private VideoView videoPreview;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_cenas_booking);
        appContext = this;
        Inti();
    }

    private void Inti() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        Setdata();
        calendar = Calendar.getInstance();
        dateFormat = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault());
        dateTextView = (CustomTextView) findViewById(R.id.book_date);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        videoPreview = (VideoView) findViewById(R.id.video_view);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
    }

    private void Setdata() {
        cenas_id = Utility.getSharedPreferences(appContext, ConstantsUrlKey.CENASID);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.book));
        ((CustomTextView) findViewById(R.id.book_cenas_name)).setText(Utility.getSharedPreferences(appContext, ConstantsUrlKey.EVENTNAME));

        ((CustomTextView) findViewById(R.id.book_cenas_type)).setText(Utility.getSharedPreferences(appContext, ConstantsUrlKey.CENASTYPE));
        ((CustomTextView) findViewById(R.id.available_qty)).setText(Utility.getSharedPreferences(appContext, ConstantsUrlKey.QUANTITY_AVAILABLE) + " Quantity Available ");
        if (!Utility.getSharedPreferences(appContext, ConstantsUrlKey.QUANTITY_AVAILABLE).equals("0")) {
            ((CustomEditText) findViewById(R.id.book_quantity)).setFilters(new InputFilter[]{new InputFilterMinMax("1", Utility.getSharedPreferences(appContext, ConstantsUrlKey.QUANTITY_AVAILABLE))});
        } else {
            ((CustomEditText) findViewById(R.id.book_quantity)).setEnabled(false);
            ((CustomEditText) findViewById(R.id.book_quantity)).setHint(R.string.qty_unavailable);
            ((CustomEditText) findViewById(R.id.book_quantity)).setHintTextColor(getResources().getColor(R.color.red));
        }

        ((CustomTextView) findViewById(R.id.book_address)).setText(Utility.getSharedPreferences(appContext, ConstantsUrlKey.CENAS_ADD));
        ((CustomTextView) findViewById(R.id.book_discription)).setText(Utility.getSharedPreferences(appContext, ConstantsUrlKey.DESCRIPTION));
        if (!Utility.getSharedPreferences(appContext, ConstantsUrlKey.ISEVENT).equals("0")) {
            (findViewById(R.id.book_date)).setOnClickListener(this);
        }else if( Utility.getSharedPreferences(appContext, ConstantsUrlKey.ISEVENT).equals("1")){
            (findViewById(R.id.book_date)).setVisibility(View.GONE);
        }
        System.out.println("Cenas type ==== > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.CENASTYPE));
        System.out.println("Cenas date time ==== > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.CENASDATE));
        System.out.println("Cenas id ==== > " + cenas_id);
        getcenasbyid(cenas_id);
        if (Utility.getSharedPreferences(appContext, ConstantsUrlKey.QUANTITY_AVAILABLE).equals("0")) {
            (findViewById(R.id.book_cenas_btn)).setVisibility(View.GONE);
        } else {
            (findViewById(R.id.book_cenas_btn)).setOnClickListener(this);
        }
    }

    public String parseDateToyyyyMMdd(String time) {
        String outputPattern = "dd/MM/yyyy";
        String inputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        String dayOfTheWeek = null;
        try {
            date = inputFormat.parse(time);
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            dayOfTheWeek = sdf.format(date);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dayOfTheWeek + " " + str;
    }

    public String Time(String time) {
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
        String str = null;
        try {
            Date date = parseFormat.parse(time);
            str = displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onBuyPressed(String String_qty) {
        try {
            int priceevent = Integer.parseInt(Utility.getSharedPreferences(appContext, ConstantsUrlKey.EVENTPRICE));
            int qty = Integer.parseInt(String_qty);
            PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE, priceevent * qty);
            Intent intent = new Intent(appContext, PaymentActivity.class);
            intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
            intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
            startActivityForResult(intent, REQUEST_CODE_PAYMENT);
        } catch (Exception e) {
        }
    }

    public void getcenasbyid(String catid) {
        System.out.println("user id === > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.CENASDETAILBTID);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.CENASID, catid);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server Result ==> " + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_book_cenas.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {

                                JSONObject cenasdetail = jobj.getJSONObject(ConstantsUrlKey.OBJECT);

                                System.out.println("image array ==> " + cenasdetail.getJSONArray("cenas_image"));
                                if (Utility.getSharedPreferences(appContext, ConstantsUrlKey.ISEVENT).equals("0")) {
                                    ((CustomTextView) findViewById(R.id.book_date)).setText(cenasdetail.getString("hours"));
                                } else {
                                    ((CustomTextView) findViewById(R.id.book_cenas_date_time))
                                            .setText(cenasdetail.getString("from_date") + " to " +cenasdetail.getString("from_date"));
                                }
                                if (cenasdetail.getJSONArray("cenas_image").length() == 0) {
                                    ((findViewById(R.id.pager_layout))).setVisibility(View.GONE);
                                    ((findViewById(R.id.video_view_layout))).setVisibility(View.VISIBLE);

                                    Uri video = Uri.parse(cenasdetail.getString("cenas_video"));
                                    videoPreview.setVideoURI(video);
                                    System.out.println("video from server ===> " + cenasdetail.getString("cenas_video"));
                                    progressBar.setVisibility(View.VISIBLE);
                                    videoPreview.start();
                                    videoPreview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                        @Override
                                        public void onPrepared(MediaPlayer mp) {
                                            mp.start();
                                            mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                                                @Override
                                                public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                                                    progressBar.setVisibility(View.GONE);
                                                    mp.start();
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    ((findViewById(R.id.pager_layout))).setVisibility(View.VISIBLE);
                                    ((findViewById(R.id.video_view_layout))).setVisibility(View.GONE);
                                    viewpager.setAdapter(new SamplePagerAdapter(Activity_book_cenas.this, cenasdetail.getJSONArray("cenas_image")));
                                    indicator.setViewPager(viewpager);
                                    viewpager.setCurrentItem(0);
                                }
                                if(cenasdetail.getString("isEvent").equals("0")){
                                    ((CustomTextView) findViewById(R.id.book_cenas_type)).setText("Cenas");}
                                else if(cenasdetail.getString("isEvent").equals("1")){
                                    ((CustomTextView) findViewById(R.id.book_cenas_type)).setText("Event");}
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_book_cenas.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_book_cenas.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    private PayPalPayment getThingToBuy(String paymentIntent, int totalamount) {
        return new PayPalPayment(new BigDecimal(totalamount),
                "USD", "paypal", paymentIntent);
    }

    protected void displayResultText(String result) {
        Utility.ShowToastMessage(appContext, result);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        Log.i(TAG, "response ===== " + confirm.toJSONObject().getJSONObject("response").toString());
                        Log.i(TAG, "response ===== " + confirm.toJSONObject().getJSONObject("response").getString("create_time"));
                        Log.i(TAG, "response ===== " + confirm.toJSONObject().getJSONObject("response").getString("id"));
                        Log.i(TAG, "response ===== " + confirm.toJSONObject().getJSONObject("response").getString("state"));

                        Log.i(TAG, confirm.getPayment().toJSONObject().getString("amount"));
                        Log.i(TAG, confirm.getPayment().toJSONObject().getString("currency_code"));
                        Log.i(TAG, confirm.getPayment().toJSONObject().getString("short_description"));

                        if (confirm.toJSONObject().getJSONObject("response").getString("state").equalsIgnoreCase("approved")) {
                            if (Utility.isConnectingToInternet(appContext)) {
                                BookCenasTask(confirm.toJSONObject().getJSONObject("response").getString("id"),
                                        ((CustomEditText) findViewById(R.id.book_quantity)).getText().toString(),
                                        confirm.getPayment().toJSONObject().getString("amount"),
                                        confirm.getPayment().toJSONObject().getString("currency_code"),
                                        ((CustomTextView) findViewById(R.id.book_date)).getText().toString(),
                                        ((CustomEditText) findViewById(R.id.book_user_name)).getText().toString(),
                                        ((CustomEditText) findViewById(R.id.book_number)).getText().toString(),
                                        confirm.getPayment().toJSONObject().getString("short_description"), "complete");
                            } else {
                                Utility.ShowToastMessage(appContext, getResources().getString(R.string.error_internet));
                            }
                        } else {
                            Utility.ShowToastMessage(appContext, "Payment Not Done Please Try Again Later");
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        displayResultText("Future Payment code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("FuturePaymentExample", "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.book_cenas_btn:
                Validation();

                break;
            case R.id.book_date:
                try {
                    String startdate = Utility.getSharedPreferences(appContext, ConstantsUrlKey.CENASDATE);
                    String endDate = Utility.getSharedPreferences(appContext, ConstantsUrlKey.CENASENDDATE);
                    System.out.println("Start date  ==> " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.CENASDATE));
                    System.out.println("End date  ==> " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.CENASENDDATE));
                    Calendar cal = Calendar.getInstance();
                    Calendar cal2 = Calendar.getInstance();
                    DatePickerDialog dpd = DatePickerDialog.newInstance(this, Integer.parseInt(startdate.split("-")[0]),
                            Integer.parseInt(startdate.split("-")[1]) - 1, Integer.parseInt(startdate.split("-")[2]));
                    cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(startdate.split("-")[2]));
                    cal.set(Calendar.YEAR, Integer.parseInt(startdate.split("-")[0]));
                    cal.set(Calendar.MONTH, Integer.parseInt(startdate.split("-")[1]) - 1);
                    dpd.setMinDate(cal);
                    cal2.set(Calendar.DAY_OF_MONTH, Integer.parseInt(endDate.split("-")[2]));
                    cal2.set(Calendar.YEAR, Integer.parseInt(endDate.split("-")[0]));
                    cal2.set(Calendar.MONTH, Integer.parseInt(endDate.split("-")[1]) - 1);
                    dpd.setMaxDate(cal2);
                    dpd.show(getFragmentManager(), "datePicker");
                } catch (Exception e) {

                }
        }
    }

    public void Validation() {
        if (((CustomEditText) findViewById(R.id.book_user_name)).getText().toString().equals("")) {
            Utility.showCroutonWarn(getResources().getString(R.string.user_name), Activity_book_cenas.this);

        } else if (((CustomEditText) findViewById(R.id.book_number)).getText().toString().equals("")) {
            Utility.showCroutonWarn(getResources().getString(R.string.add_phone), Activity_book_cenas.this);

        } else if (((CustomEditText) findViewById(R.id.book_quantity)).getText().toString().equals("")) {
            Utility.showCroutonWarn(getResources().getString(R.string.book_quantity), Activity_book_cenas.this);

        } else if (Utility.getSharedPreferences(appContext, ConstantsUrlKey.ISEVENT).equals("1") &&
                ((CustomTextView) findViewById(R.id.book_date)).getText().toString().equals("")) {
            Utility.showCroutonWarn(getResources().getString(R.string.book_date_first), Activity_book_cenas.this);
        } else {
            onBuyPressed(((CustomEditText) findViewById(R.id.book_quantity)).getText().toString());
        }
    }

    public void BookCenasTask(String transaction_id, String quantity, String price, String currency,
                              String booking_date, String user_name, String number, String payment_mode, String status) {
        System.out.println("result normal");
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.ADDORDER);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            System.out.println("User id ==> " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put("cenas_id", Utility.getSharedPreferences(appContext, ConstantsUrlKey.CENASID));
            System.out.println("cenas_id ==> " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.CENASID));
            jsonObject.put(ConstantsUrlKey.TRANSSACTION_ID, transaction_id);
            System.out.println("transaction_id ==> " + transaction_id);
            jsonObject.put(ConstantsUrlKey.QUANTITY, quantity);
            System.out.println("quantity ==> " + quantity);
            jsonObject.put(ConstantsUrlKey.PRICE, price);
            System.out.println("price ==> " + price);
            jsonObject.put(ConstantsUrlKey.CURRENCY, currency);
            System.out.println("currency ==> " + currency);
            if (!Utility.getSharedPreferences(appContext, ConstantsUrlKey.ISEVENT).equals("0")) {
                jsonObject.put(ConstantsUrlKey.BOOKING_DATE, booking_date);
                System.out.println("booking_date ==> " + booking_date);
            }
            jsonObject.put("user_name", user_name);
            System.out.println("user_name ==> " + user_name);
            jsonObject.put(ConstantsUrlKey.CO_NUMBER, number);
            System.out.println("number ==> " + number);
            jsonObject.put(ConstantsUrlKey.PAYMENT_MODE, payment_mode);
            System.out.println("payment_mode ==> " + payment_mode);
            jsonObject.put(ConstantsUrlKey.PAYMENT_STATUS, status);
            System.out.println("status ==> " + status);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("SERVER RESULT" + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_book_cenas.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                startActivity(new Intent(appContext, HomeActivity.class));
                                Utility.ShowToastMessage(appContext, getResources().getString(R.string.book_successful));
                                finish();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_book_cenas.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_book_cenas.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        dateTextView.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
    }

}
