package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioGroup;

import com.gc.materialdesign.views.ButtonRectangle;

import igs.com.cenason.CustomWidget.CustomRadioButton;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Shoaib on 20-Aug-16.
 */
public class Selection_register extends AppCompatActivity {
    Context appContext;
    RadioGroup dialog_register_select_radio_group;
    CustomRadioButton  dialog_provider_select_radio_freelance, dialog_provider_select_radio_companies,
            dialog_provider_select_radio_public,dialog_register_select_radio_regular, dialog_register_select_radio_provider;

    String userType = "";
    ButtonRectangle dialog_register_select_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = this;

        setContentView(R.layout.activity_register_select);

        dialog_register_select_btn = (ButtonRectangle) findViewById(R.id.dialog_register_select_btn);

        dialog_provider_select_radio_freelance = (CustomRadioButton)findViewById(R.id.dialog_provider_select_radio_freelance);
        dialog_provider_select_radio_companies = (CustomRadioButton) findViewById(R.id.dialog_provider_select_radio_companies);
        dialog_provider_select_radio_public = (CustomRadioButton) findViewById(R.id.dialog_provider_select_radio_public);

        Init();
    }

    private void Init() {
        dialog_register_select_radio_group = (RadioGroup) findViewById(R.id.dialog_register_select_radio_group);
        dialog_register_select_radio_regular = (CustomRadioButton) findViewById(R.id.dialog_register_select_radio_regular);
        dialog_register_select_radio_provider = (CustomRadioButton) findViewById(R.id.dialog_register_select_radio_provider);

        dialog_register_select_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (dialog_register_select_radio_regular.isChecked()) {
                    userType = "1";
                } else if (dialog_provider_select_radio_companies.isChecked()) {
                    userType = "2";
                }else if (dialog_provider_select_radio_freelance.isChecked()) {
                    userType = "3";
                }else if (dialog_provider_select_radio_public.isChecked()) {
                    userType = "4";
                }
            }
        });

        dialog_register_select_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userType.equals("1")) {
                    SwitchSignupActivity("1");
                } else if (userType.equals("2")) {
                    SwitchSignupActivity("2");
                } else if (userType.equals("3")) {
                    SwitchSignupActivity("3");
                } else if (userType.equals("4")) {
                    SwitchSignupActivity("4");
                } else {
                    Utility.ShowToastMessage(appContext, getResources().getString(R.string.please_selcect_one));
                }
              /*  } else if (userType.equals("0")) {
                    startActivity(new Intent(appContext, Selection_provider.class));
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                    finish();
                }*/

            }
        });
    }

    public void SwitchSignupActivity(String utype) {
        startActivity(new Intent(appContext, SignupActivity.class).putExtra(ConstantsUrlKey.U_TYPE, utype));
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext, LoginActivity.class));
        finish();
    }
}
