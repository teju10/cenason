package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Shoaib on 23-Aug-16.
 */
public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    Context appContext;
    Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        appContext = this;
        Init();
    }

    private void Init() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.sitting));
        (findViewById(R.id.edit_profile)).setOnClickListener(this);
        (findViewById(R.id.change_pass)).setOnClickListener(this);
        (findViewById(R.id.upgrade_plan)).setOnClickListener(this);
        (findViewById(R.id.subscription)).setOnClickListener(this);
        (findViewById(R.id.transition_history)).setOnClickListener(this);
        if (Utility.getSharedPreferences(appContext, ConstantsUrlKey.LOGINTYPE).equals("social_login")) {
            (findViewById(R.id.change_pass)).setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_profile:
                startActivity(new Intent(appContext, Edit_profile_Activity.class));
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                finish();
                break;

            case R.id.change_pass:
                startActivity(new Intent(appContext, Change_pass_Activity.class));
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                finish();
                break;

            case R.id.upgrade_plan:
                startActivity(new Intent(appContext, Activity_upgrade_plan_list.class));
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                finish();
                break;

            case R.id.subscription:
                startActivity(new Intent(appContext, Activity_renew_plan.class));
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                finish();
                break;

            case R.id.transition_history:
                startActivity(new Intent(appContext, Activity_transition_history.class));
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                finish();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext,HomeActivity.class));
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

}
