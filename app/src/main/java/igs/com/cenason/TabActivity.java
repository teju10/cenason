package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import igs.com.cenason.Adapter.PagerAdapter;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.GPSTracker;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Shoaib on 22-Aug-16.
 */
public class TabActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    Context appContext;
    public LinkedList<JSONObject> nearbylist = new LinkedList<JSONObject>();
    double lat, lang;
    GPSTracker mGPS;
    LocationManager mManager;
    ResponseTask rt;
    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_layout);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        appContext = this;
        ((CustomTextView) mToolbar.findViewById(R.id.app_name)).setText(getResources().getString(R.string.nearby_cenas));
        Init();
    }

    private void Init() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.mapview)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.listview)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager = (ViewPager) findViewById(R.id.pager);
        mManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!mManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Utility.AlertNoGPS(appContext);
        }
        mGPS = new GPSTracker(appContext);
        lat = mGPS.getLatitude();
        lang = mGPS.getLongitude();
        System.out.println("lat ===> " + String.valueOf(lat));
        System.out.println("long ===> " + String.valueOf(lang));

        if (Utility.isConnectingToInternet(appContext)) {
            getneearbylist(String.valueOf(lat), String.valueOf(lang));
        } else {
            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), TabActivity.this);
        }
    }

    private void tab() {
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext,HomeActivity.class));
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void getneearbylist(String lati, String longi) {
        System.out.println("user id=== > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.NEARBYCENAS);
            jsonObject.put("latitude", lati);
            jsonObject.put("longitute", longi);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("server Result=====>" + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), TabActivity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                for (int i = 0; i < jobj.getJSONArray(ConstantsUrlKey.OBJECT).length(); i++) {
                                    nearbylist.add(jobj.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i));
                                }
                                tab();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), TabActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), TabActivity.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }
}
