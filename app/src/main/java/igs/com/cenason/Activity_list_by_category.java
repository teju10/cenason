package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import igs.com.cenason.Adapter.GridViewAdapter;
import igs.com.cenason.Adapter.ListViewAdapter;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 9/9/2016.
 */

public class Activity_list_by_category extends AppCompatActivity implements View.OnClickListener {

    ViewStub listview, gridview;
    Context appContext;
    private ListView list;
    private GridView grid;
    ResponseTask rt;
    LinkedList<JSONObject> cenaslistbyvat=new LinkedList<>();
    private boolean list_visibile = true;
    private ListViewAdapter listViewAdapter;
    private GridViewAdapter gridViewAdapter;
    private String catname;
    Toolbar mToolbar;
    SharedPreferences share;
    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_by_category);
        appContext = this;
        Inti();
    }

    private void Inti() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.cenas_info));
        Intent i = getIntent();
        Bundle extras = i.getExtras();
        catname  = extras.getString("catgoryname");
        System.out.println("Category_name ====> "+catname);
        //set of layouts for the views
        listview = (ViewStub) findViewById(R.id.list_view);
        gridview = (ViewStub) findViewById(R.id.grid_view);

        //inflate the layouts
        listview.inflate();
        gridview.inflate();

        //set of layouts for the list/grid
        list = (ListView) findViewById(R.id.list);
        grid = (GridView) findViewById(R.id.grid);

        ((findViewById(R.id.cahnge_icon))).setOnClickListener(this);
        // ((findViewById(R.id.icon_grid))).setOnClickListener(this);
        // grid_icon = (ImageView)findViewById(R.id.icon_grid);

        if (Utility.isConnectingToInternet(appContext)){
            getcenaslist(catname);
        }else {
            Utility.showCroutonWarn(getResources().getString(R.string.error_internet), Activity_list_by_category.this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

   /* @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext, HomeActivity.class));
        finish();

    }
*/
    private void changeView() {
        //if the current view is the listview, passes to gridview
        if(list_visibile) {
            listview.setVisibility(View.GONE);
            gridview.setVisibility(View.VISIBLE);
            ((ImageView)findViewById(R.id.cahnge_icon)).setImageResource(R.drawable.list_icon);
            // list_icon.setVisibility(View.VISIBLE);
            list_visibile = false;
            setAdapters();
        }
        else {
            gridview.setVisibility(View.GONE);
            listview.setVisibility(View.VISIBLE);
            ((ImageView)findViewById(R.id.cahnge_icon)).setImageResource(R.drawable.grid_icon);
            //  grid_icon.setVisibility(View.VISIBLE);
            list_visibile = true;
            setAdapters();
        }
    }

    public void getcenaslist(String catname) {
        System.out.println("user id === > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.CENASBYCAT);
            jsonObject.put(ConstantsUrlKey.CATEGORY, catname);
            jsonObject.put(ConstantsUrlKey.USERID,  Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server Result ==> "+result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_list_by_category.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                for (int i = 0; i < jobj.getJSONArray(ConstantsUrlKey.OBJECT).length(); i++) {
                                    cenaslistbyvat.add(jobj.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i));
                                }
                                setAdapters();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_list_by_category.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_list_by_category.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    private void setAdapters() {
        if(list_visibile) {
            listViewAdapter = new ListViewAdapter(this,R.layout.all_category_list_item, cenaslistbyvat);
            list.setAdapter(listViewAdapter);
        }
        else {
            gridViewAdapter = new GridViewAdapter(this,R.layout.gridview_item, cenaslistbyvat);
            grid.setAdapter(gridViewAdapter);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
         /*   case R.id.icon_grid:
                changeView();
                break;
*/
            case R.id.cahnge_icon:
                changeView();
                break;
        }
    }
}