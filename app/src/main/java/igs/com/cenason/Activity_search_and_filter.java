package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.costum.android.widget.LoadMoreListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import igs.com.cenason.Adapter.SearchAdapter;
import igs.com.cenason.CustomWidget.CustomAutoCompleteTextView;
import igs.com.cenason.CustomWidget.CustomEditText;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.CustomWidget.MaterialSpinner;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.PlaceJSONParser;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Tejas on 11/8/2016.
 */

public class Activity_search_and_filter extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    MaterialSpinner filter_by_spinner, search_cenas_by_category;
    Context appContext;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    Toolbar mToolbar;
    ResponseTask rt;
    LinkedList<JSONObject> cenaslist = new LinkedList<>();
    SearchAdapter listAdapter;
    String selctedtype = "";
    int page = 0;
    List<String> categorylist = new ArrayList<String>();
    ParserTask parserTask;
    CustomAutoCompleteTextView address;
    PlacesTask placesTask;
    private String[] filter = {"Select type to filter", "Name", "Category", "Location"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_filter);
        appContext = this;
        Inti();
    }

    private void Inti() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.cenas_search));
        filter_by_spinner = (MaterialSpinner) findViewById(R.id.filter);
        search_cenas_by_category = (MaterialSpinner) findViewById(R.id.search_cenas_by_category);
        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this, R.layout.item_spin, filter);
        adapter_state.setDropDownViewResource(R.layout.activity_spinner_item);
        filter_by_spinner.setAdapter(adapter_state);
        filter_by_spinner.setOnItemSelectedListener(this);
        listAdapter = new SearchAdapter(appContext, cenaslist);
        ((LoadMoreListView) findViewById(R.id.search_list_view)).setAdapter(listAdapter);
        (findViewById(R.id.search_cenas_btn)).setOnClickListener(this);
        address = (CustomAutoCompleteTextView) findViewById(R.id.search_cenas_by_address);
        SetListeners();
        if (Utility.isConnectingToInternet(appContext)) {
            SpinnerValueTask();
        } else {
            Utility.showCrouton(getResources().getString(R.string.error_internet), Activity_search_and_filter.this);
        }
        ((LoadMoreListView) findViewById(R.id.search_list_view)).setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (Utility.isConnectingToInternet(appContext)) {
                    page = page + 1;
                    System.out.println("page ====> " + page);
                    if (selctedtype == "Location") {
                        GetCenaslist("" + page, selctedtype, ((CustomAutoCompleteTextView) findViewById(R.id.search_cenas_by_address)).getText().toString());
                    }else if(selctedtype == "Category")
                        GetCenaslist("" + page, selctedtype, search_cenas_by_category.getSelectedItem().toString());
                }else if(selctedtype == "Name"){
                    GetCenaslist("" + page, selctedtype, ((CustomEditText) findViewById(R.id.search_cenas_by_name)).getText().toString());
                } else {
                    Toast.makeText(appContext, getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext, HomeActivity.class));
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        filter_by_spinner.setSelection(position);
        if (filter_by_spinner.getSelectedItem().equals("Select type to filter")) {
            (findViewById(R.id.search_cenas_btn)).setVisibility(View.GONE);
            (findViewById(R.id.search_cenas_by_name)).setVisibility(View.GONE);
            (findViewById(R.id.search_cenas_by_category)).setVisibility(View.GONE);
            (findViewById(R.id.search_cenas_by_address)).setVisibility(View.GONE);
            selctedtype = "";
        } else if (filter_by_spinner.getSelectedItem().equals("Name")) {
            (findViewById(R.id.search_cenas_by_name)).setVisibility(View.VISIBLE);
            (findViewById(R.id.search_cenas_btn)).setVisibility(View.VISIBLE);
            (findViewById(R.id.search_cenas_by_category)).setVisibility(View.GONE);
            (findViewById(R.id.search_cenas_by_address)).setVisibility(View.GONE);
            selctedtype = "Name";
        } else if (filter_by_spinner.getSelectedItem().equals("Category")) {
            (findViewById(R.id.search_cenas_by_name)).setVisibility(View.GONE);
            (findViewById(R.id.search_cenas_by_address)).setVisibility(View.GONE);
            (findViewById(R.id.search_cenas_by_category)).setVisibility(View.VISIBLE);
            (findViewById(R.id.search_cenas_btn)).setVisibility(View.VISIBLE);
            selctedtype = "Category";
        } else if (filter_by_spinner.getSelectedItem().equals("Location")) {
            (findViewById(R.id.search_cenas_by_category)).setVisibility(View.GONE);
            (findViewById(R.id.search_cenas_by_address)).setVisibility(View.VISIBLE);
            (findViewById(R.id.search_cenas_by_name)).setVisibility(View.GONE);
            (findViewById(R.id.search_cenas_btn)).setVisibility(View.VISIBLE);
            selctedtype = "Location";
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void SpinnerValueTask() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.SpinnerAction);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Log.e("Server result", "Server result ===>" + result);
                    if (result == null) {
                        Utility.HideDialog();
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_search_and_filter.this);
                        //buildAlertMessage();

                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equals("1")) {
                                for (int i = 0; i < json.getJSONArray(ConstantsUrlKey.OBJECT).length(); i++) {
                                    categorylist.add(json.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i).getString("category"));
                                    // Creating adapter for spinner
                                }
                                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(appContext,
                                        R.layout.item_spin, categorylist);

                                // Drop down layout style - list view with radio button
                                spinnerAdapter.setDropDownViewResource(R.layout.activity_spinner_item);

                                // attaching data adapter to spinner
                                search_cenas_by_category.setAdapter(spinnerAdapter);
                                GetCenaslist("" + page, "", "");
                                System.out.println("page ========> " + page);
                            }
                        } catch (Exception e) {
                            Utility.HideDialog();
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        rt.execute();
    }

    public void GetCenaslist(final String page1, String type, String query) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.SEARCHCENAS);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.TYPE, type);
            jsonObject.put(ConstantsUrlKey.QUERY, query);
            jsonObject.put(ConstantsUrlKey.PAGE, page1);
            /*if (page1.equals("0")) {
                Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            }*/
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    ((LoadMoreListView) findViewById(R.id.search_list_view)).onLoadMoreComplete();
                    if (result == null) {
                        page = page - 1;
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_search_and_filter.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                JSONArray j = jobj.getJSONArray(ConstantsUrlKey.OBJECT);
                                for (int i = 0; i < j.length(); i++) {
                                    cenaslist.add(j.getJSONObject(i));
                                }
                                SetAdapter();
                            } else {
                                page = page - 1;
                                if (!page1.equals("0")) {
                                    Utility.ShowToastMessage(appContext, getResources().getString(R.string.no_cenas_found));
                                } else {
                                    Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_search_and_filter.this);
                                }
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_search_and_filter.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    private void SetAdapter() {
        listAdapter.notifyDataSetChanged();
    }

    public void Validation() {
        if ((findViewById(R.id.search_cenas_by_name)).getVisibility() == View.VISIBLE) {
            if (((CustomEditText) findViewById(R.id.search_cenas_by_name)).getText().toString().equals("")) {
                Utility.showCrouton(getResources().getString(R.string.search_cenas), Activity_search_and_filter.this);
            } else {
                if (Utility.isConnectingToInternet(appContext)) {
                    Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
                    page = 0;
                    cenaslist.clear();
                    listAdapter = new SearchAdapter(appContext, cenaslist);
                    ((LoadMoreListView) findViewById(R.id.search_list_view)).setAdapter(listAdapter);
                    GetCenaslist("" + page, selctedtype, ((CustomEditText) findViewById(R.id.search_cenas_by_name)).getText().toString());
                    Utility.hideKeyBoard((CustomEditText) findViewById(R.id.search_cenas_by_name), appContext);
                } else {
                    Utility.showCrouton(getResources().getString(R.string.error_internet), Activity_search_and_filter.this);
                }
            }
        } else if ((findViewById(R.id.search_cenas_by_address)).getVisibility() == View.VISIBLE) {
            if (((CustomAutoCompleteTextView) findViewById(R.id.search_cenas_by_address)).getText().toString().equals("")) {
                Utility.showCrouton(getResources().getString(R.string.enter_cenas_address), Activity_search_and_filter.this);
            } else {
                if (Utility.isConnectingToInternet(appContext)) {
                    Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
                    page = 0;
                    cenaslist.clear();
                    listAdapter = new SearchAdapter(appContext, cenaslist);
                    ((LoadMoreListView) findViewById(R.id.search_list_view)).setAdapter(listAdapter);
                    GetCenaslist("" + page, selctedtype, ((CustomAutoCompleteTextView) findViewById(R.id.search_cenas_by_address)).getText().toString());
                    Utility.hideKeyBoard((CustomAutoCompleteTextView) findViewById(R.id.search_cenas_by_address), appContext);
                } else {
                    Utility.showCrouton(getResources().getString(R.string.error_internet), Activity_search_and_filter.this);
                }
            }
        } else if (search_cenas_by_category.getVisibility() == View.VISIBLE) {
            if ((search_cenas_by_category.toString().equals(""))) {
                Utility.showCrouton(getResources().getString(R.string.select_category), Activity_search_and_filter.this);
            } else {
                if (Utility.isConnectingToInternet(appContext)) {
                    Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
                    page = 0;
                    cenaslist.clear();
                    listAdapter = new SearchAdapter(appContext, cenaslist);
                    ((LoadMoreListView) findViewById(R.id.search_list_view)).setAdapter(listAdapter);
                    GetCenaslist("" + page, selctedtype, search_cenas_by_category.getSelectedItem().toString());
                } else {
                    Utility.showCrouton(getResources().getString(R.string.error_internet), Activity_search_and_filter.this);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_cenas_btn:
                Validation();
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception while dol", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private void SetListeners() {
        address.setThreshold(1);
        address.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
    }

    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            String key = ConstantsUrlKey.KEY;

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input + "&" + types + "&" + sensor + "&" + key;

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;

            try {
                // Fetching the data from we service
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {
            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();
            try {
                jObject = new JSONObject(jsonData[0]);
                System.out.println("JSON OBJECT IS" + jObject);
                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            String[] from = new String[]{"description"};
            int[] to = new int[]{android.R.id.text1};
            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(appContext, result, android.R.layout.simple_list_item_1, from, to);
            // Setting the adapter
            address.setAdapter(adapter);

        }
    }

}
