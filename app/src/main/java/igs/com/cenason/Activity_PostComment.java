package igs.com.cenason;

import android.content.Context;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.costum.android.widget.LoadMoreListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import igs.com.cenason.CustomWidget.CustomEditText;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Tejas on 8/3/17.
 */

public class Activity_PostComment extends AppCompatActivity {
    Context mContext;
    ResponseTask rt;
    AdapterCommentlist adapter;
    LinkedList<JSONObject> c_arrayl = new LinkedList<>();
    int page = 0;
    Bundle c;
    String cenas_id = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postcomment_list);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.comment));
        c = new Bundle();
        List();

    }

    public void List() {
        c = getIntent().getExtras();
        cenas_id = c.getString("cid");
        GetPostedList("" + page);
        ((LoadMoreListView) findViewById(R.id.commentlist)).setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (Utility.isConnectingToInternet(mContext)) {
                    page = page + 1;
                    System.out.println("page ====> " + page);
                    GetPostedList("" + page);
                } else {
                    Toast.makeText(mContext, getResources().getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.send_msg_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CustomEditText) findViewById(R.id.send_msg_edittext)).getText().toString().equals("")) {
                    Utility.ShowToastMessage(mContext, "please write your comment");
                } else {
                    SendComment(((CustomEditText) findViewById(R.id.send_msg_edittext)).getText().toString());
                }
            }
        });
    }

    public void SendComment(String edtpost) {
        try {
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            JSONObject j = new JSONObject();
            j.put(ConstantsUrlKey.ACTION, "insertCommentByUser");
            j.put("cenas_id", cenas_id);
            j.put("comment", edtpost);
            j.put("userid", Utility.getSharedPreferences(mContext, ConstantsUrlKey.USERID));

            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject c = new JSONObject(result);
                        if (c.getString("success").equalsIgnoreCase("1")) {
                            Utility.ShowToastMessage(mContext, "Your comment is posted");
                            ((CustomEditText) findViewById(R.id.send_msg_edittext)).setText("");
                            GetPostedList("" + page);
                        } else {
                            Utility.showCroutonWarn(c.getString("msg"), Activity_PostComment.this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException c) {
            c.printStackTrace();
        }
        rt.execute();
    }

    public void GetPostedList(final String page1) {
        try {
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            JSONObject j = new JSONObject();
            j.put(ConstantsUrlKey.ACTION, "CommentListByCenasId");
            j.put("cenas_id", cenas_id);
            j.put("page", page1);
//            j.put("userid", Utility.getSharedPreferences(mContext, ConstantsUrlKey.USERID));

            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    ((LoadMoreListView) findViewById(R.id.commentlist)).onLoadMoreComplete();
                    if (result == null) {
                        page = page - 1;
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_PostComment.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                c_arrayl = new LinkedList<JSONObject>();
                                JSONArray j = jobj.getJSONArray(ConstantsUrlKey.OBJECT);
                                for (int i = 0; i < j.length(); i++) {
                                    c_arrayl.add(j.getJSONObject(i));
                                }
                                adapter = new AdapterCommentlist(mContext, R.layout.adapter_commentlist, c_arrayl);
                                ((LoadMoreListView) findViewById(R.id.commentlist)).setAdapter(adapter);

                                adapter.notifyDataSetChanged();
                            } else {
                                page = page - 1;
                                if (!page1.equals("0")) {
                                    Utility.ShowToastMessage(mContext, getResources().getString(R.string.no_comment_found));
                                } else {
                                    Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_PostComment.this);
                                }
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_PostComment.this);
                        }
                    }
                }
            });

        } catch (JSONException c) {
            c.printStackTrace();
        }
        rt.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public class AdapterCommentlist extends BaseAdapter {
        Context appcontext;
        LinkedList<JSONObject> list;
        int res = 0;

        public AdapterCommentlist(Context context, int res, LinkedList<JSONObject> list) {
            this.appcontext = context;
            this.res = res;
            this.list = list;

        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder vh;
            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = li.inflate(res, null);
                vh = new ViewHolder(convertView);

                convertView.setTag(vh);
            } else {
                vh = (ViewHolder) convertView.getTag();

            }

            JSONObject j = list.get(position);
            try {
                vh.comment_msg.setText(j.getString("comment"));
                vh.comment_user.setText(j.getString("username"));

                Glide.with(mContext).load(j.getString("profile_image"))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                (vh.comment_pb).setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(vh.comment_userimg);

            } catch (JSONException a) {
                a.printStackTrace();
            }
            return convertView;
        }

        public class ViewHolder {
            CustomTextView comment_msg, comment_user;
            ImageView comment_userimg;
            ProgressBar comment_pb;

            public ViewHolder(View b) {
                comment_msg = (CustomTextView) b.findViewById(R.id.comment_msg);
                comment_user = (CustomTextView) b.findViewById(R.id.comment_user);
                comment_userimg = (ImageView) b.findViewById(R.id.comment_userimg);
                comment_pb = (ProgressBar) b.findViewById(R.id.comment_pb);
            }
        }

    }

}
