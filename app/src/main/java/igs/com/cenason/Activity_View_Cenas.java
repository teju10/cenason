package igs.com.cenason;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.mylibrary.CircleIndicator;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;

import igs.com.cenason.Adapter.SamplePagerAdapter;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Tejas on 9/9/2016.
 */
public class Activity_View_Cenas extends AppCompatActivity implements View.OnClickListener,
        FacebookCallback<Sharer.Result> {

    Context appContext;
    Toolbar mToolbar;
    String cat_id, cenas_id, cenas_link, address = "", e_name = "", Videolink = "";
    ResponseTask rt;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    String iscell, user_id;
    ViewPager viewpager;
    CircleIndicator indicator;
    ProgressBar progressBar = null;
    Dialog Sharecustomdialog;
    ImageView fb_btn, twitter_btn, cenas_btn;
    CallbackManager callbackManager;
    ShareDialog shareDialog;
    double lati = 0, longi = 0;
    Bundle b;
    VideoView video_view;
    String Planstatus = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_cenas);
        appContext = this;

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, this);
        TwitterAuthConfig authConfig = TwitterCore.getInstance().getAuthConfig();
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());
        Inti();

        b = new Bundle();
    }

    private void Inti() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        viewpager = (ViewPager) findViewById(R.id.viewpager);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        video_view = (VideoView) findViewById(R.id.video_view);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        Intent i = getIntent();
        Bundle extras = i.getExtras();
        cat_id = extras.getString("catgory_id");
        System.out.println("Category_id ====> " + cat_id);
        if (Utility.isConnectingToInternet(appContext)) {
            Getcenasbyid(cat_id);
        } else {
            Utility.showCroutonWarn(getResources().getString(R.string.error_internet), Activity_View_Cenas.this);
        }
        if (Utility.getSharedPreferences(appContext, ConstantsUrlKey.U_TYPE).equals("4")) {
            (findViewById(R.id.btn_book)).setVisibility(View.GONE);
        } else {
            (findViewById(R.id.btn_book)).setOnClickListener(this);
        }
        (findViewById(R.id.like_icons)).setOnClickListener(this);
//        (findViewById(R.id.unlike_icons)).setOnClickListener(this);
        (findViewById(R.id.share_icon)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.video_view_detail_layout)).setOnClickListener(this);
        (findViewById(R.id.c)).setOnClickListener(this);
        (findViewById(R.id.comment_icon)).setOnClickListener(this);

    }

    public void Getcenasbyid(String catid) {
        System.out.println("user id === > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.CENASDETAILBTID);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.CENASID, catid);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server Result ==> " + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_View_Cenas.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                Planstatus = jobj.getString("plan_status");
                                JSONObject cenasdetail = jobj.getJSONObject(ConstantsUrlKey.OBJECT);
                                if (cenasdetail.getString("isEvent").equals("0")) {
                                    ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.cenas_info));
                                } else if (cenasdetail.getString("isEvent").equals("1")) {
                                    ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.event_info));

                                }
                                ((CustomTextView) findViewById(R.id.p_number)).setText(cenasdetail.getString("contact_number"));
                                ((CustomTextView) findViewById(R.id.event_price)).setText(cenasdetail.getString("price"));
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.EVENTPRICE, cenasdetail.getString("price"));
                                ((CustomTextView) findViewById(R.id.event_name)).setText(cenasdetail.getString("name"));
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.EVENTNAME, cenasdetail.getString("name"));
                                ((CustomTextView) findViewById(R.id.event_address)).setText(cenasdetail.getString("address"));
                                ((CustomTextView) findViewById(R.id.event_discription)).setText(cenasdetail.getString("description"));
                                System.out.println("LII<E-----------" + cenasdetail.getString("cenas_like"));

                                if (cenasdetail.getString("start_time").equals("") && (cenasdetail.getString("end_time").equals(""))) {
                                    ((CustomTextView) findViewById(R.id.event_time)).setText(cenasdetail.getString("hours"));
                                } else {
                                    String strtime[] = cenasdetail.getString("start_time").split("");
                                    String endtime[] = cenasdetail.getString("end_time").split("");
                                    String str,end;
                                    str = strtime[1];
                                    end = endtime[1];
                                    ((CustomTextView) findViewById(R.id.event_time)).setText(cenasdetail.getString("start_time")
                                            + " to " + cenasdetail.getString("end_time"));

                                   /* ((CustomTextView) findViewById(R.id.event_time)).setText(str + " to " + end);*/
                                }
                                String created_date = cenasdetail.getString("created_date");
                                String date[] = created_date.split(" ");

                                ((CustomTextView) findViewById(R.id.event_date)).setText(date[0]);
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.CENASIMAGE, cenasdetail.getString("cenas_image"));
                                if (cenasdetail.getString("isEvent").equals("0")) {
                                    Utility.setSharedPreference(appContext, ConstantsUrlKey.CENASTYPE, "Cenas");
                                } else if (cenasdetail.getString("isEvent").equals("1")) {
                                    Utility.setSharedPreference(appContext, ConstantsUrlKey.CENASTYPE, "Event");
                                }
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.CENASDATE, cenasdetail.getString("from_date"));
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.CENASENDDATE, cenasdetail.getString("to_date"));
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.CENASTIME, cenasdetail.getString("hours"));
                                System.out.println("date ===>" + cenasdetail.getString("from_date"));

                                System.out.println("time ===>" + cenasdetail.getString("start_time"));
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.CENASID, cenasdetail.getString("id"));
                                cenas_id = cenasdetail.getString("id");
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.CENAS_ADD, cenasdetail.getString("address"));
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.DESCRIPTION, cenasdetail.getString("description"));
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.ISEVENT, cenasdetail.getString("isEvent"));
                                Utility.setSharedPreference(appContext, ConstantsUrlKey.QUANTITY_AVAILABLE, cenasdetail.getString("available_qunatity"));
                                iscell = cenasdetail.getString("is_sell");
                                cenas_link = cenasdetail.getString("cenas_url");

                                if (cenasdetail.getString("is_sell").equals("0")) {
                                    (findViewById(R.id.btn_book)).setVisibility(View.GONE);
                                }
                                System.out.println("cenas link =============> " + cenasdetail.getString("cenas_url"));
                                if (cenasdetail.getString("like_status").equals("0")) {
                                    ((CustomTextView) findViewById(R.id.like_icons)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.like, 0, 0);
                                    ((CustomTextView) findViewById(R.id.like_icons)).setText(cenasdetail.getString("cenas_like") + " " + getResources().getString(R.string.like));
                                } else if (cenasdetail.getString("like_status").equals("1")) {
                                    ((CustomTextView) findViewById(R.id.like_icons)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.liked, 0, 0);
                                    ((CustomTextView) findViewById(R.id.like_icons)).setText(cenasdetail.getString("cenas_like") + " " + getResources().getString(R.string.liked));
                                    ((CustomTextView) findViewById(R.id.like_icons)).setClickable(false);
//                                    Utility.ShowToastMessage(appContext,"you already like these cena");
                                } else if (cenasdetail.getString("like_status").equals("2")) {
                                    ((CustomTextView) findViewById(R.id.like_icons)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.like, 0, 0);
                                    ((CustomTextView) findViewById(R.id.like_icons)).setText(cenasdetail.getString("cenas_like") + getResources().getString(R.string.like));
                                }
                                System.out.println("image array ==> " + cenasdetail.getJSONArray("cenas_image"));
                                if (cenasdetail.getJSONArray("cenas_image").length() == 0) {
                                    ((findViewById(R.id.pager_layout))).setVisibility(View.GONE);
                                    ((findViewById(R.id.video_view_detail_layout))).setVisibility(View.VISIBLE);
                                    Uri video = Uri.parse(cenasdetail.getString("cenas_video"));
                                    video_view.setVideoURI(video);
                                    System.out.println("video from server ===> " + cenasdetail.getString("cenas_video"));
                                    video_view.setMediaController(new MediaController(appContext));
                                    video_view.requestFocus();
                                    progressBar.setVisibility(View.VISIBLE);
                                    video_view.start();
                                    video_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                        @Override
                                        public void onPrepared(MediaPlayer mp) {
                                            mp.start();
                                            mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                                                @Override
                                                public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                                                    progressBar.setVisibility(View.GONE);
                                                    mp.start();
                                                }
                                            });
                                        }
                                    });

                                    Videolink = cenasdetail.getString("cenas_video");
                                } else {
                                    ((findViewById(R.id.pager_layout))).setVisibility(View.VISIBLE);
                                    ((findViewById(R.id.video_view_detail_layout))).setVisibility(View.GONE);
                                    viewpager.setAdapter(new SamplePagerAdapter(Activity_View_Cenas.this,
                                            cenasdetail.getJSONArray("cenas_image")));
                                    indicator.setViewPager(viewpager);
                                    viewpager.setCurrentItem(0);
                                }
                                address = cenasdetail.getString("address");
                                e_name = cenasdetail.getString("name");
                                lati = cenasdetail.getDouble("latitude");
                                longi = cenasdetail.getDouble("longitude");

                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_View_Cenas.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_View_Cenas.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    private void Sharewithwitter() {
        if (urlEncode(cenas_link) != null) {
            TweetComposer.Builder builder = new TweetComposer.Builder(Activity_View_Cenas.this)
                    .url(urlEncode(cenas_link));
            builder.show();
        } else {
            Utility.ShowToastMessage(appContext, getResources().getString(R.string.something_wroung));
        }
    }

    public URL urlEncode(String link) {
        try {

            return new URL(link);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void Sharewithfacebook() {
       /* boolean installed = appInstalledOrNot("com.facebook.katana");
        if (installed) {
            shareTextUrl();
        } else {
            //  System.out.println("App is not installed on your phone");
*/
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(cenas_link))
                .build();

        shareDialog.show(Activity_View_Cenas.this, content);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void ShareDialog() {
        Sharecustomdialog = new Dialog(appContext, R.style.MyDialog);
        Sharecustomdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Sharecustomdialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        Sharecustomdialog.setContentView(R.layout.share_btn_dialog);
        Sharecustomdialog.setCancelable(true);
        Sharecustomdialog.show();
        fb_btn = (ImageView) Sharecustomdialog.findViewById(R.id.fb_img);
        twitter_btn = (ImageView) Sharecustomdialog.findViewById(R.id.tw_img);
        cenas_btn = (ImageView) Sharecustomdialog.findViewById(R.id.cenas_img);

        fb_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sharecustomdialog.dismiss();
                Sharewithfacebook();
            }
        });
        twitter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sharecustomdialog.dismiss();
                Sharewithwitter();
            }
        });
        cenas_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sharecustomdialog.dismiss();
                startActivityForResult(new Intent(appContext, Activity_my_friends_list.class).putExtra("CenasId", cenas_id), 35);
            }
        });
        Sharecustomdialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_book:
                if (iscell.equals("1") && Planstatus.equals("0")) {
                    startActivity(new Intent(appContext, Activity_book_cenas.class));
                    finish();
                } else if (Planstatus.equals("1")) {
                    Utility.ShowToastMessage(appContext, appContext.getResources().getString(R.string.upgradeplan_noti));
                    startActivity(new Intent(appContext, Activity_renew_plan.class));
                    finish();
                } else {
                    Utility.showCroutonWarn(getResources().getString(R.string.book_cenas), Activity_View_Cenas.this);
                }
                break;
            case R.id.like_icons:
                if (Utility.isConnectingToInternet(appContext)) {
                    Likecenastask("1");
                } else {
                    Utility.showCrouton(getResources().getString(R.string.error_internet), Activity_View_Cenas.this);
                }
                break;
           /* case R.id.unlike_icons:
                if (Utility.isConnectingToInternet(appContext)) {
                    Dislikecenastask("2");
                } else {
                    Utility.showCrouton(getResources().getString(R.string.error_internet), Activity_View_Cenas.this);
                }
                break;*/
            case R.id.share_icon:
                ShareDialog();
                break;

            case R.id.video_view_detail_layout:
                Intent i = new Intent(appContext, Activity_VideoViewZoom.class);
                b.putString("VideoUrl", Videolink);
                i.putExtras(b);
                startActivity(i);
                break;
            case R.id.c:
                b = new Bundle();
                Intent m = new Intent(appContext, Activity_ViewCenas_OnMap.class);
                b.putString("lat", String.valueOf(lati));
                b.putString("long", String.valueOf(longi));
                b.putString("cname", e_name);
                b.putString("address", address);
                m.putExtras(b);
                startActivity(m);
                break;

            case R.id.comment_icon:
                Intent c = new Intent(appContext, Activity_PostComment.class);
                b.putString("cid", cenas_id);
                c.putExtras(b);
                startActivity(c);
                break;
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 35 && resultCode == RESULT_OK) {
            ShareCenas(data.getStringExtra("ID"));

        }
    }

    private void ShareCenas(String user_id) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.CHAT);
            jsonObject.put(ConstantsUrlKey.FROM_USER, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.TO_USER, user_id);
            jsonObject.put(ConstantsUrlKey.TYPE, "share");
            jsonObject.put(ConstantsUrlKey.CENAS_ID, cenas_id);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_View_Cenas.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                Utility.showCrouton(getResources().getString(R.string.share_Successfuly), Activity_View_Cenas.this);
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_View_Cenas.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_View_Cenas.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void Likecenastask(String status) {
        System.out.println("user id === > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.LIKEANDDISLIKE);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put("cenas_id", cenas_id);
            jsonObject.put(ConstantsUrlKey.LIKESTATUS, status);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server Result ==> " + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_View_Cenas.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                               /* ((CustomTextView) findViewById(R.id.like_icons)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.liked, 0, 0);
                                ((CustomTextView) findViewById(R.id.like_icons)).setText(getResources().getString(R.string.liked));*/
                                Getcenasbyid(cat_id);
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_View_Cenas.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_View_Cenas.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void Dislikecenastask(String status) {
        System.out.println("user id === > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.LIKEANDDISLIKE);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put("cenas_id", cenas_id);
            jsonObject.put(ConstantsUrlKey.LIKESTATUS, status);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server Result ==> " + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_View_Cenas.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                               /* ((CustomTextView) findViewById(R.id.unlike_icons)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.dislike_black, 0, 0);
                                ((CustomTextView) findViewById(R.id.unlike_icons)).setText(getResources().getString(R.string.disliked));*/
                                ((CustomTextView) findViewById(R.id.like_icons)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.like, 0, 0);
                                ((CustomTextView) findViewById(R.id.like_icons)).setText(getResources().getString(R.string.like));

                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_View_Cenas.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_View_Cenas.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    @Override
    public void onSuccess(Sharer.Result result) {
        Utility.showCroutonInfo(getResources().getString(R.string.share_success), Activity_View_Cenas.this);
    }

    @Override
    public void onCancel() {
        Utility.ShowToastMessage(appContext, getResources().getString(R.string.share_cancle));
    }

    @Override
    public void onError(FacebookException error) {
        Utility.ShowToastMessage(appContext, getResources().getString(R.string.something_wroung));
    }

}


