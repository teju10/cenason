package igs.com.cenason;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.GPSTracker;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Shoaib on 25-Aug-16.
 */

public class Activity_hot_spot extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    Context appContext;
    LocationManager mManager;
    GoogleMap googleMap;
    GPSTracker mGPS;
    double lati, longi;
    ImageView imgMyLocation;
    Toolbar mToolbar;
    ResponseTask rt;
    ArrayList<JSONObject> Hotsportlist;
    SupportMapFragment fm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_hot_spot);
        appContext = this;

        imgMyLocation = (ImageView) findViewById(R.id.imgMyLocation);
        imgMyLocation.setOnClickListener(this);
        Inti();
    }

    private void Inti() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.hot_spot));
        mManager = (LocationManager) appContext.getSystemService(Context.LOCATION_SERVICE);
        if (!mManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Utility.AlertNoGPS(appContext);
        }
        mGPS = new GPSTracker(appContext);
        lati = mGPS.getLatitude();
        longi = mGPS.getLongitude();
        System.out.println("lat ===> " + lati);
        System.out.println("long ====> " + longi);
        googleMapCheck();

        //  InitilizeMap();

    }

    private void googleMapCheck() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(appContext);
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {

            fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            fm.getMapAsync(this);
        }
    }

/*
 googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker marker) {
                try {
                    String[] title = marker.getTitle().split(",");
                    String cenasid = title[3];
                    Intent intent = new Intent(appContext, Activity_View_Cenas.class);
                    intent.putExtra("catgory_id",cenasid);
                    appContext.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

*/
/*
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                //  (findViewById(R.id.first_text)).setVisibility(View.GONE);
                // Check if there is an open info window
                if (lastOpenned != null) {
                    // Close the info window
                    lastOpenned.hideInfoWindow();

                    // Is the marker the same marker that was already open
                    if (lastOpenned.equals(marker)) {
                        // Nullify the lastOpenned object
                        lastOpenned = null;
                        // Return so that the info window isn't openned again
                        return true;
                    }
                }
                // Open the info window for the marker
                marker.showInfoWindow();
                // Re-assign the last openned such that we can close it later
                lastOpenned = marker;
                return true;
            }
        });
    }
*/

    private void getMyLocation() {
        LatLng latLng = new LatLng(lati, longi);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
        if (googleMap != null) {
            googleMap.animateCamera(cameraUpdate);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgMyLocation:
                getMyLocation();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext,HomeActivity.class));
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    public void Gethotspotlist() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.HOTSPOT);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("server Result=====>" + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_hot_spot.this);
                    } else {
                        try {
                                JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                               /* for (int i = 0; i < jobj.getJSONArray("cenas_data").length(); i++) {
                                    Hotsportlist.add(jobj.getJSONArray("cenas_data").getJSONObject(i));
                                }
                                JSONArray userdetails = null;
                                userdetails = jobj.getJSONArray("cenas_data");
                                Hotsportlist = new ArrayList<JSONObject>();
                                for (int i = 0; i < userdetails.length(); i++) {
                                    JSONObject jo = null;
                                    jo = userdetails.getJSONObject(i);
                                    Hotsportlist.add(jo);
                                    Log.d("list of Driver", "" + jo);
                                    AddMarkeronmap();*/
                                JSONArray userdetails = null;
                                userdetails = jobj.getJSONArray(ConstantsUrlKey.OBJECT);
                                Hotsportlist = new ArrayList<JSONObject>();
                                for (int i = 0; i < userdetails.length(); i++) {
                                    JSONObject jo = null;
                                    jo = userdetails.getJSONObject(i);
                                    Hotsportlist.add(jo);
                                    Log.d("list of cenas", "" + jo);
                                    AddMarkeronmap();
                                }
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_hot_spot.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_hot_spot.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

/*
    public void AddMarkeronmap() {
        System.out.println("inside add marker");
        if (Hotsportlist != null) {
            for (int i = 0; i < Hotsportlist.size(); i++) {
                try {
                    LatLng l = new LatLng(Double.parseDouble(Hotsportlist.get(i).getString("latitude")), Double.parseDouble(Hotsportlist.get(i).getString("longitude")));
                    System.out.println("Lat Long is   " + l);
                    googleMap.addMarker(new MarkerOptions().position(l).title(Hotsportlist.get(i).getString("name"))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
*/

    public void AddMarkeronmap() {
        if (Hotsportlist != null) {
            for (int i = 0; i < Hotsportlist.size(); i++) {
                try {
                    LatLng l = new LatLng(Double.parseDouble(Hotsportlist.get(i).getString("latitude")), Double.parseDouble(Hotsportlist.get(i).getString("longitude")));
                    System.out.println("Lat Long is   " + l);
                        googleMap.addMarker(new MarkerOptions().position(l).title(Hotsportlist.get(i).getString("name")).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap mgoogleMap) {
        googleMap = mgoogleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);

        // Enable / Disable Zooming Controls
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        // Enable / Disable My Location Button
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        // Enable / Disable Compass Icon
        googleMap.getUiSettings().setCompassEnabled(true);

        // Enable / Disable Rotate Gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);

        // Enable / Disable Zooming Functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        getMyLocation();
        if (Utility.isConnectingToInternet(appContext)) {
            Gethotspotlist();
        } else {
            Utility.showCrouton(getResources().getString(R.string.error_internet), Activity_hot_spot.this);
        }

    }
}