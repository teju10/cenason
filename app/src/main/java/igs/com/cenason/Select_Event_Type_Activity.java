package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioGroup;

import com.gc.materialdesign.views.ButtonRectangle;

import igs.com.cenason.CustomWidget.CustomRadioButton;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 9/2/2016.
 */
public class Select_Event_Type_Activity extends AppCompatActivity {

    Context AppContext;
    RadioGroup select_event;
    CustomRadioButton food_event, music_event;
    ButtonRectangle select_btn;
    String eventtype = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_type);
        AppContext = this;
        Inti();
    }

    private void Inti(){
        select_event = (RadioGroup) findViewById(R.id.select_event);
        food_event = (CustomRadioButton) findViewById(R.id.food_event);
        music_event = (CustomRadioButton) findViewById(R.id.music_event);

        select_event.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (food_event.isChecked()) {
                    eventtype = "0";
                } else if (music_event.isChecked()) {
                    eventtype = "1";
                }
            }
        });

        select_btn = (ButtonRectangle)findViewById(R.id.select_btn);
        select_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eventtype.equals("0")) {
                    Utility.setSharedPreference(AppContext, ConstantsUrlKey.EVENTTYPE,"food");
                      SwitchActivity();
                } else if (eventtype.equals("1")) {
                    Utility.setSharedPreference(AppContext, ConstantsUrlKey.EVENTTYPE,"music");
                    SwitchActivity();
                } else {
                    Utility.ShowToastMessage(AppContext, getResources().getString(R.string.please_selcect_one));
                }
            }
        });
    }

    public void SwitchActivity() {
        startActivity(new Intent(AppContext, Activity_Upload_Cenas.class));
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }
}
