package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.mylibrary.CircleIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import igs.com.cenason.Adapter.SamplePagerAdapter;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 9/5/2016.
 */

public class Activity_Cenas_info extends AppCompatActivity {

    Context appContext;
    String date, cenasid;
    Toolbar mToolbar;
    ResponseTask rt;
    ViewPager viewpager;
    CircleIndicator indicator;
    ProgressBar progressBar = null;
    private VideoView videoPreview;
    String Planstatus = "";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_cenas_detail);
        appContext = this;
        GetDate();
        Inti();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext, My_cenas_Activity.class));
        finish();

    }

    private void Inti() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.info));

        videoPreview = (VideoView) findViewById(R.id.video_view);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
    }

    private void GetDate() {
        try {
            date = getIntent().getStringExtra(ConstantsUrlKey.CENAS_DATA);
            JSONObject jobj = new JSONObject(date);
            cenasid = jobj.getString("id");
            System.out.println("cenas id ==> " + cenasid);
            getcenasbyid(cenasid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getcenasbyid(String cenasid) {
        System.out.println("user id === > " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.CENASDETAILBTID);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.CENASID, cenasid);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server Result ==> " + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_Cenas_info.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                JSONObject cenasdetail = jobj.getJSONObject(ConstantsUrlKey.OBJECT);
                                if(cenasdetail.getString("isEvent").equals("0")){
                                    ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.cenas_info));
                                }else if(cenasdetail.getString("isEvent").equals("1")){
                                    ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.event_info));

                                }
                                ((CustomTextView) findViewById(R.id.quantity_order)).setText(cenasdetail.getString("quantity"));
                                ((CustomTextView) findViewById(R.id.p_number)).setText(cenasdetail.getString("contact_number"));
                                ((CustomTextView) findViewById(R.id.event_price)).setText(cenasdetail.getString("price"));
                                ((CustomTextView) findViewById(R.id.event_name)).setText(cenasdetail.getString("name"));
                                ((CustomTextView) findViewById(R.id.event_address)).setText(cenasdetail.getString("address"));
                                ((CustomTextView) findViewById(R.id.event_discription)).setText(cenasdetail.getString("description"));
                                System.out.println("image array ==> " + cenasdetail.getJSONArray("cenas_image").length());
                                if (cenasdetail.getJSONArray("cenas_image").length() == 0) {
                                    ((findViewById(R.id.pager_layout))).setVisibility(View.GONE);
                                    ((findViewById(R.id.video_view_layout))).setVisibility(View.VISIBLE);
                                    Uri video = Uri.parse(cenasdetail.getString("cenas_video"));
                                    videoPreview.setVideoURI(video);
                                    System.out.println("video from server ===> " + cenasdetail.getString("cenas_video"));
                                    videoPreview.setMediaController(new MediaController(Activity_Cenas_info.this));
                                    videoPreview.requestFocus();
                                    progressBar.setVisibility(View.VISIBLE);
                                    videoPreview.start();
                                    videoPreview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                        @Override
                                        public void onPrepared(MediaPlayer mp) {
                                            mp.start();
                                            mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                                                @Override
                                                public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                                                    progressBar.setVisibility(View.GONE);
                                                    mp.start();
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    ((findViewById(R.id.pager_layout))).setVisibility(View.VISIBLE);
                                    ((findViewById(R.id.video_view_layout))).setVisibility(View.GONE);
                                    viewpager.setAdapter(new SamplePagerAdapter(Activity_Cenas_info.this, cenasdetail.getJSONArray("cenas_image")));
                                    indicator.setViewPager(viewpager);
                                    viewpager.setCurrentItem(0);
                                }
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_Cenas_info.this);
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_Cenas_info.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }
}
