package igs.com.cenason;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioGroup;

import com.gc.materialdesign.views.ButtonRectangle;

import igs.com.cenason.CustomWidget.CustomRadioButton;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 9/1/2016.
 */
public class Activity_select_cenas_type extends AppCompatActivity {

    Context AppContext;
    RadioGroup select_cenas;
    CustomRadioButton simple_cenas, cenas_event;
    ButtonRectangle done_btn;
    String cenasType = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_cenas);
        AppContext = this;
        Inti();
    }

    private void Inti(){
        select_cenas = (RadioGroup) findViewById(R.id.select_cenas);
        simple_cenas = (CustomRadioButton) findViewById(R.id.simple_cenas);
        cenas_event = (CustomRadioButton) findViewById(R.id.cenas_event);

        select_cenas.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (simple_cenas.isChecked()) {
                    cenasType = "0";
                } else if (cenas_event.isChecked()) {
                    cenasType = "1";
                }
            }
        });
//event = 1  cenas = 0
        done_btn = (ButtonRectangle)findViewById(R.id.cenas_done);
        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cenasType.equals("0")) {
                   Utility.setSharedPreference(AppContext,ConstantsUrlKey.ISEVENT_TYPE,"0");
                    startActivity(new Intent(AppContext, Activity_Upload_Cenas.class));
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                    finish();
                } else if (cenasType.equals("1")) {
                    Utility.setSharedPreference(AppContext,ConstantsUrlKey.ISEVENT_TYPE,"1");
                    SwitchActivity();
                } else {
                    Utility.ShowToastMessage(AppContext, getResources().getString(R.string.please_selcect_one));
                }
            }
        });
    }

    public void SwitchActivity() {
        startActivity(new Intent(AppContext, Select_Event_Type_Activity.class));
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }
}
