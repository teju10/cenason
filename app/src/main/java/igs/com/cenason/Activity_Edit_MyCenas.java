package igs.com.cenason;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.VideoView;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.borax12.materialdaterangepicker.timewithday.TimeWithDayPickerDialog;
import com.bumptech.glide.Glide;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import igs.com.cenason.Adapter.CategoryAdapter;
import igs.com.cenason.Adapter.CategoryItems;
import igs.com.cenason.Adapter.DayTimeAdapterForEdit;
import igs.com.cenason.CustomWidget.CustomAutoCompleteTextView;
import igs.com.cenason.CustomWidget.CustomEditText;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.FileUploader;
import igs.com.cenason.Utilities.GetActualFilePath;
import igs.com.cenason.Utilities.Helper;
import igs.com.cenason.Utilities.PlaceJSONParser;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 8/31/2016.
 */

public class Activity_Edit_MyCenas extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener, TimeWithDayPickerDialog.OnTimeSetListener {

    Context appContext;
    Toolbar mToolbar;
    String picturePath = "", mCropImageUri, path = null, picturePath1 = "",
            picturePath2 = "", picturePath3 = "", picturePath4 = "", videopath = "", thumbnailpath = "";
    String date, cenas_id, is_event, cenas_type;
    ResponseTask rt;
    ArrayList<CategoryItems> mPersonList = new ArrayList<>();
    Dialog categoryDialog;
    ArrayList<String> categoryNameList = new ArrayList<>();
    LinearLayoutManager llm;
    CategoryAdapter adapter;
    ParserTask parserTask;
    CustomAutoCompleteTextView address;
    PlacesTask placesTask;
    CustomTextView dateTextView, timeTextView;
    ArrayList<String> lastselected = new ArrayList<>();
    ProgressBar progressBar = null;
    int image_value = 0;
    ArrayList<String> imgPaths;
    ArrayList<String> daytimelist = new ArrayList<>();
    DayTimeAdapterForEdit listadapter;
    Bitmap bmThumbnail;
    String[] hours;
    private VideoView videoPreview;
    SharedPreferences share;
    SharedPreferences.Editor edit;

    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static File SaveFileIntoDir(String filepath, Context appContext) {
        File directory = appContext.getDir(filepath, Context.MODE_PRIVATE);
        return directory;
    }

    protected final static int getResourceID(final String resName, final String resType, final Context ctx) {
        final int ResourceID = ctx.getResources().getIdentifier(resName, resType, ctx.getApplicationInfo().packageName);
        if (ResourceID == 0) {
            throw new IllegalArgumentException("No resource string found with name " + resName);
        } else {
            return ResourceID;
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_my_cenas_new);
        appContext = this;
        Init();
       
    }

    private void Init() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText(appContext.getResources().getString(R.string.my_cenas));
//        (findViewById(R.id.select_img)).setOnClickListener(this);
        (findViewById(R.id.btn_Edit_cenas)).setOnClickListener(this);
        (findViewById(R.id.cenas_secltion)).setOnClickListener(this);
        dateTextView = (CustomTextView) findViewById(R.id.start_end_date);
        timeTextView = (CustomTextView) findViewById(R.id.start_end_time);
        (findViewById(R.id.start_end_date)).setOnClickListener(this);
        (findViewById(R.id.start_end_time)).setOnClickListener(this);
        address = (CustomAutoCompleteTextView) findViewById(R.id.event_address);
        GetDate();
        SetListeners();
        //imgPaths = new ArrayList<>();
        videoPreview = (VideoView) findViewById(R.id.video_view);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        ((findViewById(R.id.change_video))).setOnClickListener(this);
        ((findViewById(R.id.add_hours))).setOnClickListener(this);
        (findViewById(R.id.upload_image_0)).setOnClickListener(this);
        (findViewById(R.id.upload_image_1)).setOnClickListener(this);
        (findViewById(R.id.upload_image_2)).setOnClickListener(this);
        (findViewById(R.id.upload_image_3)).setOnClickListener(this);
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception while dol", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onTimeSet(com.borax12.materialdaterangepicker.timewithday.RadialPickerLayout view,
                          int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd, String dayofmonth) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String hourStringEnd = hourOfDayEnd < 10 ? "0" + hourOfDayEnd : "" + hourOfDayEnd;
        String minuteStringEnd = minuteEnd < 10 ? "0" + minuteEnd : "" + minuteEnd;
        String time = Time(hourString + ":" + minuteString) + " To " + Time(hourStringEnd + ":" + minuteStringEnd) + "Day is :- " + dayofmonth;
        boolean add = true;
        if (daytimelist.size() > 0) {
            for (int i = 0; i < daytimelist.size(); i++) {
                if ((daytimelist.get(i).split("-")[0]).equals(dayofmonth)) {
                    add = false;
                    Utility.showCrouton("Day is already selected please select another day", Activity_Edit_MyCenas.this);
                    break;
                }
            }
        }
        if (add) {
            System.out.println("Time ==> " + time);
            if (CheckDateTimeValidations(hourString + ":" + minuteString, hourStringEnd + ":" + minuteStringEnd)) {
                daytimelist.add(dayofmonth + "-" + Time(hourString + ":" + minuteString) + "-" + Time(hourStringEnd + ":" + minuteStringEnd));
                listadapter.notifyDataSetChanged();
                Helper.getListViewSize(((ListView) findViewById(R.id.add_hours_list)));
            }
        }
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service

    private boolean CheckDateTimeValidations(String starttime, String endtime) {
        try {
            Date date1, date2;

            SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
            date1 = parser.parse(starttime);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date1);
            cal.add(Calendar.MINUTE, 14);
            date1 = parser.parse(parser.format(cal.getTime()));
            System.out.println("date1" + date1);
            date2 = parser.parse(endtime);
            System.out.println("date2" + date2);
            if (date1.before(date2)) {
                return true;
            } else {
                Utility.showCroutonWarn(getResources().getString(R.string.msg2), Activity_Edit_MyCenas.this);
                return false;
            }
        } catch (ParseException ex) {

        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(appContext, My_cenas_Activity.class));
        finish();
    }

    public void SpinnerValueTask() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.SpinnerAction);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    Log.e("Server result", "Server result ===>" + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_Edit_MyCenas.this);
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equals("1")) {
                                JSONObject jobj = new JSONObject(getIntent().getStringExtra(ConstantsUrlKey.CENAS_DATA));
                                mPersonList = new ArrayList<>();
                                for (int i = 0; i < json.getJSONArray(ConstantsUrlKey.OBJECT).length(); i++) {
                                    mPersonList.add(new CategoryItems(json.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i)
                                            .getString("category"), false));
                                    for (int j = 0; j < jobj.getString("category").split(",").length; j++) {
                                        CategoryItems ci = new CategoryItems(jobj.getString("category").split(",")[j], true);
                                        if ((jobj.getString("category").split(",")[j]).replaceAll(" ", "").equals(json.getJSONArray("object").getJSONObject(i)
                                                .getString("category").replaceAll(" ", ""))) {
                                            mPersonList.remove(i);
                                            mPersonList.add(i, ci);
                                            ((CustomTextView) findViewById(R.id.cenas_secltion)).
                                                    setText(jobj.getString("category").split(",")[j]);
                                            categoryNameList.add(mPersonList.get(i).getName());

                                        }
                                    }
                                }
                                getcenasbyid(cenas_id);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        rt.execute();
    }

    public void selectImage() {
        final CharSequence[] options = {appContext.getResources().getString(R.string.from_camera), appContext.getResources().getString(R.string.from_gallery),
                appContext.getResources().getString(R.string.Close)};
        AlertDialog.Builder builder = new AlertDialog.Builder(appContext);
        builder.setTitle(appContext.getResources().getString(R.string.add_photo));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(appContext.getResources().getString(R.string.from_camera))) {
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    // File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                } else if (options[item].equals(appContext.getResources().getString(R.string.from_gallery))) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals(appContext.getResources().getString(R.string.Close))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void selectVideo() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 3);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            } else if (requestCode == 3) {
                if (resultCode == RESULT_OK) {
                    Uri selectedImageUri = data.getData();
                    videopath = GetActualFilePath.getPath(appContext, selectedImageUri);
                    Log.e("Upload Cenas", "videoPath ======= " + videopath);
                    try {
                        MediaPlayer mp = MediaPlayer.create(this, Uri.parse(videopath));
                        int duration = mp.getDuration();
                        mp.release();
                        Log.e("Upload Cenas", "File duration : " + duration + " min");
                        //     System.out.println("file.getName ======================= " + mp.getName());
                        if (duration / 1000 > 60) {
                            Utility.showCroutonWarn("Upload max 1 min video", Activity_Edit_MyCenas.this);
                            ((findViewById(R.id.change_video))).setVisibility(View.VISIBLE);
                        } else {
                            previewVideo();
                        }
                    } catch (Exception e) {
                        System.out.println("File not found : " + e.getMessage() + e);
                    }
                }
            }
        }
    }

    private void previewVideo() {
        try {
            ((findViewById(R.id.video_view_layout))).setVisibility(View.VISIBLE);
            ((findViewById(R.id.change_video))).setVisibility(View.VISIBLE);
            videoPreview.setVideoPath(videopath);
            videoPreview.setMediaController(new MediaController(appContext));
            videoPreview.start();
            bmThumbnail = ThumbnailUtils.createVideoThumbnail(videopath, MediaStore.Video.Thumbnails.MICRO_KIND);
            System.out.println("video thumbnail ==>" + bmThumbnail);
            thumbnailpath = Utility.SaveImage(bmThumbnail, String.valueOf(System.currentTimeMillis()), ConstantsUrlKey.SDCARD_ROOT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == Activity.RESULT_OK) {
            if (image_value == 1) {
                String croped_file = "" + Crop.getOutput(result).getPath();
                picturePath1 = croped_file;
                Glide.with(appContext).load(picturePath1).into((ImageView) findViewById(R.id.upload_image_0));
                //imgPaths.add(picturePath1);
            } else if (image_value == 2) {
                String croped_file = "" + Crop.getOutput(result).getPath();
                picturePath2 = croped_file;
                Glide.with(appContext).load(picturePath2).into((ImageView) findViewById(R.id.upload_image_1));
                //  imgPaths.add(picturePath2);
            } else if (image_value == 3) {
                String croped_file = "" + Crop.getOutput(result).getPath();
                picturePath3 = croped_file;
                Glide.with(appContext).load(picturePath3).into((ImageView) findViewById(R.id.upload_image_2));
                //   imgPaths.add(picturePath3);
            } else if (image_value == 4) {
                String croped_file = "" + Crop.getOutput(result).getPath();
                picturePath4 = croped_file;
                Glide.with(appContext).load(picturePath4).into((ImageView) findViewById(R.id.upload_image_3));
                //  imgPaths.add(picturePath4);
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(appContext, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void beginCrop(Uri source) {
        File pro = new File(MakeDir("CenasOn", appContext), System.currentTimeMillis() + "pro.jpg");
        Uri destination1 = Uri.fromFile(pro);
        Crop.of(source, destination1).start(this);
    }

    public String MakeDir(String filepath, Context appContext) {
        File path;
        if (!isExternalStorageAvailable() && !isExternalStorageReadOnly()) {
            path = new File(SaveFileIntoDir(filepath, appContext), filepath);
            if (!path.exists()) {
                path.mkdirs();
            }
        } else {
            path = new File(appContext.getExternalFilesDir(filepath), filepath);
            if (!path.exists()) {
                path.mkdirs();
            }
        }
        return path.toString();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        } else {
            Toast.makeText(this, "Required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    public void removeDatetime(int pos) {
        daytimelist.remove(pos);
        listadapter.notifyDataSetChanged();
        Helper.getListViewSize(((ListView) findViewById(R.id.add_hours_list)));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.upload_image_0:
                image_value = 1;
                selectImage();
                break;

            case R.id.upload_image_1:
                image_value = 2;
                selectImage();
                break;

            case R.id.upload_image_2:
                image_value = 3;
                selectImage();
                break;

            case R.id.upload_image_3:
                image_value = 4;
                selectImage();
                break;

            case R.id.cenas_secltion:
                CategoryDialog();
                break;

            case R.id.start_end_date:
                Date();
                break;

            case R.id.start_end_time:
                Time();

            case R.id.btn_Edit_cenas:
                CheckValidationforuploadcenas();
                break;

            case R.id.add_hours:
                DialogForAddHours();
                break;

            case R.id.change_video:
                selectVideo();
                break;
        }
    }

    public void CategoryDialog() {
        categoryDialog = new Dialog(appContext);
        categoryDialog.setCanceledOnTouchOutside(true);
        categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        categoryDialog.setContentView(R.layout.dialog_category_layout);
        categoryDialog.getWindow().getAttributes().windowAnimations = R.style.NewAnimForDialog;
        categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        categoryDialog.findViewById(R.id.recyclerView);
        setRecyclerView();

        (categoryDialog.findViewById(R.id.cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryNameList = new ArrayList<>();
                categoryDialog.dismiss();
            }
        });

        categoryDialog.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                categoryNameList = new ArrayList<>();
                for (int i = 0; i < mPersonList.size(); i++) {
                    if (mPersonList.get(i).isSelected()) {
                        categoryNameList.add(mPersonList.get(i).getName());
                    }
                }
                if (lastselected.size() > 0) {
                    ((CustomTextView) findViewById(R.id.cenas_secltion)).setText(lastselected.get(0));
                } else {
                    ((CustomTextView) findViewById(R.id.cenas_secltion)).setText(getResources().getString(R.string.spinner));
                }
                categoryDialog.dismiss();
            }
        });
        categoryDialog.show();
    }

    private void setRecyclerView() {
        llm = new LinearLayoutManager(Activity_Edit_MyCenas.this);
        ((RecyclerView) categoryDialog.findViewById(R.id.recyclerView)).setLayoutManager(llm);

        adapter = new CategoryAdapter(mPersonList);
        ((RecyclerView) categoryDialog.findViewById(R.id.recyclerView)).setAdapter(adapter);
        adapter.setOnItemClickListener(new CategoryAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View v, int position) {
                itemListClicked(position);
            }
        });
    }

    public void itemListClicked(int position) {
        CategoryItems person = mPersonList.get(position);
        boolean isSelected = person.isSelected();
        if (isSelected) {
            lastselected.remove(person.getName());
        } else {
            lastselected.add(person.getName());
        }
        adapter.setItemSelected(position, !isSelected);
    }

    public void Date() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, 14);
        DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                Activity_Edit_MyCenas.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
//        dpd.setMinDate(now);
        dpd.setThemeDark(true);
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    public void Time(){
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                Activity_Edit_MyCenas.this,
//                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.DAY_OF_WEEK),
                now.get(Calendar.MINUTE), false);
        tpd.setThemeDark(true);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
            }
        });
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear,
                          int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String date = year + "-" + (++monthOfYear) + "-" + dayOfMonth + " To "
                + yearEnd + "-" + (++monthOfYearEnd) + "-" + dayOfMonthEnd;
        dateTextView.setText(date);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String hourStringEnd = hourOfDayEnd < 10 ? "0" + hourOfDayEnd : "" + hourOfDayEnd;
        String minuteStringEnd = minuteEnd < 10 ? "0" + minuteEnd : "" + minuteEnd;
        String time = Time(hourString + ":" + minuteString) + " To " + Time(hourStringEnd + ":" + minuteStringEnd);
        timeTextView.setText(time);
    }

    public String Time(String time) {
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
        String str = null;
        try {
            Date date = parseFormat.parse(time);
            str = displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String Time2(String time) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        String str = null;
        try {
            Date date = parseFormat.parse(time);
            str = displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public void CheckValidationforuploadcenas() {
        if (categoryNameList == null || categoryNameList.size() == 0) {
            Utility.showCroutonWarn(getResources().getString(R.string.category_msg), Activity_Edit_MyCenas.this);

        } /*else if (((CustomEditText) findViewById(R.id.quantity_order)).getText().toString().equals("")) {
            Utility.showCroutonWarn(getResources().getString(R.string.add_quantity), Activity_Edit_MyCenas.this);

        }*/ else if (((CustomEditText) findViewById(R.id.phone_no)).getText().toString().equals("")) {
            Utility.showCroutonWarn(getResources().getString(R.string.add_phone), Activity_Edit_MyCenas.this);

        } else if (((CustomEditText) findViewById(R.id.event_price)).getText().toString().equals("")) {
            Utility.showCroutonWarn(getResources().getString(R.string.add_price), Activity_Edit_MyCenas.this);

        } else if (((CustomEditText) findViewById(R.id.event_name)).getText().toString().equals("")) {
            Utility.showCroutonWarn(getResources().getString(R.string.add_name), Activity_Edit_MyCenas.this);

        } else if (((CustomAutoCompleteTextView) findViewById(R.id.event_address)).getText().toString().equals("")) {
            Utility.showCroutonWarn(getResources().getString(R.string.add_address), Activity_Edit_MyCenas.this);

        } else if (((CustomEditText) findViewById(R.id.event_discription)).getText().toString().equals("")) {
            Utility.showCroutonWarn(getResources().getString(R.string.add_discrip), Activity_Edit_MyCenas.this);

        } else if (is_event.equals("1") && cenas_type.equals("music") && ((CustomEditText) findViewById(R.id.singer_name)).getText().toString().equals("")) {
            Utility.showCroutonWarn(getResources().getString(R.string.add_singer_name), Activity_Edit_MyCenas.this);
        } else if (!Utility.isConnectingToInternet(appContext)) {
            Utility.showCroutonWarn(getResources().getString(R.string.error_internet), Activity_Edit_MyCenas.this);

        } else if (is_event.equals("1")) {
            CondionCheck2();
        } else {
            CondionCheck3();
        }
    }

    private void CondionCheck2() {
        if (((CustomTextView) findViewById(R.id.start_end_date)).getText().toString().equals("Select Start and End Date")) {
            Utility.showCroutonWarn(getResources().getString(R.string.select_date), Activity_Edit_MyCenas.this);

        } else if (((CustomTextView) findViewById(R.id.start_end_time)).getText().toString().equals("Select Start and End Time")) {
            Utility.showCroutonWarn(getResources().getString(R.string.select_time), Activity_Edit_MyCenas.this);
        } else if (CheckDateTimeValidations(((CustomTextView) findViewById(R.id.start_end_date)).getText().toString().split(" To ")[0],
                ((CustomTextView) findViewById(R.id.start_end_date)).getText().toString().split(" To ")[1],
                Time2(((CustomTextView) findViewById(R.id.start_end_time)).getText().toString().split(" To ")[0]),
                Time2(((CustomTextView) findViewById(R.id.start_end_time)).getText().toString().split(" To ")[1]))) {
            buildAlertforissell();
        }
    }

    private void CondionCheck3() {
        if (daytimelist.size() == 0) {
            Utility.showCroutonWarn(getResources().getString(R.string.select_hours), Activity_Edit_MyCenas.this);
        } else {
            buildAlertforissell();
        }
    }

    private boolean CheckDateTimeValidations(String startdate, String enddate, String starttime, String endtime) {
        try {
            Date date1, date2;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date1 = sdf.parse(startdate);
            date2 = sdf.parse(enddate);
            if (date1.compareTo(date2) > 0) {
                Utility.showCroutonWarn(getResources().getString(R.string.msg1), Activity_Edit_MyCenas.this);
                return false;
            } else if (date1.compareTo(date2) < 0) {
                return true;
            } else if (date1.compareTo(date2) == 0) {
                SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
                date1 = parser.parse(starttime);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date1);
                cal.add(Calendar.MINUTE, 14);
                date1 = parser.parse(parser.format(cal.getTime()));
                System.out.println("date1" + date1);
                date2 = parser.parse(endtime);
                System.out.println("date2" + date2);
                if (date1.before(date2)) {
                    return true;
                } else {
                    Utility.showCroutonWarn(getResources().getString(R.string.msg2), Activity_Edit_MyCenas.this);
                    return false;
                }
            }
        } catch (ParseException ex) {

        }
        return false;
    }

    public void getcenasbyid(String cenasid) {
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.CENASDETAILBTID);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.CENASID, cenasid);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server Result ==> " + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_Edit_MyCenas.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                JSONObject cenasdetail = jobj.getJSONObject(ConstantsUrlKey.OBJECT);
                                ((CustomTextView) findViewById(R.id.quantity_order)).setText(cenasdetail.getString("quantity"));
                                ((CustomEditText) findViewById(R.id.phone_no)).setText(cenasdetail.getString("contact_number"));
                                ((CustomEditText) findViewById(R.id.event_price)).setText(cenasdetail.getString("price"));
                                ((CustomEditText) findViewById(R.id.event_name)).setText(cenasdetail.getString("name"));
                                ((CustomAutoCompleteTextView) findViewById(R.id.event_address)).setText(cenasdetail.getString("address"));
                                ((CustomEditText) findViewById(R.id.event_discription)).setText(cenasdetail.getString("description"));
                                if (!cenasdetail.getString("hours").equals("")) {
                                    hours = cenasdetail.getString("hours").split(",");
                                    for (int i = 0; i < hours.length; i++) {
                                        daytimelist.add(hours[i]);
                                    }
                                    listadapter.notifyDataSetChanged();
                                    Helper.getListViewSize(((ListView) findViewById(R.id.add_hours_list)));
                                }
                                if (cenasdetail.getString("isEvent").equals("1")) {
                                    ((CustomTextView) findViewById(R.id.start_end_date)).setText(cenasdetail.getString("from_date") + " To " + cenasdetail.getString("to_date"));
                                    ((CustomTextView) findViewById(R.id.start_end_time)).setText(Time(cenasdetail.getString("start_time")) + " To " + Time(cenasdetail.getString("end_time")));
                                    ((CustomEditText) findViewById(R.id.singer_name)).setText(cenasdetail.getString("singer_name"));
                                }
                                System.out.println("image array ==> " + cenasdetail.getJSONArray("cenas_image").length());
                                if (cenasdetail.getJSONArray("cenas_image").length() == 0) {
                                    System.out.println("inside video");
                                    ((findViewById(R.id.image_layout))).setVisibility(View.GONE);
                                    ((findViewById(R.id.video_view_layout))).setVisibility(View.VISIBLE);
                                    ((findViewById(R.id.change_video))).setVisibility(View.VISIBLE);
                                    Uri video = Uri.parse(cenasdetail.getString("cenas_video"));
                                    videoPreview.setVideoURI(video);
                                    System.out.println("video from server ===> " + cenasdetail.getString("cenas_video"));
                                    videoPreview.setMediaController(new MediaController(Activity_Edit_MyCenas.this));
                                    videoPreview.requestFocus();
                                    progressBar.setVisibility(View.VISIBLE);
                                    videoPreview.start();
                                    videoPreview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                        @Override
                                        public void onPrepared(MediaPlayer mp) {
                                            mp.start();
                                            mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                                                @Override
                                                public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                                                    progressBar.setVisibility(View.GONE);
                                                    mp.start();
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    try {
                                        JSONArray jsonArray = new JSONArray(cenasdetail.get("cenas_image").toString());
                                        for (int m = 0; m < jsonArray.length(); m++) {
                                            Glide.with(appContext).load(jsonArray.getString(m)).into((ImageView)
                                                    findViewById(getResourceID("upload_image_" + m, "id", appContext)));
                                            //  Utility.SaveImage(jsonArray.getString(m)), String.valueOf(System.currentTimeMillis()), ConstantsUrlKey.SDCARD_ROOT);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_Edit_MyCenas.this);
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_Edit_MyCenas.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void DialogForAddHours() {
        Calendar now = Calendar.getInstance();
        TimeWithDayPickerDialog tpd = TimeWithDayPickerDialog.newInstance(
                Activity_Edit_MyCenas.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setThemeDark(true);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
            }
        });
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    private void GetDate() {
        try {
            date = getIntent().getStringExtra(ConstantsUrlKey.CENAS_DATA);
            JSONObject jobj = new JSONObject(date);
            cenas_id = jobj.getString("id");
            System.out.println("cenas id ====> " + cenas_id);
            is_event = jobj.getString("isEvent");
            cenas_type = jobj.getString("cenas_type");
            System.out.println("cenas type ==> " + cenas_type);
            if (is_event.equals("0") && cenas_type.equals("")) {
                ((findViewById(R.id.date_time_layout))).setVisibility(View.GONE);
                ((findViewById(R.id.singer_name))).setVisibility(View.GONE);
                ((findViewById(R.id.add_hours))).setVisibility(View.VISIBLE);
                listadapter = new DayTimeAdapterForEdit(appContext, daytimelist);
                ((ListView) findViewById(R.id.add_hours_list)).setAdapter(listadapter);
            } else {
                ((findViewById(R.id.date_time_layout))).setVisibility(View.VISIBLE);
                ((findViewById(R.id.singer_name))).setVisibility(View.VISIBLE);
                ((findViewById(R.id.add_hours))).setVisibility(View.GONE);
            }
            for (int i = 0; i < jobj.getString("category").split(",").length; i++) {
                CategoryItems ci = new CategoryItems(jobj.getString("category").split(",")[i], true);
                mPersonList.add(ci);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SpinnerValueTask();
    }

    private void buildAlertforissell() {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setMessage(getResources().getString(R.string.issell_msg))
                .setPositiveButton(getResources().getString(R.string.logout_yes), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        String cats = categoryNameList.toString().substring(1, categoryNameList.toString().length() - 1);
                        String datetime = "";
                        if (daytimelist.size() == 0 && !is_event.equals("1")) {
                            Utility.showCroutonWarn(getResources().getString(R.string.select_hours_msg), Activity_Edit_MyCenas.this);
                        } else {
                            datetime = daytimelist.toString();
                            datetime = datetime.substring(1, datetime.length() - 1);
                            System.out.println("data for send server ==> " + datetime.replaceAll(", ", ","));
                            if (is_event.equals("1")) {
                                if (Utility.isConnectingToInternet(appContext)) {
                                    new Update_Cenas(((CustomEditText) findViewById(R.id.event_name)).getText().toString(),
                                            ((CustomEditText) findViewById(R.id.event_discription)).getText().toString(),
                                            ((CustomAutoCompleteTextView) findViewById(R.id.event_address)).getText().toString(),
                                            ((CustomEditText) findViewById(R.id.phone_no)).getText().toString(),
//                                            ((CustomEditText) findViewById(R.id.quantity_order)).getText().toString(),
                                            ((CustomTextView) findViewById(R.id.start_end_date)).getText().toString().split(" To ")[0],
                                            ((CustomTextView) findViewById(R.id.start_end_date)).getText().toString().split(" To ")[1],
                                            Time2(((CustomTextView) findViewById(R.id.start_end_time)).getText().toString().split(" To ")[0]),
                                            Time2(((CustomTextView) findViewById(R.id.start_end_time)).getText().toString().split(" To ")[1]),
                                            cats,
                                            ((CustomEditText) findViewById(R.id.event_price)).getText().toString(), "1",
                                            ((CustomEditText) findViewById(R.id.singer_name)).getText().toString(), datetime.replaceAll(", ", ",")
                                    ).execute();
                                } else {
                                    Utility.showCroutonWarn(getResources().getString(R.string.error_internet), Activity_Edit_MyCenas.this);
                                }
                            } else {
                                if (Utility.isConnectingToInternet(appContext)) {
                                    new Update_Cenas(((CustomEditText) findViewById(R.id.event_name)).getText().toString(),
                                            ((CustomEditText) findViewById(R.id.event_discription)).getText().toString(),
                                            ((CustomAutoCompleteTextView) findViewById(R.id.event_address)).getText().toString(),
                                            ((CustomEditText) findViewById(R.id.phone_no)).getText().toString(),
//                                            ((CustomEditText) findViewById(R.id.quantity_order)).getText().toString(),
                                            "", "", "", "", cats,
                                            ((CustomEditText) findViewById(R.id.event_price)).getText().toString(), "1",
                                            ((CustomEditText) findViewById(R.id.singer_name)).getText().toString(), datetime.replaceAll(", ", ",")).execute();
                                } else {
                                    Utility.showCroutonWarn(getResources().getString(R.string.error_internet), Activity_Edit_MyCenas.this);
                                }
                            }
                        }
                    }
                }).setNegativeButton(getResources().getString(R.string.logout_no), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                String cats = categoryNameList.toString().substring(1, categoryNameList.toString().length() - 1);
                String datetime = "";
                if (daytimelist.size() == 0 && !is_event.equals("1")) {
                    Utility.showCroutonWarn(getResources().getString(R.string.select_hours_msg), Activity_Edit_MyCenas.this);
                } else {
                    datetime = daytimelist.toString();
                    datetime = datetime.substring(1, datetime.length() - 1);
                    System.out.println("data for send server ==> " + datetime.replaceAll(", ", ","));
                    if (is_event.equals("1")) {
                        if (Utility.isConnectingToInternet(appContext)) {
                            new Update_Cenas(((CustomEditText) findViewById(R.id.event_name)).getText().toString(),
                                    ((CustomEditText) findViewById(R.id.event_discription)).getText().toString(),
                                    ((CustomAutoCompleteTextView) findViewById(R.id.event_address)).getText().toString(),
                                    ((CustomEditText) findViewById(R.id.phone_no)).getText().toString(),
//                                    ((CustomEditText) findViewById(R.id.quantity_order)).getText().toString(),
                                    ((CustomTextView) findViewById(R.id.start_end_date)).getText().toString().split(" To ")[0],
                                    ((CustomTextView) findViewById(R.id.start_end_date)).getText().toString().split(" To ")[1],
                                    Time2(((CustomTextView) findViewById(R.id.start_end_time)).getText().toString().split(" To ")[0]),
                                    Time2(((CustomTextView) findViewById(R.id.start_end_time)).getText().toString().split(" To ")[1]),
                                    cats,
                                    ((CustomEditText) findViewById(R.id.event_price)).getText().toString(), "0",
                                    ((CustomEditText) findViewById(R.id.singer_name)).getText().toString(), datetime.replaceAll(", ", ",")).execute();
                        } else {
                            Utility.showCroutonWarn(getResources().getString(R.string.error_internet), Activity_Edit_MyCenas.this);
                        }
                    } else {
                        if (Utility.isConnectingToInternet(appContext)) {
                            new Update_Cenas(((CustomEditText) findViewById(R.id.event_name)).getText().toString(),
                                    ((CustomEditText) findViewById(R.id.event_discription)).getText().toString(),
                                    ((CustomAutoCompleteTextView) findViewById(R.id.event_address)).getText().toString(),
                                    ((CustomEditText) findViewById(R.id.phone_no)).getText().toString(),
//                                    ((CustomEditText) findViewById(R.id.quantity_order)).getText().toString(),
                                    "", "", "", "", cats,
                                    ((CustomEditText) findViewById(R.id.event_price)).getText().toString(), "0",
                                    ((CustomEditText) findViewById(R.id.singer_name)).getText().toString(), datetime.replaceAll(", ", ",")).execute();
                        } else {
                            Utility.showCroutonWarn(getResources().getString(R.string.error_internet), Activity_Edit_MyCenas.this);
                        }
                    }
                }
            }
        });
        final android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void SetListeners() {
        address.setThreshold(1);
        address.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
    }

    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            String key = ConstantsUrlKey.KEY;

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input + "&" + types + "&" + sensor + "&" + key;

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;

            try {
                // Fetching the data from we service
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {
            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();
            try {
                jObject = new JSONObject(jsonData[0]);
                System.out.println("JSON OBJECT IS" + jObject);
                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            String[] from = new String[]{"description"};
            int[] to = new int[]{android.R.id.text1};
            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(appContext, result, android.R.layout.simple_list_item_1, from, to);
            // Setting the adapter
            address.setAdapter(adapter);

        }
    }

    public class Update_Cenas extends AsyncTask<String, String, String> {
        String eventname, des, add, co_no, f_date, t_date, s_time, e_time, cat, event_price, issell, singer_name, hours;

        public Update_Cenas(String eventname, String des, String add, String co_no, String f_date, String t_date,
                            String s_time, String e_time, String cat, String event_price, String issell, String singer_name, String hours) {
            this.eventname = eventname;
            this.des = des;
            this.add = add;
            this.co_no = co_no;
//            this.qty = qty;
            this.f_date = f_date;
            this.t_date = t_date;
            this.s_time = s_time;
            this.e_time = e_time;
            this.cat = cat;
            this.event_price = event_price;
            this.issell = issell;
            this.singer_name = singer_name;
            this.hours = hours;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utility.ShowLoading(appContext, getResources().getString(R.string.loading_msg));
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                FileUploader multipart = new FileUploader(ConstantsUrlKey.SERVER_URL, "UTF-8");
                multipart.addFormField(ConstantsUrlKey.ACTION, ConstantsUrlKey.UPDATECENAS);
                multipart.addFormField("id", cenas_id);
                System.out.println("cenasid ==> " + cenas_id);
                multipart.addFormField(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
                System.out.println("userid ==> " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
                multipart.addFormField(ConstantsUrlKey.ISEVENT, is_event);
                System.out.println("isevent ==> " + is_event);
                if (is_event.equals("1")) {
                    multipart.addFormField(ConstantsUrlKey.CENAS_TYPE, cenas_type);
                    System.out.println("cenastype ==> " + cenas_type);
                }
                multipart.addFormField(ConstantsUrlKey.EVENTNAME, eventname);
                System.out.println("event type ==> " + eventname);
                multipart.addFormField(ConstantsUrlKey.DESCRIPTION, des);
                System.out.println("discription ==> " + des);
                multipart.addFormField(ConstantsUrlKey.ADDRESS, add);
                System.out.println("address ==> " + add);
                multipart.addFormField(ConstantsUrlKey.CO_NUMBER, co_no);
                System.out.println("number ==> " + co_no);
//                multipart.addFormField(ConstantsUrlKey.QUANTITY, qty);
//                System.out.println("quantity ==> " + qty);
                multipart.addFormField(ConstantsUrlKey.FROM_DATE, f_date);
                System.out.println("start date ==> " + f_date);
                multipart.addFormField(ConstantsUrlKey.TO_DATE, t_date);
                System.out.println("end date ==> " + t_date);
                multipart.addFormField(ConstantsUrlKey.START_TIME, s_time);
                System.out.println("start time ==> " + s_time);
                multipart.addFormField(ConstantsUrlKey.END_TIME, e_time);
                System.out.println("end time ==> " + e_time);
                multipart.addFormField(ConstantsUrlKey.CATEGORY, cat);
                System.out.println("caty ==> " + cat);
                multipart.addFormField(ConstantsUrlKey.EVENTPRICE, event_price);
                System.out.println("price ==> " + event_price);
                multipart.addFormField("is_sell", issell);
                if (cenas_type.equals("music")) {
                    multipart.addFormField("singer_name", singer_name);
                    System.out.println("singer ==> " + singer_name);
                }
                if (!is_event.equals("1")) {
                    multipart.addFormField("hours", hours);
                    System.out.println("hours ==> " + hours);
                }
                if (videopath != null && !videopath.equals("")) {
                    multipart.addFilePart("video", new File(videopath));
                    multipart.addFilePart("video_thumbnail", new File(thumbnailpath));
                    System.out.println("video_thumbnail ===> " + new File(thumbnailpath));
                    System.out.println("video ==> " + new File(videopath));
                } else {
                    multipart.addFormField("video", "");
                    multipart.addFormField("video_thumbnail", "");
                }
                /*if (imgPaths.size() > 0) {
                    for (int i = 0; i < imgPaths.size(); i++) {
                        multipart.addFilePart("image[]", new File(imgPaths.get(i)));
                        System.out.println("images send to server ==> " + new File(imgPaths.get(i)));
                    }
                } else {
                    multipart.addFormField("image[]", "");
                }*/

                if (picturePath1.equals("")) {
                    multipart.addFormField("image1", "");
                } else {
                    multipart.addFilePart("image1", new File(picturePath1));
                }
                if (picturePath2.equals("")) {
                    multipart.addFormField("image2", "");
                } else {
                    multipart.addFilePart("image2", new File(picturePath2));
                }
                if (picturePath3.equals("")) {
                    multipart.addFormField("image3", "");
                } else {
                    multipart.addFilePart("image3", new File(picturePath3));
                }
                if (picturePath4.equals("")) {
                    multipart.addFormField("image4", "");
                } else {
                    multipart.addFilePart("image4", new File(picturePath4));
                }
                return multipart.finish();
            } catch (Exception e) {
                e.printStackTrace();
                return "Server Not Responding !";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Utility.HideDialog();
            System.out.println("SERVER RESULT" + result);
            if (result == null) {
                Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_Edit_MyCenas.this);
            } else {
                try {
                    JSONObject json = new JSONObject(result);
                    if (json.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                        startActivity(new Intent(appContext, My_cenas_Activity.class));
                        finish();
                        Utility.ShowToastMessage(appContext, getResources().getString(R.string.cenas_update_Successfuly));
                    } else {
                        Utility.showCroutonWarn(json.getString(ConstantsUrlKey.SERVER_MSG), Activity_Edit_MyCenas.this);
                    }
                } catch (Exception e) {
                    Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_Edit_MyCenas.this);
                }
            }
            super.onPostExecute(result);
        }
    }
}
