package igs.com.cenason.fragment;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import igs.com.cenason.Activity_conversation;
import igs.com.cenason.CustomWidget.CustomEditText;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 10/14/2016.
 */

public class Chat_contact_list_fragment extends Fragment {

    Context appContext;
    ResponseTask rt;
    LinkedList<JSONObject> mycontactlist = new LinkedList<>();
    ContactListAdapter listAdapter;
    Dialog add_friend_dialog;
    Button done_btn, cancle_btn;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    CustomEditText comment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_list_view, container, false);
        appContext = getActivity();
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        Inti();
        return rootView;
    }

    private void Inti() {
        fetchContacts();
    }


    public void fetchContacts() {
        StringBuilder str = new StringBuilder();
        String phoneNumber = null;

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        StringBuffer output = new StringBuffer();
        ContentResolver contentResolver = getActivity().getContentResolver();
        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);

        // Loop for every contact in the phone
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);
                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        str.append(phoneNumber);
                        str.append(",");
                    }
                    phoneCursor.close();
                }
            }
            System.out.println("str ===> " + str.toString().replaceAll(" ", "").replaceAll("-", "").replaceAll("\\(", "").replaceAll("\\)", ""));
            if (Utility.isConnectingToInternet(appContext)) {
                String con=str.toString().replaceAll(" ", "").replaceAll("-", "").replaceAll("\\(", "").replaceAll("\\)", "");
                if(con.length()>0){
                Getcontactlist(str.toString().replaceAll(" ", "").replaceAll("-", "").replaceAll("\\(", "").replaceAll("\\)", ""));
            }
            } else {
                Utility.showCrouton(getResources().getString(R.string.error_internet), getActivity());
            }
        }
    }

    public void Getcontactlist(String contacts) {
        System.out.println("User id =====> " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.GETCONTACTLIST);
            jsonObject.put("mobileno", contacts.substring(0, contacts.length() - 1));
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("server Result=====>" + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                for (int i = 0; i < jobj.getJSONArray(ConstantsUrlKey.OBJECT).length(); i++) {
                                    mycontactlist.add(jobj.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i));
                                }
                                listAdapter = new ContactListAdapter(appContext, mycontactlist);
                                ((ListView) getActivity().findViewById(R.id.list)).setAdapter(listAdapter);
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), getActivity());
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void SendRequesttask(final int pos, String comment) {
        System.out.println("User id =====> " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            String userid = mycontactlist.get(pos).getString("userid");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.SENDFRIENDREQUEST);
            jsonObject.put(ConstantsUrlKey.FROM_USER, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.TO_USER, userid);
            jsonObject.put(ConstantsUrlKey.COMMENT, comment);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                JSONObject j2 = mycontactlist.get(pos);
                                j2.remove("friend_status");
                                j2.put("friend_status", "1");
                                mycontactlist.remove(pos);
                                mycontactlist.add(pos, j2);
                                listAdapter.notifyDataSetChanged();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), getActivity());
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public class ContactListAdapter extends BaseAdapter {

        Context appContext;
        LinkedList<JSONObject> list;

        public ContactListAdapter(Context appContext, LinkedList<JSONObject> list) {
            this.appContext = appContext;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = li.inflate(R.layout.contact_list_item, null);
            }
            try {
                ((CustomTextView) convertView.findViewById(R.id.tvContactName)).setText(list.get(position).getString("fullname"));
                ((CustomTextView) convertView.findViewById(R.id.tvPhoneNumber)).setText(list.get(position).getString("mobileno"));
                Glide.with(appContext).load(list.get(position).getString("image_path")).
                        into(((ImageView) convertView.findViewById(R.id.user_image)));
                convertView.setOnClickListener(new MyClicklist(position));
                if (list.get(position).getString("friend_status").equals("0") &&
                        list.get(position).getString("to_friend_status").equals("0")) {
                    (convertView.findViewById(R.id.send_request_btn)).setVisibility(View.VISIBLE);
                    (convertView.findViewById(R.id.requested)).setVisibility(View.GONE);
                    (convertView.findViewById(R.id.send_request_btn)).setOnClickListener(new MyClick(position));
                    System.out.println("first condition");
                } else if (list.get(position).getString("friend_status").equals("1")) {
                    (convertView.findViewById(R.id.send_request_btn)).setVisibility(View.GONE);
                    (convertView.findViewById(R.id.requested)).setVisibility(View.VISIBLE);
                    System.out.println("second condition");
                } else if (list.get(position).getString("friend_status").equals("2")) {
                    (convertView.findViewById(R.id.send_request_btn)).setVisibility(View.GONE);
                    (convertView.findViewById(R.id.requested)).setVisibility(View.GONE);
                    System.out.println("third condition");
                } else if (list.get(position).getString("to_friend_status").equals("2") &&
                        list.get(position).getString("friend_status").equals("0")) {
                    (convertView.findViewById(R.id.send_request_btn)).setVisibility(View.GONE);
                    (convertView.findViewById(R.id.requested)).setVisibility(View.GONE);
                    System.out.println("fourth condition");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        class MyClick implements View.OnClickListener {
            int position;

            public MyClick(int mposition) {
                position = mposition;
            }

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.send_request_btn:
                        add_friend_dialog = new Dialog(appContext);
                        add_friend_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        add_friend_dialog.setContentView(R.layout.add_friend_dialog);
                        add_friend_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        add_friend_dialog.getWindow().getAttributes().windowAnimations = R.style.NewAnimForDialog;
                        done_btn = (Button) add_friend_dialog.findViewById(R.id.confirm_button);
                        cancle_btn = (Button) add_friend_dialog.findViewById(R.id.cancle_button);
                        comment = (CustomEditText) add_friend_dialog.findViewById(R.id.add_comment);
                        done_btn.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (Utility.isConnectingToInternet(appContext)) {
                                    SendRequesttask(position, comment.getText().toString());
                                } else {
                                    Utility.showCrouton(getResources().getString(R.string.error_internet), getActivity());
                                }
                                add_friend_dialog.dismiss();
                            }
                        });
                        cancle_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                add_friend_dialog.dismiss();
                            }
                        });
                        add_friend_dialog.show();
                        break;
                }
            }
        }

        class MyClicklist implements View.OnClickListener {
            int mylistposition;

            public MyClicklist(int Pos) {
                mylistposition = Pos;
            }

            @Override
            public void onClick(View v) {
                try {
                    System.out.println("list position ====> " + mylistposition);
                    if (list.get(mylistposition).getString("friend_status").equals("0") &&
                            list.get(mylistposition).getString("to_friend_status").equals("0")) {

                    } else if (list.get(mylistposition).getString("friend_status").equals("1")) {

                    } else if (list.get(mylistposition).getString("friend_status").equals("2")) {
                        Intent intent = new Intent(appContext, Activity_conversation.class);
                        intent.putExtra(ConstantsUrlKey.CENAS_DATA, list.get(mylistposition).toString());
                        appContext.startActivity(intent);
                    } else if (list.get(mylistposition).getString("to_friend_status").equals("2") &&
                           list.get(mylistposition).getString("friend_status").equals("0")) {
                        Intent intent = new Intent(appContext, Activity_conversation.class);
                        intent.putExtra(ConstantsUrlKey.CENAS_DATA, list.get(mylistposition).toString());
                        appContext.startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
