package igs.com.cenason.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import igs.com.cenason.Activity_conversation;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.R;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 10/14/2016.
 */
public class Chat_list_fragment extends Fragment {

    Context appContext;
    ResponseTask rt;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    LinkedList<JSONObject> myuserchatlist = new LinkedList<>();
    ChatuserListAdapter listAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_list_view, container, false);
        appContext = getActivity();
        Inti();
        return rootView;
    }

    private void Inti() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        Getuserchatlist();
    }

    public void Getuserchatlist() {
        System.out.println("User id =====> " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        System.out.println("chat list");
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.CHATDETAILBYUSER);
            jsonObject.put(ConstantsUrlKey.USERID, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("server Result=====>" + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                for (int i = 0; i < jobj.getJSONArray(ConstantsUrlKey.OBJECT).length(); i++) {
                                    myuserchatlist.add(jobj.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i));
                                }
                                listAdapter = new ChatuserListAdapter(appContext, myuserchatlist);
                                ((ListView) getActivity().findViewById(R.id.list)).setAdapter(listAdapter);
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), getActivity());
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public class ChatuserListAdapter extends BaseAdapter {

        Context appContext;
        LinkedList<JSONObject> list;

        public ChatuserListAdapter(Context appContext, LinkedList<JSONObject> list) {
            this.appContext = appContext;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = li.inflate(R.layout.contact_list_item, null);
            }
            try {
                ((CustomTextView) convertView.findViewById(R.id.send_request_btn)).setVisibility(View.GONE);
                ((CustomTextView) convertView.findViewById(R.id.tvContactName)).setText(list.get(position).getString("fullname"));
                if (list.get(position).getString("message").equals("")) {
                    ((CustomTextView) convertView.findViewById(R.id.tvPhoneNumber)).setText(getResources().getString(R.string.cenas_share));
                } else {
                    ((CustomTextView) convertView.findViewById(R.id.tvPhoneNumber)).setText(list.get(position).getString("message"));
                }
                ((CustomTextView) convertView.findViewById(R.id.mobile)).setText(list.get(position).getString("datetime"));
                Glide.with(appContext).load(list.get(position).getString("image_path")).into(((ImageView) convertView.findViewById(R.id.user_image)));

                convertView.setOnClickListener(new MyClick(position));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        class MyClick implements View.OnClickListener {
            int position;

            public MyClick(int mposition) {
                position = mposition;
            }

            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(getActivity(), Activity_conversation.class);
                    i.putExtra(ConstantsUrlKey.CENAS_DATA, list.get(position).toString());
                    startActivity(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
