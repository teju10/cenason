package igs.com.cenason.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import igs.com.cenason.Activity_View_Cenas;
import igs.com.cenason.R;
import igs.com.cenason.TabActivity;
import igs.com.cenason.Utilities.GPSTracker;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Shoaib on 22-Aug-16.
 */
public class Nearby_cenas_map_fragment extends Fragment implements View.OnClickListener, OnMapReadyCallback {
    GoogleMap googleMap;
    Context appContext;
    LocationManager mManager;
    GPSTracker mGPS;
    double lati, longi;
    SupportMapFragment fm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_nearby_cenas_map_view, container, false);
        appContext = getActivity();
        (rootView.findViewById(R.id.imgMyLocation)).setOnClickListener(this);

        Init();

        return rootView;
    }

    private void Init() {
        mManager = (LocationManager) appContext.getSystemService(Context.LOCATION_SERVICE);
        if (!mManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Utility.AlertNoGPS(appContext);
        }
        mGPS = new GPSTracker(appContext);
        lati = mGPS.getLatitude();
        longi = mGPS.getLongitude();

        googleMapCheck();
    }

    private void getMyLocation() {
        LatLng latLng = new LatLng(lati, longi);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 12);
        googleMap.animateCamera(cameraUpdate);
    }

    private void googleMapCheck() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(appContext);

        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
            dialog.show();
        } else {

            fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            fm.getMapAsync(this);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgMyLocation:
                getMyLocation();
        }
    }

    public void getneearbylist() {
        if (((TabActivity) appContext).nearbylist.size() > 0 && ((TabActivity) appContext).nearbylist != null) {
            if (((TabActivity) appContext).nearbylist.size() > 0 && ((TabActivity) appContext).nearbylist != null) {
                for (int i = 0; i < ((TabActivity) appContext).nearbylist.size(); i++) {
                    try {
                        LatLng l = new LatLng(Double.parseDouble(((TabActivity) appContext).nearbylist.get(i).getString("latitude")), Double.parseDouble(
                                ((TabActivity) appContext).nearbylist.get(i).getString("longitude")));
                       /* googleMap.addMarker(new MarkerOptions().position(l).title(((TabActivity) appContext).nearbylist.get(i).getString("name")
                                + "," +
                                ((TabActivity) appContext).nearbylist.get(i).getString("price") + "," +
                                ((TabActivity) appContext).nearbylist.get(i).getString("description") + "," +
                                ((TabActivity) appContext).nearbylist.get(i).getString("id")).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));*/

                        googleMap.addMarker(new MarkerOptions().position(l).title(((TabActivity) appContext)
                                .nearbylist.get(i).getString("name"))
                                .snippet(((TabActivity) appContext).nearbylist.get(i).getString("price") + "," +
                                        ((TabActivity) appContext).nearbylist.get(i).getString("description") + "," +
                                        ((TabActivity) appContext).nearbylist.get(i).getString("name"))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));

                       /* googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                            @Override
                            public void onInfoWindowClick(Marker marker) {
                                try {
                                    for (int i = 0; i < ((TabActivity) appContext).nearbylist.size(); i++) {
                                        Intent intent = new Intent(appContext, Activity_View_Cenas.class);
                                        intent.putExtra("catgory_id", ((TabActivity) appContext).nearbylist.get(i).getString("id"));
                                        appContext.startActivity(intent);
                                        ((TabActivity) appContext).finish();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });*/

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap mgoogleMap) {
        googleMap = mgoogleMap;

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);

        // Enable / Disable Zooming Controls
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        // Enable / Disable My Location Button
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        // Enable / Disable Compass Icon
        googleMap.getUiSettings().setCompassEnabled(true);

        // Enable / Disable Rotate Gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);

        // Enable / Disable Zooming Functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        getMyLocation();
        if (Utility.isConnectingToInternet(appContext)) {
            getneearbylist();
        } else {
            Utility.showCrouton(getResources().getString(R.string.error_internet), getActivity());
        }
    }


}
