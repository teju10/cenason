package igs.com.cenason.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import igs.com.cenason.Adapter.NearbylistAdapter;
import igs.com.cenason.R;
import igs.com.cenason.TabActivity;

/**
 * Created by Shoaib on 22-Aug-16.
 */
public class Nearby_cenas_list_fragment extends Fragment {

    Context appContext;
    NearbylistAdapter listAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.nearby_list_view, container, false);
        rootView.postDelayed(new Runnable() {

            @Override
            public void run() {
                Init();
            }
        }, 1000);

        return rootView;
    }

    private void Init() {
        getneearbylist();
    }

    public void getneearbylist() {
        try {
            if (((TabActivity) appContext).nearbylist.size() > 0 && ((TabActivity) appContext).nearbylist != null) {
                listAdapter = new NearbylistAdapter(appContext, ((TabActivity) appContext).nearbylist);
                ((ListView) getActivity().findViewById(R.id.list_view)).setAdapter(listAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
