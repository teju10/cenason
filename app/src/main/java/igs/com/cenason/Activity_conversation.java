package igs.com.cenason;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igs.com.cenason.Adapter.ChatAdapter;
import igs.com.cenason.CustomWidget.CircleImageView;
import igs.com.cenason.CustomWidget.CustomEditText;
import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.ServerInteraction.ResponseListener;
import igs.com.cenason.ServerInteraction.ResponseTask;
import igs.com.cenason.Utilities.ConstantsUrlKey;
import igs.com.cenason.Utilities.Utility;

/**
 * Created by Infograins on 10/24/2016.
 */
public class Activity_conversation extends AppCompatActivity implements View.OnClickListener {
    public static final String mBroadcastString = "mynotification";
    Context appContext;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    String date;
    Toolbar mToolbar;
    ResponseTask rt;
    ChatAdapter listAdapter;
    ArrayList<JSONObject> chatlist = new ArrayList<>();
    public static String isOPenActivity;
    public static String from_user_id;
    public static String to_user_id;
    private IntentFilter mIntentFilter;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(mBroadcastString)) {
                Getmsgtask();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        try {
            registerReceiver(mReceiver, mIntentFilter);
        } catch (Exception e) {

        }
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation_layout);
        appContext = this;

        isOPenActivity = "hjghgh";
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(mBroadcastString);
        Inti();
    }

    @Override
    protected void onDestroy() {
        isOPenActivity = null;
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    private void Inti() {
        share = appContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CircleImageView) findViewById(R.id.user_image)).setVisibility(View.VISIBLE);
        ((findViewById(R.id.send_msg_btn))).setOnClickListener(this);
        GetDate();

        if (Utility.isConnectingToInternet(appContext)) {
            Getmsgtask();
        } else {
            Utility.showCrouton(getResources().getString(R.string.error_internet), Activity_conversation.this);
        }
    }

    private void GetDate() {
        try {
            date = getIntent().getStringExtra(ConstantsUrlKey.CENAS_DATA);
            JSONObject jobj = new JSONObject(date);
            from_user_id = jobj.getString("userid");
            to_user_id = Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID);
            System.out.println("user_id ====> " + from_user_id);
            Glide.with(appContext).load(jobj.getString("image_path")).into(((CircleImageView) findViewById(R.id.user_image)));
            ((CustomTextView) findViewById(R.id.app_name)).setText(jobj.getString("fullname"));
            ((CustomTextView) findViewById(R.id.app_name)).setPadding(20, 0, 0, 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void Sendmsgtask(String msg) {
        System.out.println("User id =====> " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.CHAT);
            jsonObject.put(ConstantsUrlKey.FROM_USER, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.TO_USER, from_user_id);
            jsonObject.put(ConstantsUrlKey.MESSAGE, msg);
            jsonObject.put(ConstantsUrlKey.TYPE, "text");
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_conversation.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                ((CustomEditText) findViewById(R.id.send_msg_edittext)).setText("");
                                Getmsgtask();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_conversation.this);
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_conversation.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_msg_btn:
                if (((CustomEditText) findViewById(R.id.send_msg_edittext)).getText().toString().equals("")) {
                    Utility.ShowToastMessage(appContext, "please enter your message");
                } else {
                    if (Utility.isConnectingToInternet(appContext)) {
                        Sendmsgtask(((CustomEditText) findViewById(R.id.send_msg_edittext)).getText().toString());
                    } else {
                        Utility.showCrouton(getResources().getString(R.string.error_internet), Activity_conversation.this);
                    }
                }
        }
    }

    private void scrollDown() {
        listAdapter = new ChatAdapter(appContext, chatlist);
        System.out.println("Size of list  ======>>>>>>   " + chatlist.size());
        ((ListView) findViewById(R.id.conversation_list)).setAdapter(listAdapter);
        ((ListView) findViewById(R.id.conversation_list)).setSelection(listAdapter.getCount() - 1);
    }

    public void Getmsgtask() {
        System.out.println("User id =====> " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.CHATLIST);
            jsonObject.put(ConstantsUrlKey.FROM_USER, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.TO_USER, from_user_id);
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_conversation.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {
                                chatlist.clear();
                                for (int i = 0; i < jobj.getJSONArray(ConstantsUrlKey.OBJECT).length(); i++) {
                                    chatlist.add(jobj.getJSONArray(ConstantsUrlKey.OBJECT).getJSONObject(i));
                                }
                                scrollDown();
                                Sendreadstatus();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_conversation.this);
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_conversation.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void Sendreadstatus() {
        System.out.println("User id =====> " + Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantsUrlKey.ACTION, ConstantsUrlKey.UPDATEREADSTATUS);
            jsonObject.put(ConstantsUrlKey.FROM_USER, Utility.getSharedPreferences(appContext, ConstantsUrlKey.USERID));
            jsonObject.put(ConstantsUrlKey.TO_USER, from_user_id);
            //Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(ConstantsUrlKey.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_conversation.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(ConstantsUrlKey.RESULT_KEY).equals("1")) {

                            } else {
                                Utility.showCroutonWarn(jobj.getString(ConstantsUrlKey.SERVER_MSG), Activity_conversation.this);
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Activity_conversation.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
