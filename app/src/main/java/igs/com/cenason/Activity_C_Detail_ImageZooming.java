package igs.com.cenason;

import android.content.Context;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;

import java.lang.reflect.Array;
import java.util.ArrayList;

import igs.com.cenason.CustomWidget.CustomTextView;
import igs.com.cenason.Utilities.ZoomageView;

/**
 * Created by sandeep on 24/2/17.
 */

public class Activity_C_Detail_ImageZooming extends AppCompatActivity {

    Context mContext;
    ImageAdapter adapter;
    Bundle b;
    ViewPager viewPager;
    CirclePageIndicator circlePageIndicator;
    ArrayList<String> imageArray;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_imagezoom_sliderpager);

        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomTextView) findViewById(R.id.app_name)).setText("Preview");
        circlePageIndicator = (CirclePageIndicator) findViewById(R.id.preview_image_indicator);
        viewPager = (ViewPager) findViewById(R.id.home_detail_view_pager);

        Intent i = getIntent();
        String file_array = i.getStringExtra("file_array");
        try{
            JSONArray jsonArray = new JSONArray(file_array);
            imageArray = new ArrayList<>();
            for (int j=0; j<jsonArray.length(); j++){
                imageArray.add(jsonArray.getString(j));
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        adapter = new ImageAdapter(mContext, R.layout.dialog_zoom_view, imageArray);
        viewPager.setAdapter(adapter);
        circlePageIndicator.setViewPager(viewPager);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public class ImageAdapter extends PagerAdapter {
        Context context;
        ArrayList<String> list = new ArrayList<>();
        int res;

        public ImageAdapter(Context context, int resourse, ArrayList<String> imageList) {
            this.context = context;
            this.list = imageList;
            this.res = resourse;
            System.out.println("list size" + list);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            final ViewGroup layout = (ViewGroup) inflater.inflate(res, container, false);
            Glide.with(mContext).load(list.get(position)).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            ((ProgressBar) layout.findViewById(R.id.pb)).setVisibility(View.GONE);
                            return false;
                        }
                    }).into(((ImageView) layout.findViewById(R.id.img_zoom)));

            container.addView(layout);
            ((ZoomageView) layout.findViewById(R.id.img_zoom)).setTranslatable(true);
            ((ZoomageView) layout.findViewById(R.id.img_zoom)).setZoomable(true);
            ((ZoomageView) layout.findViewById(R.id.img_zoom)).setAutoCenter(true);
            ((ZoomageView) layout.findViewById(R.id.img_zoom)).setAnimateOnReset(true);

            return layout;

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
